package com.despegar.testng.flights;

import com.despegar.utils.*;
import com.google.common.collect.Ordering;
import com.opencsv.CSVReader;
import com.despegar.model.TestParameters;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.despegar.pages.CheckoutPage;
import com.despegar.pages.ClustersPage;
import com.despegar.pages.FlightsPage;
import com.despegar.services.flights.FlightsService;
import com.despegar.services.flights.dto.response.BusquestsResponseDTO;
import com.despegar.services.flights.dto.response.BusquestsClusterDTO;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FlightsValidationTest {

    private static Logger LOGGER = LoggerFactory.getLogger(FlightsValidationTest.class.getName());
    private final static String URL_SEARCH = "url.search";

    @Test(dataProvider = "testParametersProvider")
    public void searchBoxResultsValidation(TestParameters testParameters) throws Exception {

        /* Initializations */
        WebDriver driver = SeleniumUtils.buildDriver(Browser.CHROME);
        driver.manage().window().maximize();
        Properties props = getProperty(testParameters.getCountry());
        ParametersPack tripInfo = testParameters.getParametersPack();
        LabelsPack labelsPack = testParameters.getLabelsPack();
        UrlParameters urlInfo = new UrlParameters(props.getProperty("site"), props.getProperty("channel"), props.getProperty("orderBy"),
                props.getProperty("currency"), props.getProperty("viewMode"), props.getProperty("language"),
                props.getProperty("limit"));

        try {
           verifyFlow(driver, labelsPack, props, tripInfo, testParameters.getCountry());

           /* Backend */
           FlightsService flightsService = new FlightsService();
           BusquestsResponseDTO responseDTO;
           ResponseEntity<BusquestsResponseDTO> entity = flightsService.search(tripInfo, urlInfo);
           responseDTO = entity.getBody();

           List<BusquestsClusterDTO> clusters = responseDTO.getClusters().subList(0, 10);
           clusters.stream().forEach(cluster -> verifyCluster(cluster, tripInfo));

            /* Check BE and GUI */
            ClustersPage backendClustersPage = new ClustersPage(driver).go(props.getProperty(URL_SEARCH), tripInfo, testParameters.getCountry());
            verifyPageResults(responseDTO, backendClustersPage, testParameters.getLabelsPack().getDirect());

        } finally {
            if (driver != null) {
                driver.quit();
            }
        }
    }

    private void verifyFlow(WebDriver driver, LabelsPack labelsPack, Properties props, ParametersPack tripInfo, String country) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        FlightsPage flightsPage = new FlightsPage(driver);

        flightsPage = flightsPage.go(props.getProperty("url.homeFlights"));
        verifySearchBoxInput(flightsPage, labelsPack);
        flightsPage.search(tripInfo.getFrom(), tripInfo.getTo());


        ClustersPage clustersPage = new ClustersPage(driver).go(props.getProperty(URL_SEARCH), tripInfo, country);

        if(!country.equals("AR"))
            clustersPage.closePopUp();

        List<Integer> prices = clustersPage.getPrices();
        LOGGER.info("Prices are ordered: " + Ordering.natural().isOrdered(prices));
        softAssert.assertTrue(Ordering.natural().isOrdered(prices));

        CheckoutPage checkoutPage = clustersPage.goFirstCheckout();
        LOGGER.info("Total price is OK for the first cluster: " + checkoutPage.totalPriceIsOK());
        softAssert.assertTrue(checkoutPage.totalPriceIsOK());

        ClustersPage clustersPageBack = new ClustersPage(driver).go(props.getProperty(URL_SEARCH), tripInfo, country);
        CheckoutPage secondCheckout = clustersPageBack.goRandomCheckout();
        LOGGER.info("Total price is OK for a random cluster: " + secondCheckout.totalPriceIsOK());
        softAssert.assertTrue(secondCheckout.totalPriceIsOK());
    }

    private void verifyPageResults(BusquestsResponseDTO responseDTO, ClustersPage backendClustersPage, String directLabel) {
        SoftAssert softAssert = new SoftAssert();

        LOGGER.info("Direct flights Count: (Page)" + backendClustersPage.getDirectFlights(directLabel) + " | (Expected)  " + responseDTO.getDirectFlightsCount());
        softAssert.assertEquals(Integer.parseInt(backendClustersPage.getDirectFlights(directLabel)), responseDTO.getDirectFlightsCount());

        LOGGER.info("Price Cluster --> (Page) " + backendClustersPage.getPrices().get(0) + " | (Expected)  " + responseDTO.getClusters().get(0).getAmount());
        softAssert.assertEquals(backendClustersPage.getPrices().get(0), responseDTO.getClusters().get(0).getAmount(), 0);

        List<Integer> pricesPage = backendClustersPage.getPrices();
        pricesPage.sort(Comparator.comparing(value -> value));

        LOGGER.info("Lower price --> (Page) " + pricesPage.get(0) + " | (Expected) " + getLowerPrice(responseDTO.getClusters()));
        softAssert.assertEquals(pricesPage.get(0), getLowerPrice(responseDTO.getClusters()), 0);

    }

    private void verifySearchBoxInput(FlightsPage flightsPage, LabelsPack labelsPack){
        SoftAssert softAssert = new SoftAssert();

        LOGGER.info("Title Text: " + flightsPage.getTitleText());
        softAssert.assertEquals(flightsPage.getTitleText(), labelsPack.getTitle());

        LOGGER.info("Origin Text : " + flightsPage.getOriginLabelText(labelsPack.getOrigin()));
        softAssert.assertTrue(flightsPage.originLabelIsDisplayed(labelsPack.getOrigin()));
        softAssert.assertEquals(flightsPage.getOriginLabelText(labelsPack.getOrigin()), labelsPack.getOrigin());

        LOGGER.info("Destiny Text: " + flightsPage.getDestinyLabelText(labelsPack.getDestiny()));
        softAssert.assertTrue(flightsPage.destinyLabelIsDisplayed(labelsPack.getDestiny()));
        softAssert.assertEquals(flightsPage.getDestinyLabelText(labelsPack.getDestiny()), labelsPack.getDestiny());

        LOGGER.info("Departure Text: " + flightsPage.getDepartureInputPlaceholder());
        softAssert.assertTrue(flightsPage.departureInputIsDisplayed());
        softAssert.assertEquals(flightsPage.getDepartureInputPlaceholder(), labelsPack.getDeparturePlaceholder());

        LOGGER.info("Departure Text: " + flightsPage.getReturnInputPlaceholder());
        softAssert.assertTrue(flightsPage.returnInputIsDisplayed());
        softAssert.assertEquals(flightsPage.getReturnInputPlaceholder(), labelsPack.getReturnPlaceholder());

        LOGGER.info("Radio Button Text: " + flightsPage.getJustGoRadioText(labelsPack.getJustGo()));
        softAssert.assertTrue(flightsPage.justGoRadioIsDisplayed(labelsPack.getJustGo()));

        LOGGER.info("Passengers is displayed: " + flightsPage.passengersIsDisplayed());
        softAssert.assertTrue(flightsPage.passengersIsDisplayed());

        LOGGER.info("Search Button is displayed: " + flightsPage.searchButtonIsDisplay());
        softAssert.assertTrue(flightsPage.searchButtonIsDisplay());
    }

    private double getLowerPrice(List<BusquestsClusterDTO> clusters){
        List<Double> amounts = clusters.stream().map(cluster -> cluster.getAmount()).collect(Collectors.toList());
        amounts.sort(Comparator.comparing(value -> value));
        return amounts.get(0);
    }

    private void verifyCluster(BusquestsClusterDTO cluster, ParametersPack tripInfo) {
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(cluster.getDepartureAirportCodeOutbound(), tripInfo.getFrom());
        softAssert.assertEquals(cluster.getArrivalAirportCodeOutbound(), tripInfo.getTo());
        softAssert.assertEquals(cluster.getDepartureAirportCodeInbound(), tripInfo.getTo());
        softAssert.assertEquals(cluster.getArrivalAirportCodeInbound(), tripInfo.getFrom());

    }

    private Properties getProperty(String country) throws IOException {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Properties countryProperty = new Properties();
        InputStream resourceStream = loader.getResourceAsStream(country);

        countryProperty.load(resourceStream);
        return countryProperty;
    }

    @DataProvider(name = "testParametersProvider")
    public Iterator<TestParameters[]> itearatorDataProvider(ITestContext context) throws IOException {

        String country = context.getCurrentXmlTest().getParameter("country");
        String title = context.getCurrentXmlTest().getParameter("title");
        String origin = context.getCurrentXmlTest().getParameter("origin");
        String destiny = context.getCurrentXmlTest().getParameter("destiny");
        String departurePlaceholder = context.getCurrentXmlTest().getParameter("departurePlaceholder");
        String returnPlaceholder = context.getCurrentXmlTest().getParameter("returnPlaceholder");
        String justGo = context.getCurrentXmlTest().getParameter("just-go");
        String direct = context.getCurrentXmlTest().getParameter("direct");
        LabelsPack labelsPack = new LabelsPack(title, origin,destiny, departurePlaceholder, returnPlaceholder, justGo, direct);

        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream resourceStream = loader.getResourceAsStream("urlParams.csv");
        CSVReader reader = new CSVReader(new InputStreamReader(resourceStream));
        String [] nextLine;
        List<TestParameters> testParameters = new ArrayList<TestParameters>();

        while ((nextLine = reader.readNext()) != null) {
            ParametersPack parametersPack = new ParametersPack(nextLine[0], nextLine[1], nextLine[2], nextLine[3], nextLine[4], nextLine[5], nextLine[6], nextLine[7]);
            TestParameters testparameter = new TestParameters(country, labelsPack, parametersPack);
            testParameters.add(testparameter);
        }


        List<TestParameters[]> dataList = new ArrayList<TestParameters[]>();

        for (TestParameters test : testParameters) {
            dataList.add(new TestParameters[] {test});
        }

        return dataList.iterator();
    }
}