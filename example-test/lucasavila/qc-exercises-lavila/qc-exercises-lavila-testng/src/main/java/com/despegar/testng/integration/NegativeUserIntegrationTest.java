package com.despegar.testng.integration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import com.despegar.services.users.UserException;
import com.despegar.services.users.UserService;
import com.despegar.services.users.dto.request.CreateUserRequestDTO;
import com.despegar.services.users.dto.request.OfficeCode;
import com.despegar.services.users.dto.response.UserResponseDTO;

public class NegativeUserIntegrationTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositiveUserIntegrationTest.class.getName());
    public final static String USERS_APP_URL = "http://10.254.168.101:1458";
    public final static String EMAIL = "test@despegar.com";
    public final static String PASS = "despegar";
    private final UserService userService = new UserService(USERS_APP_URL);

    @Parameters({"user-first-name", "user-last-name", "user-gender", "user-office-code", "user-email"})
    @Test
    public void unknownPassword(String name, String lastName, String gender, String office, String email){
        Assertion hardAssert = new Assertion();
        CreateUserRequestDTO signUpRequest = new CreateUserRequestDTO(email, name, gender, lastName, "", OfficeCode.DESPEGAR_AR, "");
        try {
            ResponseEntity<UserResponseDTO> creationResponse = userService.create(signUpRequest);
            hardAssert.fail("This case is an exception.");
        } catch (UserException ue) {
            LOGGER.info("User Exception: ", ue);
            hardAssert.assertNotNull(ue);
            hardAssert.assertEquals(ue.getMessage(), "Attribute password is empty.");
            hardAssert.assertEquals(ue.getCode(), HttpStatus.BAD_REQUEST);
        }
    }

    @Parameters({"user-first-name", "user-last-name", "user-gender", "user-office-code", "user-password"})
    @Test
    public void unknownEmail(String name, String lastName, String gender, String office, String password){
        Assertion hardAssert = new Assertion();
        CreateUserRequestDTO signUpRequest = new CreateUserRequestDTO("", name, gender, lastName, "", OfficeCode.DESPEGAR_AR, password);

        try {
            ResponseEntity<UserResponseDTO> creationResponse = userService.create(signUpRequest);
            hardAssert.fail("This case is an exception.");
        } catch (UserException ue) {
            LOGGER.info("User Exception: ", ue);
            hardAssert.assertNotNull(ue);
            hardAssert.assertEquals(ue.getMessage(), "Attribute email is empty.");
            hardAssert.assertEquals(ue.getCode(), HttpStatus.BAD_REQUEST);
        }
    }
}
