package com.despegar.model;

import com.despegar.services.users.dto.request.OfficeCode;

public class User {

    private String firstName;
    private String lastName;
    private String gender;
    private OfficeCode officeCode;
    private String email;
    private String password;

    public User(String firstName, String lastName, String gender, OfficeCode officeCode, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.officeCode = officeCode;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public OfficeCode getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(OfficeCode officeCode) {
        this.officeCode = officeCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
