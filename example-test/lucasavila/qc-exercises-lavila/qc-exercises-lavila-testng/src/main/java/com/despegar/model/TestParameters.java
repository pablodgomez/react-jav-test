package com.despegar.model;

import com.despegar.utils.LabelsPack;
import com.despegar.utils.ParametersPack;

public class TestParameters {

    String country;
    LabelsPack labelsPack;
    ParametersPack parametersPack;

    public TestParameters(String country, LabelsPack labelsPack, ParametersPack parametersPack) {
        this.country = country;
        this.labelsPack = labelsPack;
        this.parametersPack = parametersPack;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LabelsPack getLabelsPack() {
        return labelsPack;
    }

    public void setLabelsPack(LabelsPack labelsPack) {
        this.labelsPack = labelsPack;
    }

    public ParametersPack getParametersPack() {
        return parametersPack;
    }

    public void setParametersPack(ParametersPack parametersPack) {
        this.parametersPack = parametersPack;
    }

}
