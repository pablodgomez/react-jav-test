package com.despegar.testng.integration;

import com.despegar.model.User;
import com.despegar.services.users.dto.response.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import com.despegar.services.users.UserService;
import com.despegar.services.users.dto.request.CreateUserRequestDTO;
import com.despegar.services.users.dto.request.LoginRequestDTO;
import com.despegar.services.users.dto.request.OfficeCode;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PositiveUserIntegrationTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PositiveUserIntegrationTest.class.getName());
    public final static String USERS_APP_URL = "http://10.254.168.101:1458";
    public final static String EMAIL = "test@despegar.com";
    public final static String PASS = "despegar";
    private final UserService userService = new UserService(USERS_APP_URL);


    @Test(dataProvider = "usersDataProvider")
    public void fullFlowTest(User user, ITestContext context){
        Assertion hardAssert = new Assertion();
        LOGGER.info("----------Test ---------");
        LOGGER.info(String.format("Creating user --> Name: %s, Last Name: %s, Gender: %s, Office Code: %s, Email: %s, Password: %s",
                user.getFirstName(), user.getLastName(), user.getGender(), user.getOfficeCode(), user.getEmail(), user.getPassword()));

        // Create //
        CreateUserRequestDTO signUpRequest = new CreateUserRequestDTO(user.getEmail(), user.getFirstName(), user.getGender(), user.getLastName(), "", user.getOfficeCode(), user.getPassword());
        ResponseEntity<UserResponseDTO> creationResponse = userService.create(signUpRequest);

        UserDTO userCreated = creationResponse.getBody().getData();
        MetaDataDTO metaData = creationResponse.getBody().getMetaData();
        hardAssert.assertNotNull(userCreated);
        hardAssert.assertNotNull(metaData);
        hardAssert.assertEquals(metaData.getMessage(), "User Created");
        hardAssert.assertEquals(metaData.getCode(), HttpStatus.OK);
        hardAssert.assertNotNull(userCreated.getId());
        hardAssert.assertTrue(!StringUtils.isEmpty(userCreated.getId()));

        String userId = userCreated.getId();
        LOGGER.info("User created");
        // Search Validation //

        LoginRequestDTO loginRequest = new LoginRequestDTO(EMAIL, PASS);

        ResponseEntity<LoginResponseDTO> loginResponse = userService.loginUser(loginRequest);

        LoginDataDTO loginData = loginResponse.getBody().getData();
        hardAssert.assertNotNull(loginData);
        hardAssert.assertEquals(EMAIL, loginData.getEmail());
        hardAssert.assertTrue(loginResponse.getStatusCode().is2xxSuccessful());

        String hash = loginData.getHash();

        ResponseEntity<UserResponseDTO> searchResponse = userService.searchUser(userId, EMAIL, hash);
        UserDTO userSearched = searchResponse.getBody().getData();
        hardAssert.assertNotNull(userSearched.getId());
        hardAssert.assertTrue(!StringUtils.isEmpty(userSearched.getId()));
        hardAssert.assertEquals(userSearched.getId(), userId);
        hardAssert.assertEquals(userSearched.getEmail(), user.getEmail());
        hardAssert.assertEquals(userSearched.getFirstName(), user.getFirstName());
        hardAssert.assertEquals(userSearched.getGender(), user.getGender());
        hardAssert.assertEquals(userSearched.getLastName(), user.getLastName());
        hardAssert.assertEquals(userSearched.getOfficeCode(), user.getOfficeCode());
        LOGGER.info("Search was successful for " + user.getEmail());

        context.getCurrentXmlTest().addParameter("userId", userId);
        context.getCurrentXmlTest().addParameter("hash", hash);
    }

    @AfterMethod
    public void deleteUser(ITestContext context){
        Assertion hardAssert = new Assertion();

        String userId = context.getCurrentXmlTest().getParameter("userId");
        String hash = context.getCurrentXmlTest().getParameter("hash");
        LOGGER.info("User ID to delete: " + userId);
        LOGGER.info("Hash: " + hash);
        ResponseEntity<DeleteUserResponseDTO> deleteResponse = userService.delete(userId, EMAIL, hash);

        MetaDataDTO deletedData = deleteResponse.getBody().getMetaData();

        hardAssert.assertEquals(deletedData.getCode(), HttpStatus.OK);
        hardAssert.assertEquals(deletedData.getMessage(), "User Deleted");
        LOGGER.info("User delete finished");
    }


    @DataProvider(name = "usersDataProvider")
    public Iterator<User[]> itearatorDataProvider() {
        String genderF = "Female";
        String genderM = "Male";
        List<User> users = new ArrayList<User>();
        users.add(new User("Nestor", "Ibarraa", genderM, OfficeCode.DESPEGAR_AR, "test@aol.com", "134542"));
        users.add(new User("Cristina", "Amarilla", genderF, OfficeCode.DESPEGAR_AR, "example@ppt.com", "2tBkds3"));

        users.add(new User("Dilma", "Rousseff", genderF, OfficeCode.DESPEGAR_BR, "example@rsf.br", "wed516wd"));
        users.add(new User("Felipe", "Esturro", genderM, OfficeCode.DESPEGAR_CO, "example@xsf.ar", "awPeq@dkq"));
        users.add(new User("Belen", "Tripidi", genderF, OfficeCode.DESPEGAR_AR, "example@asq.br", "aqefÑsame"));
        users.add(new User("Lucas", "Awila", genderM, OfficeCode.DESPEGAR_AR, "example@xxqp.ar", "asqWprdlqm"));
        users.add(new User("Dany", "Mmqw", genderM, OfficeCode.DESPEGAR_AR, "example@xamq.br", "dqrfmQXX"));
        users.add(new User("Azel", "Maddony", genderF, OfficeCode.DESPEGAR_CO, "example@gmeil.cl", "dprg204aP"));
        users.add(new User("Rodrigo", "Sysout", genderM, OfficeCode.DESPEGAR_MX, "example@wppe.mx", "amrgpZaff@"));
        users.add(new User("Jayme", "Rousseff", genderM, OfficeCode.DESPEGAR_AR, "example@asqe.fr", "aspfkq_dw"));

        List<User[]> dataList = new ArrayList<User[]>();

        for (User user : users) {
            dataList.add(new User[] {user});
        }

        return dataList.iterator();
    }

}
