package com.despegar.utils;

public class LabelsPack {

    private String title;
    private String origin;
    private String destiny;
    private String departurePlaceholder;
    private String returnPlaceholder;
    private String justGo;
    private String direct;

    public LabelsPack(String title, String origin, String destiny, String departurePlaceholder, String returnPlaceholder, String justGo, String direct) {
        this.title = title;
        this.origin = origin;
        this.destiny = destiny;
        this.departurePlaceholder = departurePlaceholder;
        this.returnPlaceholder = returnPlaceholder;
        this.justGo = justGo;
        this.direct = direct;
    }

    public String getJustGo() {
        return justGo;
    }

    public String getTitle() {
        return title;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestiny() {
        return destiny;
    }

    public String getDeparturePlaceholder() {
        return departurePlaceholder;
    }

    public String getReturnPlaceholder() {
        return returnPlaceholder;
    }

    public String getDirect() {
        return direct;
    }
}
