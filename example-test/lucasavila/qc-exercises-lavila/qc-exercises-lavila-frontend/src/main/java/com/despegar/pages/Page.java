package com.despegar.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Actions actions;

    public Page(WebDriver driver) {
        super();
        this.driver = driver;
        PageFactory.initElements(this.driver, this);

        this.wait = new WebDriverWait(this.driver, 20);
        this.actions = new Actions(this.driver);

    }

}