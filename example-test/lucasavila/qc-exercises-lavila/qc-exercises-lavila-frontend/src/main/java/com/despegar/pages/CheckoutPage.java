package com.despegar.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class CheckoutPage extends Page{

    @FindBy(className = "breakdown-prices")
    private WebElement prices;
    @FindBy(className = "payment_discount")
    private WebElement discount;
    @FindBy(id = "total-price")
    private WebElement totalPrice;
    @FindBy(className = "popup-wrapper")
    private WebElement popUp;

    public CheckoutPage(WebDriver driver) {
        super(driver);
    }

    public Boolean totalPriceIsOK(){
        String amountClass = "amount";
        wait.until(ExpectedConditions.visibilityOf(prices));
        Integer realTotal = Integer.parseInt(totalPrice.findElement(By.className(amountClass)).getText().trim().replace(".","").replace(",", ""));
        List<String> amountsStrings = prices.findElements(By.className(amountClass)).stream().map(amount -> amount.getText().trim().replace(".", "").replace(",", ""))
                .filter(amount -> !amount.isEmpty())
                .collect(Collectors.toList());
        List<Integer> amounts = amountsStrings.stream().map(amount -> Integer.parseInt(amount)).collect(Collectors.toList());

        Integer total = amounts.stream().reduce(0, Integer::sum).intValue() - realTotal;
        if(driver.findElements(By.className("payment_discount")).size() > 0)
            total -= 2*Integer.parseInt(discount.findElement(By.className(amountClass)).getText().replace(".","").replace(",", ""));

        return total.equals(realTotal);
    }

    public ClustersPage goBack() {
        driver.navigate().back();
        return new ClustersPage(this.driver);
    }
}
