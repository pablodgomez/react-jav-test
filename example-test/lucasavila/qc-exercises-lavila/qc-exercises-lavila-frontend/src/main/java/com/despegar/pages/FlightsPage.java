package com.despegar.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;
import java.util.stream.Collectors;

public class FlightsPage extends Page{

    @FindBy(id = "searchbox-sbox-all-boxes")
    private WebElement searchBox;
    @FindBy(className = "sbox-title-container-flights")
    private WebElement title;
    @FindBy(className = "sbox-place-container")
    private List<WebElement> placesContainer;
    @FindBy(className = "sbox-bind-reference-flight-roundtrip-origin-input")
    private WebElement originInput;
    @FindBy(className = "sbox-bind-error-flight-roundtrip-destination-empty")
    private WebElement destinyInput;
    @FindBy(className = "sbox-bind-reference-flight-start-date-input")
    private WebElement departureInput;
    @FindBy(className = "sbox-bind-reference-flight-end-date-input")
    private WebElement returnInput;
    @FindBy(className = "radio-label-container")
    private List<WebElement> radios;
    @FindBy(id = "searchbox-sbox-all-boxes")
    private WebElement passengers;
    @FindBy(css = ".sbox-button-default div.sbox-button-container")
    private WebElement searchButton;
    @FindBy(className = "as-login-close")
    private WebElement initialPopUp;
    @FindBy(className = "sbox-bind-checked-no-specified-date")
    private WebElement checkBox;
    private final String labelClass = "sbox-input-label";


    public FlightsPage(WebDriver driver) {
        super(driver);
    }

    public FlightsPage go(String url) {
        this.driver.get(url);
        return new FlightsPage(this.driver);
    }

    public ClustersPage search(String origin, String destiny) throws InterruptedException {
        this.closePopUp();
        this.setInputText(this.getOriginInput(),origin);
        this.setInputText(this.getDestinyInput(),destiny);
        this.clickCheckBox();
        this.clickSearch();

        return new ClustersPage(this.driver);
    }

    public void setInputText(WebElement input, String text) {
        actions.moveToElement(input);
        actions.click();
        actions.sendKeys(text);
        actions.build().perform();
        wait.until(ExpectedConditions.visibilityOf(input.findElement(By.xpath("..")).findElement(By.className("geo-autocomplete"))));
        wait.until(ExpectedConditions.elementToBeClickable(input.findElement(By.xpath("..")).findElement(By.className("geo-autocomplete"))));
        actions.sendKeys(Keys.DOWN);
        actions.build().perform();
        actions.sendKeys(Keys.ENTER);
        actions.build().perform();
    }

    public WebElement getSearchBox() {
        return searchBox;
    }

    private WebElement getTitle() {
        return title.findElement(By.className("sbox-title"));
    }

    private WebElement getOriginLabel(String label) {
        return placesContainer.stream().filter(place ->
                place.findElement(By.className(labelClass)).getText().equals(label)
        ).collect(Collectors.toList()).get(0).findElement(By.className(labelClass));
    }

    private WebElement getDestinyLabel(String label) {
        return placesContainer.stream().filter(place ->
                place.findElement(By.className(labelClass)).getText().equals(label)
        ).collect(Collectors.toList()).get(0).findElement(By.className(labelClass));
    }

    private WebElement getDepartureInput() {
        return departureInput;
    }

    private WebElement getReturnInput() {
        return returnInput;
    }

    private WebElement getJustGoRadio(String label) {
        return radios.stream().filter(radio ->
            radio.findElement(By.className("sbox-3-label-form")).getText().equals(label)
        ).collect(Collectors.toList()).get(0);
    }

    public String getJustGoRadioText(String label){
        return getJustGoRadio(label).getText();
    }

    public boolean justGoRadioIsDisplayed(String label){
        return getJustGoRadio(label).isDisplayed();
    }

    private WebElement getPassengers() {
        return passengers;
    }

    private WebElement getSearchButton() {
        return searchButton;
    }

    private void clickSearch(){
        this.getSearchButton().findElement(By.className("sbox-3-btn")).click();
    }

    private void closePopUp() {
        initialPopUp.click();
    }

    private WebElement getOriginInput() {
        return originInput;
    }

    private WebElement getDestinyInput() {
        return destinyInput;
    }

    private WebElement getCheckBox() {
        return checkBox;
    }

    private void clickCheckBox() {
        getCheckBox().click();
    }

    public boolean passengersIsDisplayed() {
        return this.getPassengers().isDisplayed();
    }

    public boolean searchButtonIsDisplay() {
        return this.getSearchButton().isDisplayed();
    }

    public String getTitleText() {
        return this.getTitle().getText();
    }

    public String getOriginLabelText(String origin) {
        return this.getOriginLabel(origin).getText();
    }

    public boolean originLabelIsDisplayed(String origin) {
        return this.getOriginLabel(origin).isDisplayed();
    }

    public String getDestinyLabelText(String destiny) {
        return this.getDestinyLabel(destiny).getText();
    }

    public boolean destinyLabelIsDisplayed(String destiny) {
        return this.getDestinyLabel(destiny).isDisplayed();
    }

    public String getDepartureInputPlaceholder() {
        return getDepartureInput().getAttribute("placeholder");
    }

    public boolean departureInputIsDisplayed() {
        return getDepartureInput().isDisplayed();
    }

    public String getReturnInputPlaceholder() {
        return getReturnInput().getAttribute("placeholder");
    }

    public boolean returnInputIsDisplayed() {
        return getReturnInput().isDisplayed();
    }
}
