package com.despegar.utils;

public class ParametersPack {

    private String from;
    private String to;
    private String departureDate;
    private String returnDate;
    private String adults;
    private String justBorn;
    private String kids;
    private String babies;

    public ParametersPack(String from, String to, String departureDate, String returnDate, String adults, String justBorn, String kids, String babies) {
        this.from = from;
        this.to = to;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
        this.returnDate = returnDate;
        this.adults = adults;
        this.justBorn = justBorn;
        this.babies = babies;
        this.kids = kids;
    }

    public ParametersPack() {

    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public String getAdults() {
        return adults;
    }

    public String getJustBorn() {
        return justBorn;
    }

    public String getKids() {
        return kids;
    }

    public String getBabies() {
        return babies;
    }
}
