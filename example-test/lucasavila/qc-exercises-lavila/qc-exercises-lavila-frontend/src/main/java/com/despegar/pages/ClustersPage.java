package com.despegar.pages;

import com.google.common.collect.Iterables;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.despegar.utils.ParametersPack;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ClustersPage extends Page {

    @FindBy(className = "cluster-CLUSTER")
    private List<WebElement> clusters;
    private static Logger LOGGER = LoggerFactory.getLogger(ClustersPage.class.getName());

    public ClustersPage(WebDriver driver) {
        super(driver);
    }

    public ClustersPage go(String mainUrl, ParametersPack parameters, String country) {
        this.driver.get(generateUrl(mainUrl, parameters));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("progress-bar-wrapper")));

        String popUpClass = "nevo-modal-header";
        if(!country.equals("AR") && driver.findElements(By.className(popUpClass)).size() != 0)
            wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.className(popUpClass))));

        return new ClustersPage(this.driver);
    }



    private String generateUrl(String mainUrl, ParametersPack parameters) {
        String finalUrl = String.format(mainUrl, parameters.getFrom(), parameters.getTo(),
                parameters.getDepartureDate(), parameters.getReturnDate(),
                parameters.getAdults(), "%s", "%s");

        Integer justBorn = Integer.parseInt(parameters.getJustBorn().trim());
        Integer kids = Integer.parseInt(parameters.getKids().trim());

        if(justBorn == 0 && kids == 0) {
            finalUrl = String.format(finalUrl, "0", parameters.getBabies());
            return finalUrl;
        }

        if(justBorn > 0 ) {
            String input = StringUtils.repeat("I", "-", justBorn);
            finalUrl = String.format(finalUrl, input, "%s");
        } else {
            finalUrl = String.format(finalUrl, "%s", "%s");
        }

        if(kids > 0 ) {
            String input = StringUtils.repeat("C", "-", kids);
            if(justBorn > 0)
                input = "-" + input;
            finalUrl = String.format(finalUrl, input, "%s");
        }

        finalUrl = String.format(finalUrl, parameters.getBabies());
        LOGGER.info("URL --->" + finalUrl);
        return finalUrl;
    }

    public List<Integer> getPrices(){
        List<String> pricesText = clusters.stream().map(cluster -> Iterables.getLast(cluster.findElements(By.tagName("flights-price")))
                .findElement(By.className("price-amount")).getText()
                .trim()
                .replace(".", "")
                .replace(",", ""))
                .filter(price -> !price.isEmpty())
                .collect(Collectors.toList());
        return pricesText.stream().map(price -> Integer.parseInt(price)).collect(Collectors.toList());
    }

    public CheckoutPage goFirstCheckout(){
        clusters.get(0).findElement(By.tagName("buy-button")).click();
        return new CheckoutPage(this.driver);
    }

    public void closePopUp() {
        String popUpClass = "nevo-modal-header";
        if(driver.findElements(By.className(popUpClass)).size() != 0)
            driver.findElement(By.className(popUpClass)).findElement(By.className("nevo-icon-close")).click();
    }

    public CheckoutPage goRandomCheckout() {
        int random = new Random().nextInt(9);
        WebElement buyButton = clusters.get(random).findElement(By.tagName("buy-button"));
        wait.until(ExpectedConditions.elementToBeClickable(buyButton));
        buyButton.click();
        return new CheckoutPage(this.driver);
    }

    public String getDirectFlights(String label) {
        return this.driver.findElements(By.tagName("checkbox-filter-item")).stream().filter(item ->
        item.findElement(By.tagName("em")).getText().equals(label))
                .collect(Collectors.toList())
                .get(0)
                .findElement(By.className("filters-quantity"))
                .getText();
    }
}
