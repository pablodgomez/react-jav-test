package com.despegar.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class SeleniumUtils {

    public static WebDriver buildDriver(Browser browser) throws Exception {
        WebDriver driver = null;

        switch (browser) {
            case FIREFOX:
                driver = new FirefoxDriver();
                break;
            case CHROME:
                // Si en caso no tienen seteado el chromedriver como variable de ambiente.
                // System.setProperty("webdriver.chrome.driver", "D:/install/chromedriver.exe");
                driver = new ChromeDriver();
                break;
            case IEXPLORE:
                driver = new InternetExplorerDriver();
                break;
            case HTML_UNIT:
                driver = new HtmlUnitDriver();
                break;

            default:
                throw new Exception("Browser not found!");
        }

        return driver;
    }

}