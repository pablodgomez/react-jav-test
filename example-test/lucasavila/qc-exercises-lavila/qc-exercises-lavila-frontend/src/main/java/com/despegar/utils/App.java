package com.despegar.utils;

public enum App {
    FLIGHTS("https://www.despegar.com.ar/vuelos/");

    private String url;

    private App(final String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

}