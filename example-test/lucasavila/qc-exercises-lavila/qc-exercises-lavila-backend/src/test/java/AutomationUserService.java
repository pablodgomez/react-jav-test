
import com.despegar.services.users.dto.response.*;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import com.despegar.services.users.UserException;
import com.despegar.services.users.UserService;
import com.despegar.services.users.dto.request.CreateUserRequestDTO;
import com.despegar.services.users.dto.request.LoginRequestDTO;
import com.despegar.services.users.dto.request.OfficeCode;

import static org.junit.Assert.*;

public class AutomationUserService {
    private static Logger LOGGER = LoggerFactory.getLogger(AutomationUserService.class.getName());
    public final static String USERS_APP_URL = "http://10.254.168.101:1458";
    public final static String EMAIL = "test@despegar.com";
    public final static String PASSWORD = "despegar";


    @Test
    public void testUsersFlow() {
        UserService userService = new UserService(USERS_APP_URL);

        // Creation //
        CreateUserRequestDTO signUpRequest = new CreateUserRequestDTO("example@despegar.com", "Lucas", "Male", "Avila", "",OfficeCode.DESPEGAR_AR, "1907");
        ResponseEntity<UserResponseDTO> creationResponse = userService.create(signUpRequest);

        UserDTO userCreated = creationResponse.getBody().getData();
        MetaDataDTO metaData = creationResponse.getBody().getMetaData();
        assertNotNull(userCreated);
        assertNotNull(metaData);
        assertEquals("User Created", metaData.getMessage());
        assertEquals(HttpStatus.OK, metaData.getCode());
        assertNotNull(userCreated.getId());
        assertTrue(!StringUtils.isEmpty(userCreated.getId()));

        String userId = userCreated.getId();

        LOGGER.info("User created successfully");

        // Login //
        LoginRequestDTO loginRequest = new LoginRequestDTO(EMAIL, PASSWORD);

        ResponseEntity<LoginResponseDTO> loginResponse = userService.loginUser(loginRequest);

        LoginDataDTO loginData = loginResponse.getBody().getData();
        assertNotNull(loginData);
        assertEquals("test@despegar.com", loginData.getEmail());
        assertTrue(loginResponse.getStatusCode().is2xxSuccessful());
        LOGGER.info("EMAIL ---> " + loginData.getEmail());
        LOGGER.info("HASH ---> " + loginData.getHash());
        LOGGER.info("Login done!");

        String hash = loginData.getHash();

        // Search //
        ResponseEntity<UserResponseDTO> searchResponse = userService.searchUser(userId, EMAIL, hash);
        UserDTO userSearched = searchResponse.getBody().getData();
        assertNotNull(userSearched.getId());
        assertTrue(!StringUtils.isEmpty(userSearched.getId()));
        assertEquals(userId, userSearched.getId());
        assertEquals("example@despegar.com", userSearched.getEmail());
        assertEquals("Lucas", userSearched.getFirstName());
        assertEquals("Male", userSearched.getGender());
        assertEquals("Avila", userSearched.getLastName());
        assertEquals(OfficeCode.DESPEGAR_AR, userSearched.getOfficeCode());
        LOGGER.info("The searchUser was glorious");

        // Delete //
        ResponseEntity<DeleteUserResponseDTO> deleteResponse = userService.delete(userId, EMAIL, hash);

        MetaDataDTO deletedData = deleteResponse.getBody().getMetaData();

        assertEquals(HttpStatus.OK, deletedData.getCode());
        assertEquals("User Deleted", deletedData.getMessage());
        LOGGER.info("Delete finished");
    }

    @Test
    public void createUserWithoutPassword(){
        UserService userService = new UserService(USERS_APP_URL);

        CreateUserRequestDTO signUpRequest = new CreateUserRequestDTO("example@despegar.com", "Lucas", "Male", "Avila", "",OfficeCode.DESPEGAR_AR, "");

        try {
            ResponseEntity<UserResponseDTO> creationResponse = userService.create(signUpRequest);
            fail("This case is an exception.");
        } catch (UserException ue) {
            LOGGER.info("User Exception: ", ue);
            assertNotNull(ue);
            assertEquals("Attribute password is empty.", ue.getMessage());
            assertEquals(HttpStatus.BAD_REQUEST, ue.getCode());
        }
    }

}