package com.despegar.services.flights.dto.response;

import java.util.List;
import java.util.Map;

public class BusquestsResponseDTO {

    private List<BusquestsClusterDTO> clusters;
    private ReferenceDataDTO referenceData;
    private int directFlightsCount;

    public List<BusquestsClusterDTO> getClusters() {
        return clusters;
    }

    public Map<String,String> getAirlinesNames() {
        return referenceData.getAirlinesNames();
    }

    public Map<String,String> getCities() {
        return referenceData.getCities();
    }

    public Map<String,String> getCitiesCodeByAirport() {
        return referenceData.getCitiesCodeByAirport();
    }

    public void setClusters(List<BusquestsClusterDTO> clusters) {
        this.clusters = clusters;
    }

    public ReferenceDataDTO getReferenceData() {
        return referenceData;
    }

    public void setReferenceData(ReferenceDataDTO referenceData) {
        this.referenceData = referenceData;
    }

    public int getDirectFlightsCount() {
        return directFlightsCount;
    }

    public void setDirectFlightsCount(int directFlightsCount) {
        this.directFlightsCount = directFlightsCount;
    }
}
