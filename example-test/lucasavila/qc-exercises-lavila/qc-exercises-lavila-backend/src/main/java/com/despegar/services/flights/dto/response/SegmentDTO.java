package com.despegar.services.flights.dto.response;

public class SegmentDTO {

    private FlightInformationDTO arrival;
    private FlightInformationDTO departure;
    private String airlineCode;
    private CabinType cabinType;
    private String duration;

    public FlightInformationDTO getArrival() {
        return arrival;
    }

    public void setArrival(FlightInformationDTO arrival) {
        this.arrival = arrival;
    }

    public FlightInformationDTO getDeparture() {
        return departure;
    }

    public void setDeparture(FlightInformationDTO departure) {
        this.departure = departure;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public CabinType getCabinType() {
        return cabinType;
    }

    public void setCabinType(CabinType cabinType) {
        this.cabinType = cabinType;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }


}
