package com.despegar.services.flights.dto.response;

import java.util.HashMap;

public class ReferenceDataDTO {

    private HashMap<String, String> airlines;
    private HashMap<String, String> cities;
    private HashMap<String, String> citiesCodeByAirport;

    public HashMap<String,String> getAirlinesNames() {
        return airlines;
    }

    public HashMap<String, String> getCities() {
        return cities;
    }

    public HashMap<String,String> getCitiesCodeByAirport() {
        return citiesCodeByAirport;
    }

    public HashMap<String, String> getAirlines() {
        return airlines;
    }

    public void setAirlines(HashMap<String, String> airlines) {
        this.airlines = airlines;
    }

    public void setCities(HashMap<String, String> cities) {
        this.cities = cities;
    }

    public void setCitiesCodeByAirport(HashMap<String, String> citiesCodeByAirport) {
        this.citiesCodeByAirport = citiesCodeByAirport;
    }
}
