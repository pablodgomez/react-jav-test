package com.despegar.services.users.dto.response;

import org.springframework.http.HttpStatus;

public class MetaDataDTO {

    private HttpStatus code;
    private String message;
    private String uow;

    public HttpStatus getCode() {
        return code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUow() {
        return uow;
    }

    public void setUow(String uow) {
        this.uow = uow;
    }
}
