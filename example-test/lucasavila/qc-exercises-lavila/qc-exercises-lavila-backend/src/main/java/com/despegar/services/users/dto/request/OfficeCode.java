package com.despegar.services.users.dto.request;

public enum OfficeCode {

    DESPEGAR_AR, DESPEGAR_BR, DESPEGAR_CL, DESPEGAR_CO, DESPEGAR_MX;
}
