package com.despegar.services.users.dto.response;

import org.springframework.http.HttpStatus;

public class ErrorUserResponseDTO {

    private MetaDataDTO metaData;

    public HttpStatus getCode(){
        return metaData.getCode();
    }

    public MetaDataDTO getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaDataDTO metaData) {
        this.metaData = metaData;
    }

    public String getMessage(){
        return metaData.getMessage();
    }

    public String getUow(){
        return metaData.getUow();
    }
}
