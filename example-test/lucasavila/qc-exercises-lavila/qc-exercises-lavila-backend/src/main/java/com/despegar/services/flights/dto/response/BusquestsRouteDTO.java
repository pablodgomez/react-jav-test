package com.despegar.services.flights.dto.response;

import java.util.Date;
import java.util.List;

public class BusquestsRouteDTO {

    private List<SegmentDTO> segments;
    private BusquestsBaggageInfoDTO baggageInfo;
    private List<String> airlines;
    private FlightInformationDTO departure;
    private FlightInformationDTO arrival;
    private int stopsCount;
    private String totalDuration;
    private CabinType highestCabinType;

    public int getBaggageQuantity(){
        return baggageInfo.getQuantity();
    }

    public List<String> getAirlines(){
        return airlines;
    }

    public String getDepartureAirportCode() {
        return departure.getAirportCode();
    }

    public String getArrivalAirportCode() {
        return arrival.getAirportCode();
    }

    public Date getDepartureDate() {
        return departure.getDate();
    }

    public Date getArrivalDate() {
        return arrival.getDate();
    }

    public String getTotalDuration() {
        return totalDuration;
    }

    public BusquestsBaggageInfoDTO getBaggageInfo() {
        return baggageInfo;
    }

    public List<SegmentDTO> getSegments() {
        return segments;
    }

    public void setSegments(List<SegmentDTO> segments) {
        this.segments = segments;
    }

    public void setBaggageInfo(BusquestsBaggageInfoDTO baggageInfo) {
        this.baggageInfo = baggageInfo;
    }

    public void setAirlines(List<String> airlines) {
        this.airlines = airlines;
    }

    public FlightInformationDTO getDeparture() {
        return departure;
    }

    public void setDeparture(FlightInformationDTO departure) {
        this.departure = departure;
    }

    public FlightInformationDTO getArrival() {
        return arrival;
    }

    public void setArrival(FlightInformationDTO arrival) {
        this.arrival = arrival;
    }

    public int getStopsCount() {
        return stopsCount;
    }

    public void setStopsCount(int stopsCount) {
        this.stopsCount = stopsCount;
    }

    public void setTotalDuration(String totalDuration) {
        this.totalDuration = totalDuration;
    }

    public CabinType getHighestCabinType() {
        return highestCabinType;
    }

    public void setHighestCabinType(CabinType highestCabinType) {
        this.highestCabinType = highestCabinType;
    }
}
