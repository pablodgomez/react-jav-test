package com.despegar.services.flights.dto.response;

public enum BaggageType {

    INCLUDED, WITHOUT_INFO, NOT_INCLUDED, NOT_INCLUDED_WITH_UPSELLING;
}
