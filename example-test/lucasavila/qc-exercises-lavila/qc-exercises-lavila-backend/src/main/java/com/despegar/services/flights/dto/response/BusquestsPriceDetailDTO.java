package com.despegar.services.flights.dto.response;

public class BusquestsPriceDetailDTO {

    private MainFareDTO mainFare;
    private TotalFareDTO totalFare;

    public double getAmountWithoutDiscount() {
        return mainFare.getAmountWithoutDiscount();
    }

    public double getAmount(){
        if(totalFare != null)
            return totalFare.getAmount();

        return mainFare.getAmount();
    }

    public MainFareDTO getMainFare() {
        return mainFare;
    }

    public void setMainFare(MainFareDTO mainFare) {
        this.mainFare = mainFare;
    }

    public TotalFareDTO getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(TotalFareDTO totalFare) {
        this.totalFare = totalFare;
    }
}
