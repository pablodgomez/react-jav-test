package com.despegar.services.users;

import com.despegar.services.users.dto.request.CreateUserRequestDTO;
import com.despegar.services.users.dto.request.LoginRequestDTO;
import com.despegar.services.users.dto.response.DeleteUserResponseDTO;
import com.despegar.services.users.dto.response.LoginResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import com.despegar.services.LoggingRequestInterceptor;
import com.despegar.services.users.dto.response.UserResponseDTO;
import com.despegar.utils.MappingMessageConverterUtils;

import java.util.ArrayList;
import java.util.List;

public class UserService {
    private static Logger LOGGER = LoggerFactory.getLogger(UserService.class.getName());

    private RestTemplate connector;
    public static final String SEARCH = "/manager/users/";
    public static final String LOGIN = "/access/loginUser/";
    private String url;

    public UserService(String url) {
        super();
        this.url = url;

        this.connector = new RestTemplate();

        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new LoggingRequestInterceptor());
        this.connector.setInterceptors(interceptors);

        this.connector.setErrorHandler(new UserExceptionHandler());

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(MappingMessageConverterUtils.getJacksonMessageConverterWithUnderscores());
        messageConverters.add(MappingMessageConverterUtils.getJacksonMessageConverter());
        this.connector.setMessageConverters(messageConverters);
    }

    public ResponseEntity<UserResponseDTO> searchUser(String id, String email, String hash) {
        String requestUrl = this.url + SEARCH + id;
        LOGGER.info("Request to Url (search): " + requestUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.add("email", email);
        headers.add("hash", hash);

        HttpEntity<?> request = new HttpEntity<>(headers);
        return this.connector.exchange(requestUrl, HttpMethod.GET, request, UserResponseDTO.class);
    }

    public ResponseEntity<LoginResponseDTO> loginUser(LoginRequestDTO loginRequest) {
        String requestUrl = this.url + LOGIN;
        LOGGER.info("Request to Url (login): " + requestUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<LoginRequestDTO> request = new HttpEntity<LoginRequestDTO>(loginRequest, headers);
        return this.connector.exchange(requestUrl, HttpMethod.POST, request, LoginResponseDTO.class);
    }

    public ResponseEntity<UserResponseDTO> create(CreateUserRequestDTO creationRequest) {
        String requestUrl = this.url + SEARCH;
        LOGGER.info("Request to Url: " + requestUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CreateUserRequestDTO> request = new HttpEntity<CreateUserRequestDTO>(creationRequest, headers);
        return this.connector.exchange(requestUrl, HttpMethod.POST, request, UserResponseDTO.class);
    }

    public ResponseEntity<DeleteUserResponseDTO> delete(String id, String email, String hash) {
        String requestUrl = this.url + SEARCH + id;
        LOGGER.info("Request to Url: " + requestUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.add("email", email);
        headers.add("hash", hash);

        HttpEntity<?> request = new HttpEntity<>(headers);
        return this.connector.exchange(requestUrl, HttpMethod.DELETE, request, DeleteUserResponseDTO.class);
    }

}