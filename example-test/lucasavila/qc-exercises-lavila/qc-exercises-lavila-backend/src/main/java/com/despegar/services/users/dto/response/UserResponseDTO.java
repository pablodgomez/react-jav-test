package com.despegar.services.users.dto.response;

public class UserResponseDTO {

    private UserDTO data;
    private MetaDataDTO metaData;

    public void setData(UserDTO data) {
        this.data = data;
    }

    public void setMetaData(MetaDataDTO metaData) {
        this.metaData = metaData;
    }

    public UserDTO getData() {
        return data;
    }

    public MetaDataDTO getMetaData() {
        return metaData;
    }
}
