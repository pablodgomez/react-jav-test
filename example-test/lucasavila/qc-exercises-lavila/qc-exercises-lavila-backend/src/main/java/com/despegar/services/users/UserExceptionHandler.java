package com.despegar.services.users;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.ResponseErrorHandler;
import com.despegar.services.users.dto.response.ErrorUserResponseDTO;

import java.io.IOException;

public class UserExceptionHandler implements ResponseErrorHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(UserExceptionHandler.class.getName());

    private ResponseErrorHandler errorHandler;


    public UserExceptionHandler() {
        super();
        this.errorHandler = new DefaultResponseErrorHandler();
    }

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        return this.errorHandler.hasError(response);
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        HttpHeaders headers = response.getHeaders();
        if (headers != null) {
            LOGGER.info("Headers Response:" + headers);
        }

        String stringResponse = IOUtils.toString(response.getBody(), "UTF-8");
        LOGGER.info("Json Response:" + stringResponse);

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);


        ErrorUserResponseDTO error = mapper.readValue(stringResponse, ErrorUserResponseDTO.class);

        throw new UserException(error);
    }
}