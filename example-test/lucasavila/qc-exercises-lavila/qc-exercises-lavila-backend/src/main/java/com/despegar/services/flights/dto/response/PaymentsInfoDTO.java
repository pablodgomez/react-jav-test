package com.despegar.services.flights.dto.response;

public class PaymentsInfoDTO {

    private String paymentsMessage;

    public String getPaymentsMessage() {
        return paymentsMessage;
    }
}
