package com.despegar.services.flights.dto.response;

public class MainFareDTO {

    private double amount;
    private double amountWithoutDiscount;

    public double getAmountWithoutDiscount() {
        return amountWithoutDiscount;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setAmountWithoutDiscount(double amountWithoutDiscount) {
        this.amountWithoutDiscount = amountWithoutDiscount;
    }
}
