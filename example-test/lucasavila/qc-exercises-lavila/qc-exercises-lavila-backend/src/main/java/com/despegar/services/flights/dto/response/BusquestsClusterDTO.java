package com.despegar.services.flights.dto.response;

import java.util.Date;
import java.util.List;

public class BusquestsClusterDTO {

    private List<ChoiceDTO> routeChoices;
    private BusquestsPriceDetailDTO priceDetail;
    private PaymentsInfoDTO paymentsInfo;

    public ChoiceDTO getOutboundChoice() {
        return routeChoices.get(0);
    }

    public ChoiceDTO getInboundChoice(){
         return routeChoices.get(1);
    }

    public boolean hasAReturnChoice() {
        return routeChoices.size() > 1;
    }

    public BusquestsPriceDetailDTO getPriceDetail() {
        return priceDetail;
    }

    public double getAmount(){
        return priceDetail.getAmount();
    }

    public String getPaymentsMessage() {
        return paymentsInfo.getPaymentsMessage();
    }

    public Date getDepartureDate(){
        return routeChoices.get(0).getDepartureDate();
    }

    public Date getArrivalDate(){
        return routeChoices.get(0).getArrivalDate();
    }

    public String getDepartureAirportCodeOutbound(){
        return routeChoices.get(0).getDepartureAirportCode();
    }

    public String getArrivalAirportCodeOutbound(){
        return routeChoices.get(0).getArrivalAirportCode();
    }

    public String getDepartureAirportCodeInbound(){
        return routeChoices.get(1).getDepartureAirportCode();
    }

    public String getArrivalAirportCodeInbound(){
        return routeChoices.get(1).getArrivalAirportCode();
    }

    public List<ChoiceDTO> getRouteChoices() {
        return routeChoices;
    }

    public void setRouteChoices(List<ChoiceDTO> routeChoices) {
        this.routeChoices = routeChoices;
    }

    public void setPriceDetail(BusquestsPriceDetailDTO priceDetail) {
        this.priceDetail = priceDetail;
    }

    public PaymentsInfoDTO getPaymentsInfo() {
        return paymentsInfo;
    }

    public void setPaymentsInfo(PaymentsInfoDTO paymentsInfo) {
        this.paymentsInfo = paymentsInfo;
    }
}
