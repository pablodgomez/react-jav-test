package com.despegar.services.users.dto.request;

public class CreateUserRequestDTO {

    private String email;
    private String firstName;
    private String gender;
    private String lastName;
    private String id;
    private OfficeCode officeCode;
    private String password;

    public CreateUserRequestDTO(String email, String firstName, String gender, String lastName, String id, OfficeCode officeCode, String password) {
        this.email = email;
        this.firstName = firstName;
        this.gender = gender;
        this.lastName = lastName;
        this.id = id;
        this.officeCode = officeCode;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public OfficeCode getOfficeCode() {
        return officeCode;
    }

    public void setOfficeCode(OfficeCode officeCode) {
        this.officeCode = officeCode;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
