package com.despegar.services.users;

import org.springframework.http.HttpStatus;
import com.despegar.services.users.dto.response.ErrorUserResponseDTO;

public class UserException extends RuntimeException {

    private HttpStatus code;
    private String message;
    private String uow;

    public UserException(ErrorUserResponseDTO errorResponseDTO) {
        super();
        this.code = errorResponseDTO.getCode();
        this.message = errorResponseDTO.getMessage();
        this.uow = errorResponseDTO.getUow();
    }

    public HttpStatus getCode() {
        return this.code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "GeneralException [code=" + this.code + ", message=" + this.message + ", uow=" + this.uow
                + "]";
    }
}