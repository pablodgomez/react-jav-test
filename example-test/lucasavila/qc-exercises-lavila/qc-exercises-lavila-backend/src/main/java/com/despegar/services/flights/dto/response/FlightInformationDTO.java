package com.despegar.services.flights.dto.response;

import java.util.Date;

public class FlightInformationDTO {

    private String airportCode;
    private Date date;

    public String getAirportCode() {
        return airportCode;
    }

    public Date getDate() {
        return date;
    }
}
