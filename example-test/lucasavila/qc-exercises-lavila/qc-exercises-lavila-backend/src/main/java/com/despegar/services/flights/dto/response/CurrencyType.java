package com.despegar.services.flights.dto.response;

public enum CurrencyType {

    ARS, USD;
}
