package com.despegar.services.users.dto.response;

public class LoginDataDTO {

    private String hash;
    private String email;

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
