package com.despegar.utils;

public class UrlParameters {

    private String site;
    private String channel;
    private String orderBy;
    private String currency;
    private String viewMode;
    private String language;
    private String limit;

    public UrlParameters(String site, String channel, String orderBy, String currency, String viewMode, String language, String limit) {
        this.site = site;
        this.channel = channel;
        this.orderBy = orderBy;
        this.currency = currency;
        this.viewMode = viewMode;
        this.language = language;
        this.limit = limit;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getViewMode() {
        return viewMode;
    }

    public void setViewMode(String viewMode) {
        this.viewMode = viewMode;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }
}
