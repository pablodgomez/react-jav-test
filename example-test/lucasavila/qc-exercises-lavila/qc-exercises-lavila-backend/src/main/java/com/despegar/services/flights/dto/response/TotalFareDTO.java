package com.despegar.services.flights.dto.response;

public class TotalFareDTO {

    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
