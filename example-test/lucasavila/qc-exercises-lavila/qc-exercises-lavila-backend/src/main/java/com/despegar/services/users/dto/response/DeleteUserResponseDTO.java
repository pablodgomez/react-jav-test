package com.despegar.services.users.dto.response;

public class DeleteUserResponseDTO {

    private MetaDataDTO metaData;

    public MetaDataDTO getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaDataDTO metaData) {
        this.metaData = metaData;
    }
}
