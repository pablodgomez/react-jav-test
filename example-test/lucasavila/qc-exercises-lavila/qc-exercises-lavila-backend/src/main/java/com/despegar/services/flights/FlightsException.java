package com.despegar.services.flights;

import org.springframework.http.HttpStatus;
import com.despegar.services.flights.dto.response.ErrorBusquestsResponseDTO;

public class FlightsException extends RuntimeException {

    private HttpStatus code;
    private String message;

    public FlightsException(ErrorBusquestsResponseDTO errorResponseDTO) {
        super();
        this.code = HttpStatus.valueOf(Integer.parseInt(errorResponseDTO.getStatus()));
        this.message = errorResponseDTO.getMessage();
    }

    public HttpStatus getCode() {
        return this.code;
    }

    public void setCode(HttpStatus code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "GeneralException [code=" + this.code + ", message=" + this.message + "]";
    }
}
