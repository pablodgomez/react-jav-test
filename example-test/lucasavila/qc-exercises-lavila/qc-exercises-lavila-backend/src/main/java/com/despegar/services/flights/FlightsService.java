package com.despegar.services.flights;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.despegar.services.LoggingRequestInterceptor;
import com.despegar.services.flights.dto.response.BusquestsResponseDTO;
import com.despegar.utils.MappingMessageConverterUtils;
import com.despegar.utils.ParametersPack;
import com.despegar.utils.UrlParameters;

import java.util.ArrayList;
import java.util.List;

@Service
public class FlightsService {

    private static Logger LOGGER = LoggerFactory.getLogger(FlightsService.class.getName());

    private RestTemplate connector;
    private final String PATH = "https://www.despegar.com.ar/shop/flights-busquets/api/v1/web/search?from=%s&to=%s&departureDate=%s&returnDate=%s&adults=%s&children=%s&infants=%s&site=%s&channel=%s&orderBy=%s&currency=%s&viewMode=%s&language=%s&limit=%s";

    public FlightsService() {
        super();

        this.connector = new RestTemplate();

        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();
        interceptors.add(new LoggingRequestInterceptor());
        this.connector.setInterceptors(interceptors);

        this.connector.setErrorHandler(new FlightsExceptionHandler());

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(MappingMessageConverterUtils.getJacksonMessageConverter());
        this.connector.setMessageConverters(messageConverters);
    }

    public ResponseEntity<BusquestsResponseDTO> search(ParametersPack tripInfo, UrlParameters urlInfo) {
        String requestUrl = generateRequestURL(tripInfo, urlInfo);
        LOGGER.info("Request to Url: " + requestUrl);

        HttpHeaders headers = new HttpHeaders();
        headers.add("XDESP-SF-DEBUG", "true");

        HttpEntity<?> request = new HttpEntity<>(headers);
        return this.connector.exchange(requestUrl, HttpMethod.GET, request, BusquestsResponseDTO.class);
    }

    public String generateRequestURL(ParametersPack tripInfo, UrlParameters urlInfo) {
        return String.format(this.PATH, tripInfo.getFrom(), tripInfo.getTo(), tripInfo.getDepartureDate(),
                tripInfo.getReturnDate(), tripInfo.getAdults(), tripInfo.getKids(), tripInfo.getBabies(),
                urlInfo.getSite(), urlInfo.getChannel(), urlInfo.getOrderBy(), urlInfo.getCurrency(),
                urlInfo.getViewMode(), urlInfo.getLanguage(), urlInfo.getLimit());
    }

}
