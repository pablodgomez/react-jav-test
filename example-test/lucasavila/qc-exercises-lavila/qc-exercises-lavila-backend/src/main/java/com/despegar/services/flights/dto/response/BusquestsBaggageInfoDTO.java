package com.despegar.services.flights.dto.response;

public class BusquestsBaggageInfoDTO {

    private int quantity;
    private BaggageType type;
    private BaggageType handbagType;

    public int getQuantity() {
        return quantity;
    }

    public BaggageType getType() {
        return type;
    }

    public BaggageType getHandbagType() {
        return handbagType;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setType(BaggageType type) {
        this.type = type;
    }

    public void setHandbagType(BaggageType handbagType) {
        this.handbagType = handbagType;
    }
}
