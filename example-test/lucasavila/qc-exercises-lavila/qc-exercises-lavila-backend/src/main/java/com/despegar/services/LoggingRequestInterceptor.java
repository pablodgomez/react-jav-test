package com.despegar.services;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class LoggingRequestInterceptor implements ClientHttpRequestInterceptor {

    private static Logger LOGGER = LoggerFactory.getLogger(LoggingRequestInterceptor.class.getName());

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException {

        ClientHttpResponse response = execution.execute(request, body);

        this.log(request, body, response);

        return response;
    }

    private void log(HttpRequest request, byte[] body, ClientHttpResponse response) {
        if (request != null) {
            LOGGER.info("Headers Request: " + request.getHeaders());
        }

        if (body != null) {
            String bodyAsString = new String(body, StandardCharsets.UTF_8);
            if (!bodyAsString.trim().isEmpty()) {
                LOGGER.info("Json Request: " + bodyAsString);
            }
        }
    }
}