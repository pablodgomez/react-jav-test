package com.despegar.services.flights.dto.response;

import java.util.Date;
import java.util.List;

public class ChoiceDTO {

    private List<BusquestsRouteDTO> routes;

    public List<BusquestsRouteDTO> getRoutes() {
        return routes;
    }

    public Date getDepartureDate() {
        return routes.get(0).getDepartureDate();
    }

    public Date getArrivalDate() {
        return routes.get(0).getArrivalDate();
    }

    public String getDepartureAirportCode() {
        return routes.get(0).getDepartureAirportCode();
    }

    public String getArrivalAirportCode() {
        return routes.get(0).getArrivalAirportCode();
    }

    public void setRoutes(List<BusquestsRouteDTO> routes) {
        this.routes = routes;
    }
}
