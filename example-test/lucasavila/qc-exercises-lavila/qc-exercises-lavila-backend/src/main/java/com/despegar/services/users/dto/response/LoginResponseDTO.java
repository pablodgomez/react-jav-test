package com.despegar.services.users.dto.response;

public class LoginResponseDTO {

    private MetaDataDTO metaData;
    private LoginDataDTO data;

    public MetaDataDTO getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaDataDTO metaData) {
        this.metaData = metaData;
    }

    public LoginDataDTO getData() {
        return data;
    }

    public void setData(LoginDataDTO data) {
        this.data = data;
    }
}
