public class CalculadorDePrecio {

    private static float iva = 0.21f;

    float calcularPrecio(Float precio, Cliente cliente){

        float precioFinal = cliente.calcularPrecio(precio);

        return precioFinal+ (precioFinal*iva);
    }
}
