public class Pasajero extends TipoCliente{

    private float indice;
    private float comision;

    public float calcularPrecio(float precio) {
        return super.calcularPrecio(precio)+this.getComision();
    }

    public float getComision() {
        return 6;
    }

    public float getIndice() {
        return 0;
    }
}
