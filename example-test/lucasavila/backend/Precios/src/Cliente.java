public class Cliente {

    private String nombre;
    private TipoCliente tipo;

    public Cliente(TipoCliente tipo){
        this.tipo = tipo;
    }

    public float calcularPrecio(float precio){
        return this.tipo.calcularPrecio(precio);
    };

    public void setTipo(TipoCliente tipo) {
        this.tipo = tipo;
    }

}
