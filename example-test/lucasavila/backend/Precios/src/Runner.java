public class Runner {

    public static void main(String args[]){

        CalculadorDePrecio unCalculador = new CalculadorDePrecio();
        Cliente pasajero = new Cliente(new Pasajero());
        Cliente empleadoD = new Cliente(new EmpleadoDespegar());

        System.out.println("El precio del pasajero es: " + unCalculador.calcularPrecio(300f, pasajero));
        System.out.println("El precio del empleado es: " + unCalculador.calcularPrecio(300f, empleadoD));
    }
}
