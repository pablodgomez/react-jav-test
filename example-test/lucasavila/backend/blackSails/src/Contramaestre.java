public class Contramaestre extends Especialidad {

    @Override
    public double calcularCoraje(Embarcacion embarcacion) {
        return this.corajeBase + ( embarcacion.corajeTripulantes() * 0.1 );
    }
}
