import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class Embarcacion {

    private Tripulante capitan;
    private Tripulante contramaestre;
    private List<Tripulante> tripulantes;
    private List<Cañon> cañones;
    private Posicion posicion;
    private double botin;

    public Embarcacion(List<Tripulante> tripulantes, List<Cañon> cañones, Posicion posicion, double botin){
        this.tripulantes = tripulantes;
        this.cañones = cañones;
        this.posicion = posicion;
        this.botin = botin;
    }

    public double daño(){
        return 0;
    }

    public double dañoTotal(){
        double dañoCañones = cañones.stream().mapToDouble(cañon -> cañon.calcularDaño()).sum();
        double dañoTripulacion = tripulantes.stream().mapToDouble(tripulante -> tripulante.coraje(this)).sum() + capitan.coraje(this) + contramaestre.coraje(this);
        return dañoCañones + dañoTripulacion;
    }

    public double corajeTripulantes(){
        return tripulantes.stream().mapToDouble(tripulante -> tripulante.coraje(this)).sum();
    }

    public void aumentarCorajeBaseTripulacion(double coraje) {
        capitan.aumentarCorajeBase(coraje);
        contramaestre.aumentarCorajeBase(coraje);
        tripulantes.stream().forEach(tripulante -> tripulante.aumentarCorajeBase(coraje));
    }

    public void matarCobardes() {
        this.ordenarSegunCoraje();
        tripulantes.subList(tripulantes.size() - 3, tripulantes.size()).clear();
    }

    public Tripulante getCapitan() {
        return this.capitan;
    }

    public void cambiarCapitan(Tripulante capitan) {
        this.capitan = capitan;
    }

    public void ordenarSegunCoraje(){
        tripulantes.sort(Comparator.comparing(Tripulante::getCorajeBase));
    }

    public Tripulante masCorajudo() {
        this.ordenarSegunCoraje();
        return tripulantes.get(0);
    }

    public void cambiarContramaestre(Tripulante tripulante) {
        this.contramaestre = tripulante;
        tripulantes.subList(0, 1).clear();
    }

    public Posicion getPosicion() {
        return posicion;
    }

    public boolean enMismoOceanoQue(Embarcacion segunda) {
        return this.posicion.getOceano() == segunda.getPosicion().getOceano();
    }

    public boolean tieneHabilNegociador() {
        return capitan.esHabilNegociador() || contramaestre.esHabilNegociador() ||
                tripulantes.stream().anyMatch(tripulante -> tripulante.esHabilNegociador());
    }

    public void perderMitadBotin(Embarcacion primera) {
        primera.aumentarBotin(this.botin/2);
        this.botin -= this.botin/2;
    }

    private void aumentarBotin(double botin) {
        this.botin += botin;
    }

    public void pasarTripulantesDominantes(Embarcacion segunda) {
        this.ordenarSegunCoraje();
        segunda.agregarTripulantes(tripulantes.subList(0, 3));
    }

    private void agregarTripulantes(List<Tripulante> tripulantesNuevos) {
        this.tripulantes.addAll(tripulantesNuevos);
    }

    public void generarMotin() {
        if(capitan.coraje(this) < contramaestre.coraje(this)){
            this.capitan = this.contramaestre;
            this.contramaestre = this.masCorajudo();
        } else {
            this.resistirMotin();
        }
    }

    private void resistirMotin() {
        if(this.porcentajeRandom() < 80){
            List<ArmamentoDeMano> armas = new ArrayList<ArmamentoDeMano>();
            armas.add(new Espada(1));
            this.contramaestre.cambiarEspecilidad(new Pirata(armas));
        } else {
            this.contramaestre.cambiarEspecilidad(new Cocinero(new Cuchillo(), new Cuchillo()));

        }
        tripulantes.add(this.contramaestre);
        this.contramaestre = this.masCorajudo();
    }

    private int porcentajeRandom() {
        Random random = new Random();
        return random.nextInt();
    }

    public void envejecerCañones(Integer antiguedad) {
        this.cañones.stream().forEach(cañon -> cañon.envejecer(antiguedad));
    }

    public void disminuirCoraje(double cobardiaInducida) {
        this.capitan.disminuirCoraje(cobardiaInducida);
        this.contramaestre.disminuirCoraje(cobardiaInducida);
        tripulantes.stream().forEach(tripulante -> tripulante.disminuirCoraje(cobardiaInducida));
    }

    public void matarMasCorajudos(int cantidadAMatar) {
        this.ordenarSegunCoraje();
        tripulantes.subList(0, cantidadAMatar).clear();
    }
}
