import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClasicaBatalla extends Contienda {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClasicaBatalla.class);

    @Override
    protected boolean puedeTomar(Embarcacion primera, Embarcacion segunda) {
        return primera.dañoTotal() > segunda.dañoTotal();
    }

    @Override
    public void tomar(Embarcacion primera, Embarcacion segunda) {
        primera.aumentarCorajeBaseTripulacion(5);
        LOGGER.info("La tripulacion de la primera embarcación ha aumentado su coraje base en 5");
        segunda.matarCobardes();
        LOGGER.info("Se mataron los cobardes de la segunda embarcación");
        segunda.cambiarCapitan(primera.getCapitan());
        LOGGER.info("Se cambió el capitan de la segunda embarcación.");
        primera.cambiarContramaestre(primera.masCorajudo());
        LOGGER.info("Se cambió e contramaestre de la primera embarcación.");
        primera.pasarTripulantesDominantes(segunda);
        LOGGER.info("Se pasaron los 3 tripulantes de la primera más corajudos a la segunda embarcación.");
    }

}
