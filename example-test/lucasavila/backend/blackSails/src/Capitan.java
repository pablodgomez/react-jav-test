public class Capitan extends Especialidad {

    @Override
    public double calcularCoraje(Embarcacion embarcacion) {
        return this.corajeBase + (embarcacion.daño() * 0.5);
    }
}
