public class Posicion {

    String oceano;
    Integer x;
    Integer y;

    public Posicion(String oceano, Integer x, Integer y){
        this.oceano = oceano;
        this.x = x;
        this.y = y;
    }

    public String getOceano() {
        return oceano;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }
}
