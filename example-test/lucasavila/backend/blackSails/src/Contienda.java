import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Contienda {

    private static double valorConfigurable;
    private static final Logger LOGGER = LoggerFactory.getLogger(Contienda.class);

    protected boolean puedeEntrarEnConflicto(Embarcacion primera, Embarcacion segunda){
        return valorConfigurable > this.diferenciaDeCoordenadas(primera.getPosicion(), segunda.getPosicion()) && mismoOceano(primera, segunda);
    }

    protected boolean mismoOceano(Embarcacion primera, Embarcacion segunda){
        return primera.enMismoOceanoQue(segunda);
    }

    protected double diferenciaDeCoordenadas(Posicion primera, Posicion segunda){
        return Math.sqrt(Math.pow(primera.getX() - segunda.getX(), 2) + Math.pow(primera.getY() - segunda.getY(), 2));
    }

    protected abstract boolean puedeTomar(Embarcacion primera, Embarcacion segunda);

    protected abstract void tomar(Embarcacion primera, Embarcacion segunda);

    public void intentarTomar(Embarcacion primera, Embarcacion segunda){
        if(this.puedeEntrarEnConflicto(primera, segunda) && this.puedeTomar(primera, segunda)){
            LOGGER.info("La toma se puede realizar.");
            this.tomar(primera, segunda);
        } else {
            LOGGER.error("La toma no se pudo realizar");
            throw new ContiendaInvalidaException
                    ("La batalla no se puede realizar porque estan muy lejos o la primera es muy debil :(.");
        }
    }

    public void setValorConfigurable(double valorConfigurable) {
        this.valorConfigurable = valorConfigurable;
    }
}
