public class BallenaAzul extends Bestia {

    @Override
    public void atacar(Embarcacion embarcacion) {
        embarcacion.envejecerCañones(8);
    }
}
