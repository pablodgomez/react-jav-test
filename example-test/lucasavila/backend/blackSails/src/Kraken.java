public class Kraken extends Bestia{

    private static Kraken self;
    private final double daño = 100000;

    private Kraken(){}

    public static Kraken getInstance(){
        if(self == null){
            self = new Kraken();
        }
        return self;
    }

    @Override
    public void atacar(Embarcacion embarcacion) {
        embarcacion.matarMasCorajudos(5);
    }
}
