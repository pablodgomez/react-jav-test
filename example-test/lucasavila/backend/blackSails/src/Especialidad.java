public abstract class Especialidad {

    protected double corajeBase;

    public abstract  double calcularCoraje(Embarcacion embarcacion);

    public double getCorajeBase() {
        return this.corajeBase;
    }

    public void aumentarCorajeBase(double coraje){
        this.corajeBase += coraje;
    };

    public void disminuirCoraje(double cobardia){
        this.corajeBase -= cobardia;
    }
}
