public abstract class Bestia {

    private double nivelFuerza;

    public void puedeAtacar(Embarcacion embarcacion){
        if(nivelFuerza > embarcacion.dañoTotal()){
            this.atacar(embarcacion);
        }
    }

    public abstract void atacar(Embarcacion embarcacion);
}
