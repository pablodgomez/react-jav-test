public class Pistola extends ArmamentoDeMano{

    private double calibre;
    private double dureza;

    public Pistola(double calibre, double dureza){
        this.calibre = calibre;
        this.dureza = dureza;
    }

    @Override
    public double calcularDaño() {
        return this.calibre * this.dureza;
    }
}
