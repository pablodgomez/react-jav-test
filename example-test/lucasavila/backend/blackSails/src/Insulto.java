public class Insulto extends ArmamentoDeMano{

    private double cantPalabras;
    private Pirata pirata;

    public Insulto(double cantPalabras, Pirata pirata){
        this.cantPalabras = cantPalabras;
        this.pirata = pirata;
    }

    @Override
    public double calcularDaño() {
        return cantPalabras * this.pirata.getCorajeBase();
    }
}
