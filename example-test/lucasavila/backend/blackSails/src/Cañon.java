public class Cañon implements Armamento {

    private double nivelDaño = 350;
    private Integer antiguedad;

    public double calcularDaño() {
        return nivelDaño - (0.1 * nivelDaño * antiguedad);
    }

    public void setNivelDaño(double nivelDaño) {
        this.nivelDaño = nivelDaño;
    }

    public void envejecer(Integer antiguedad){
        this.antiguedad += antiguedad;
    }
}
