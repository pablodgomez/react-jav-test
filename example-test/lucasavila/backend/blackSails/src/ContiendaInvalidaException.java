public class ContiendaInvalidaException extends RuntimeException {

    public ContiendaInvalidaException(String message) {
        super(message);
    }
}
