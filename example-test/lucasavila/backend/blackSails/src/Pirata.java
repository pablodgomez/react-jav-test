import java.util.List;

public class Pirata extends Especialidad {

    private List<ArmamentoDeMano> armas;

    public Pirata(List<ArmamentoDeMano> armas){
        this.armas = armas;
    }

    @Override
    public double calcularCoraje(Embarcacion embarcacion) {
        double dañoArmas = this.armas.stream().mapToDouble(arma -> arma.calcularDaño()).sum();
        return this.corajeBase + dañoArmas;
    }
}
