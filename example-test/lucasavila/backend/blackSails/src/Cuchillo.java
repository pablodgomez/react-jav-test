public class Cuchillo extends ArmamentoDeMano {

    private static double daño;

    @Override
    public double calcularDaño() {
        return this.daño;
    }

    public void setDaño(double daño){
        this.daño = daño;
    }
}
