import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Negociacion extends Contienda {

    private static final Logger LOGGER = LoggerFactory.getLogger(Negociacion.class);

    @Override
    public boolean puedeTomar(Embarcacion primera, Embarcacion segunda) {
        return primera.tieneHabilNegociador();
    }

    @Override
    public void tomar(Embarcacion primera, Embarcacion segunda) {
        segunda.perderMitadBotin(primera);
        LOGGER.info("La segunda embarcacion ha perdido la mitad de su botín");
    }
}
