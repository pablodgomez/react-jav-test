public class Tiburones extends Bestia {

    private double cobardiaInducida;

    public Tiburones(double cobardiaInducida){
        this.cobardiaInducida = cobardiaInducida;
    }

    @Override
    public void atacar(Embarcacion embarcacion) {
        embarcacion.disminuirCoraje(this.cobardiaInducida);
    }
}
