public class Tripulante {

    private Especialidad especialidad;
    private double inteligencia;

    public double coraje(Embarcacion embarcacion){
        return especialidad.calcularCoraje(embarcacion);
    }

    public void aumentarCorajeBase(double coraje){
        this.especialidad.aumentarCorajeBase(coraje);
    }

    public double getCorajeBase(){
        return this.especialidad.getCorajeBase();
    }

    public boolean esHabilNegociador() {
        return inteligencia > 50;
    }

    public void cambiarEspecilidad(Especialidad especialidad) {
        this.especialidad = especialidad;
    }

    public void disminuirCoraje(double cobardia){
        this.especialidad.disminuirCoraje(cobardia);
    }
}
