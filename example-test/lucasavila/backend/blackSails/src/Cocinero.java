public class Cocinero extends Especialidad {

    private Cuchillo unCuchillo;
    private Cuchillo otroCuchillo;

    public Cocinero(Cuchillo unCuchillo, Cuchillo otroCuchillo){
        this.unCuchillo = unCuchillo;
        this.otroCuchillo = otroCuchillo;
    }

    @Override
    public double calcularCoraje(Embarcacion embarcacion) {
        return this.corajeBase + unCuchillo.calcularDaño() + otroCuchillo.calcularDaño();
    }
}
