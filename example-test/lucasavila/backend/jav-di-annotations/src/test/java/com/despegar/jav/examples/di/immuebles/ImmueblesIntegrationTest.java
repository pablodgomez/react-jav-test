package com.despegar.jav.examples.di.immuebles;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ImmueblesIntegrationTest {
    private ImmueblesApp immueblesApp = Main.buildApp();

    @Test
    void publicar_cuandoElRequestEstaCompleto_respondePublicacionAceptada() {
        // ¿Esta bueno se ingeniso con los datos de test?
        String request =
                "command=publicar " +
                        "amb=2 " +
                        "m2=30 " +
                        "monto=100000 " +
                        "direccion=JuanaManso900 " +
                        "vendedor=PepePropiedades";
        String response = immueblesApp.process(request);
        assertTrue(response.startsWith("Publicacion Acceptada"));
    }

    @Test
    void publicar_cuandoElRequestEstaCompleto_loAgregaAlasPublicaciones() {
        // Arrange
        String requestPublicar =
                "command=publicar " +
                        "amb=2 " +
                        "m2=30 " +
                        "monto=100000 " +
                        "descripcion=MuyLuminiso " +
                        "direccion=JuanaManso900 " +
                        "vendedor=PepePropiedades";
        immueblesApp.process(requestPublicar);
        // Act
        String request = "command=buscar";
        String response = immueblesApp.process(request);
        // Assert
        assertEquals("[Publicacion{ambientes=2," +
                        " metrosCuadrados=30," +
                        " descripcion='MuyLuminiso'," +
                        " direccion=JuanaManso900," +
                        " vendedor=PepePropiedades," +
                        " monto=100000}]"
                , response);
    }

    @Test
    void publicar_cuandoElRequestEstaCompleto_loAgregaALasOrdenesDeCobro() {
        // Arrange
        String requestPublicar =
                "command=publicar " +
                        "amb=2 " +
                        "m2=30 " +
                        "monto=100000 " +
                        "descripcion=MuyLuminiso " +
                        "direccion=JuanaManso900 " +
                        "vendedor=PepePropiedades";
        immueblesApp.process(requestPublicar);
        // Act
        String request = "command=cobros";
        String response = immueblesApp.process(request);
        // Assert
        assertEquals("[OrdenCobro{" +
                        "publicacion=Publicacion{ambientes=2," +
                        " metrosCuadrados=30," +
                        " descripcion='MuyLuminiso'," +
                        " direccion=JuanaManso900," +
                        " vendedor=PepePropiedades," +
                        " monto=100000}," +
                        " monto=10}]"
                , response);
    }

}