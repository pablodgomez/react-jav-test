package com.despegar.jav.examples.di.immuebles.commands;

import com.despegar.jav.examples.di.immuebles.domain.cobros.Cobrador;
import com.despegar.jav.examples.di.immuebles.domain.cobros.model.OrdenCobro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class VerCobros implements Command {

    @Autowired
    private final Cobrador cobrador;

    public VerCobros(Cobrador cobrador) {
        this.cobrador = cobrador;
    }

    public String apply(Map<String, String> request) {
        List<OrdenCobro> ordenes = cobrador.verCobros();
        return ordenes.toString();
    }

}
