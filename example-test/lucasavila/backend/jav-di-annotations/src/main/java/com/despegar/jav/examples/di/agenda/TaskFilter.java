package com.despegar.jav.examples.di.agenda;

public interface TaskFilter {

    boolean pass(Task task);

}
