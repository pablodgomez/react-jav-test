package com.despegar.jav.examples.di.immuebles.domain.database;

import com.despegar.jav.examples.di.immuebles.domain.publicacion.model.Publicacion;
import org.springframework.stereotype.Component;

@Component
public class PublicacionDao extends Dao<Publicacion>{
}
