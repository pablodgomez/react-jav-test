package com.despegar.jav.examples.di.immuebles.domain.database;

import com.despegar.jav.examples.di.immuebles.domain.cobros.model.OrdenCobro;
import org.springframework.stereotype.Component;

@Component
public class OrdenDeCobroDao extends Dao<OrdenCobro> {
}
