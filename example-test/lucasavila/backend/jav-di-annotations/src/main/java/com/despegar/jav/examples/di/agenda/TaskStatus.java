package com.despegar.jav.examples.di.agenda;

public enum TaskStatus {
    TODO, DONE, CANCEL
}
