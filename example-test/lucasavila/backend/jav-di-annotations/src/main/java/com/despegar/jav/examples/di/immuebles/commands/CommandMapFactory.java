package com.despegar.jav.examples.di.immuebles.commands;

import java.util.HashMap;
import java.util.Map;

public class CommandMapFactory {

    public static Map<String, Command> createCommandMap(Buscar buscarCmd
            , Publicar publicarCmd
            , VerCobros verCobrosCmd) {
        Map<String, Command> commands = new HashMap<>();
        commands.put("buscar", buscarCmd);
        commands.put("publicar", publicarCmd);
        commands.put("cobros", verCobrosCmd);
        return commands;
    }
}
