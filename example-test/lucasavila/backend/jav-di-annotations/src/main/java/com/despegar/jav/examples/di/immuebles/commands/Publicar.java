package com.despegar.jav.examples.di.immuebles.commands;

import com.despegar.jav.examples.di.immuebles.domain.publicacion.Publicador;
import com.despegar.jav.examples.di.immuebles.domain.publicacion.model.Publicacion;
import com.despegar.jav.examples.di.immuebles.parser.RequestParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class Publicar implements Command {

    @Autowired
    private final Publicador publicador;
    @Autowired
    private final RequestParser parser;

    public Publicar(Publicador publicador, RequestParser parser) {
        this.publicador = publicador;
        this.parser = parser;
    }

    public String apply(Map<String, String> request) {
        Publicacion publicacion = new Publicacion(
                parser.requiredInt(request, "amb")
                , parser.requiredBigDecimal(request, "monto")
                , parser.requiredInt(request, "m2")
                , request.get("descripcion")
                , parser.requiredString(request, "direccion")
                , parser.requiredString(request, "vendedor"));

        String id = this.publicador.publicar(publicacion);
        return "Publicacion Acceptada" + id;
    }

}
