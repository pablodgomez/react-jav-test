package com.despegar.jav.examples.di.agenda;

import com.despegar.jav.examples.di.agenda.filters.AnyTaskFilter;
import com.despegar.jav.examples.di.agenda.filters.DueAfterFilter;
import com.despegar.jav.examples.di.agenda.filters.TaskStatusFilter;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDateTime;

public class Main {

    public static void main(String[] args) {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("application-context.xml");

        Agenda agenda = applicationContext.getBean(Agenda.class);
        // Tambien puedo obtener el bean por nombre
        // Agenda agenda = (Agenda) applicationContext.getBean("agenda");

        if (args[1].equals("add")) {
            Task task = new Task(
                    args[2] // description
                    , LocalDateTime.parse(args[3]) // due
                    , TaskStatus.valueOf(args[4])  // status
            );
            agenda.save(task);
        } else if (args[1].equals("list")) {
            TaskFilter filter = new AnyTaskFilter();
            if (args[2].equals("from")) {
                LocalDateTime from = LocalDateTime.parse(args[3]);
                filter = new DueAfterFilter(from);
            } else if (args[2].equals("status")) {
                TaskStatus status = TaskStatus.valueOf(args[3]);
                filter = new TaskStatusFilter(status);
            }
            agenda.filter(filter);
        }
    }
}
