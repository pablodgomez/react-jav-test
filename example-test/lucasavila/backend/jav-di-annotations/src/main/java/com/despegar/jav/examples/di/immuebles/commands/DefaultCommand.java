package com.despegar.jav.examples.di.immuebles.commands;

import java.util.Map;

public class DefaultCommand implements Command {
    public String apply(Map<String, String> request) {
        return "Unknown command:" + request;
    }
}
