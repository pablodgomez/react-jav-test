package com.despegar.jav.examples.di.agenda.filters;

import com.despegar.jav.examples.di.agenda.Task;
import com.despegar.jav.examples.di.agenda.TaskFilter;

public class AnyTaskFilter implements TaskFilter {
    @Override
    public boolean pass(Task task) {
        return true;
    }
}
