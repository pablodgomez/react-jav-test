import java.util.List;

public class Shopping implements Establecimiento{

    private List<Local> locales;

    public Shopping(List<Local> locales){
        this.locales = locales;
    }

    public Integer cantidadVentasEn(String lugar){
        return this.locales.stream().mapToInt(local -> local.cantidadVentasEn(lugar)).sum();
    }

    @Override
    public float dineroMovido() {
        return (float) this.locales.stream().mapToDouble(local -> local.dineroMovido()).sum();
    }
}
