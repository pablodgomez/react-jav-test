import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Local implements Establecimiento{

    private List<Venta> ventas;
    private String ubicacion;

    public Local(String ubicacion){
        this.ubicacion = ubicacion;
    }

    public Integer cantidadVentasEn(String lugar){
        return ventas.stream().filter(venta -> venta.getLugar() == lugar).collect(Collectors.toList()).size();
    }

    public boolean alMenosUnaPromocionVendida(){
        return ventas.stream().anyMatch(
                venta -> venta.huboPromocion());
    }

    public List<Venta> ventasEn(Date fecha){
        return ventas.stream().filter(venta -> venta.getFecha()
                .equals(fecha))
                .collect(Collectors.toList());
    }

    public float calcularAhorro(Date fecha){
        List<Venta> ventasConAhorro = this.ventasEn(fecha);
        return (float) ventasConAhorro.stream().mapToDouble(
                venta -> venta.montoAhorrado()).sum();
    }

    @Override
    public float dineroMovido() {
        return (float) this.ventas.stream().mapToDouble(venta -> venta.montoVenta()).sum();
    }
}
