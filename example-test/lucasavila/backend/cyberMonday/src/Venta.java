import java.util.Date;
import java.util.List;

public class Venta {

    private List<Producto> productos;
    private Date fecha;
    private String lugar;

    public Venta(List<Producto> productos, Date fecha, String lugar){
        this.productos = productos;
        this.fecha = fecha;
        this.lugar = lugar;
    }

    public float montoVenta(){
        float monto = 0;
        for(Producto producto : productos){
            monto += producto.calcularPrecio();
        }
        return monto;
    }

    public float montoAhorrado(){
        return (float) this.productos.stream().mapToDouble(producto -> producto.dineroAhorrado()).sum();
    }

    public List<Producto> getProductos() {
        return this.productos;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public String getLugar() {
        return this.lugar;
    }

    public boolean huboPromocion() {
        return this.productos.stream()
                .anyMatch(producto -> producto.getPromocion());
    }
}
