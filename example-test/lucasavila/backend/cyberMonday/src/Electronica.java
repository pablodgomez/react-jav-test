public class Electronica extends Producto {

    private final Integer constante = 15;
    private static float factor;

    public Electronica(float factor){
        this.factor = factor;
    }

    public void setFactor(float factor){
        this.factor = factor;
    }

    @Override
    public float calcularCosto() {
        return constante * factor;
    }
}
