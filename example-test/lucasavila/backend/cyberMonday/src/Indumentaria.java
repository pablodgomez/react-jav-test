public class Indumentaria extends Producto {

    private Integer talle;
    private float factor;

    public Indumentaria(Integer talle, float factor){
        this.talle = talle;
        this.factor = factor;
    }

    @Override
    public float calcularCosto() {
        return this.talle * this.factor;
    }
}
