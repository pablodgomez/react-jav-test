public class Material {

    private float costo;

    public Material(float costo){
        this.costo = costo;
    }

    public float getCosto() {
        return this.costo;
    }
}
