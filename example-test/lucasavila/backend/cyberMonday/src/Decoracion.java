import java.util.ArrayList;
import java.util.List;

public class Decoracion extends Producto {

    private float peso;
    private float ancho;
    private float alto;
    private List<Material> materiales;

    public Decoracion(Material material, float peso, float ancho, float alto){
        this.peso = peso;
        this.ancho = ancho;
        this.alto = alto;
        List<Material> materiales = new ArrayList<>();
        materiales.add(material);
    }

    public void agregarMaterial(Material material){
        materiales.add(material);
    }

    @Override
    public float calcularCosto() {
        double costosMateriales = materiales.stream().mapToDouble(mat -> mat.getCosto()).sum();
        return this.peso*this.ancho*this.alto + (float) costosMateriales;
    }
}
