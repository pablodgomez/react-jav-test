import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Empresa {

    private List<Establecimiento> establecimientos;
    private List<String> lugares;

    public Empresa(List<Establecimiento> establecimientos){
        this.establecimientos = establecimientos;
    }

    public String lugarMasExitoso(){
        Map<String, Float> lugaresConVentas = new HashMap<String, Float>();
        lugaresConVentas = this.generarMapa(lugaresConVentas);
        return this.obtenerMaxKey(lugaresConVentas);
    }

    private Map<String,Float> generarMapa(Map<String, Float> lugaresConVentas) {
        double ventasLugar;
        for(String lugar : lugares){
            ventasLugar = establecimientos
                    .stream()
                    .mapToDouble(establecimiento -> establecimiento.cantidadVentasEn(lugar))
                    .sum();
            lugaresConVentas.put(lugar, (float) ventasLugar);
        }

        return lugaresConVentas;
    }

    private String obtenerMaxKey(Map<String, Float> map) {
        String lugar = new String();
        float ventasMax = 0;
        for (Map.Entry<String, Float> entry : map.entrySet()) {
            if (entry.getValue() > ventasMax) {
                lugar = entry.getKey();
                ventasMax = entry.getValue();
            }
        }
        return lugar;
    }

    public boolean esDeAvaro(String lugar){
        //List<Establecimiento> candidatos = filtrar por el lugar...;

        // candidatos.stream().allMatch(establecimiento -> establecimiento.
        // Falta metodo para consultar por todas las ventas del establecimiento que
        // hayan sido en promoción...);
        return true;
    }
}
