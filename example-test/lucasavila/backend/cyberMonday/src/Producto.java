public abstract class Producto {

    private boolean promocion;
    private float descuento;

    public float calcularPrecio(){
        return this.aplicarDescuento(this.calcularCosto());
    }

    private float aplicarDescuento(float costo){
        float precio = costo;
        if(promocion){
            precio = costo * this.descuento;
        }
        return precio;
    }

    public abstract float calcularCosto();

    public void setPromocion(boolean promocion) {
        this.promocion = promocion;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public boolean getPromocion(){
        return this.promocion;
    }

    public float getDescuento() {
        return this.descuento;
    }

    public float dineroAhorrado(){
        float costo = this.calcularCosto();
        return costo - (costo - this.getDescuento() * costo);
    }
}
