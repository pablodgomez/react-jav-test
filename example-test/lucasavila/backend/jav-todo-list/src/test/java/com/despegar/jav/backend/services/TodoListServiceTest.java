package com.despegar.jav.backend.services;

import com.despegar.jav.backend.datasources.TodoDao;
import com.despegar.jav.backend.model.Todo;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TodoListServiceTest {
    @Test
    public void testGetAll() {
        TodoDao todoDao = mock(TodoDao.class);
        List<Todo> todoList = Arrays.asList(new Todo("asdf", "asdf"));
        when(todoDao.getAll()).thenReturn(todoList);
        TodoListService todoListService = new TodoListService(todoDao);
        Assert.assertEquals(todoList, todoListService.getTodoList());
    }
}
