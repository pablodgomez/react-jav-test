package com.despegar.jav.backend.services;

import com.despegar.jav.backend.datasources.TodoDao;
import com.despegar.jav.backend.model.Todo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoListService {
    Logger LOGGER= LoggerFactory.getLogger(TodoListService.class);

    @Autowired
    private TodoDao todoDao;

    public TodoListService(TodoDao todoDao) {
        this.todoDao = todoDao;
    }

    public List<Todo> getTodoList(){
        LOGGER.info("retrieving todolist");
        return todoDao.getAll();
    }

    public List<Todo> getTodoListIn(String date) {
        LOGGER.info("retrieving todolist with specific date");
        return todoDao.getAllFrom(date);
    }

    public void addTodo(Todo todo){
        LOGGER.info("adding new todo");
        todoDao.add(todo);
    }
}
