package com.despegar.jav.backend;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ComponentScan
public class MainApp implements WebMvcConfigurer {
    public static void main(String[] argv) throws Exception {
        //Contexto de Spring que va a tener toda las definiciones de los beans.
        AnnotationConfigWebApplicationContext webContext = new AnnotationConfigWebApplicationContext();
        webContext.register(MainApp.class);

        // creo el jetty embedded
        Server server = new Server(8080);
        ServletContextHandler servletContextHandler = new ServletContextHandler();
        ServletHolder servletHolder = new ServletHolder(new DispatcherServlet(webContext));
        servletContextHandler.addServlet(servletHolder, "/*");

        server.setHandler(servletContextHandler);
        server.start();
        server.join();
    }
}
