package com.despegar.jav.backend.controllers;

import com.despegar.jav.backend.model.Todo;
import com.despegar.jav.backend.services.TodoListService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;

@Controller
public class TodoListController {

    @Autowired  // Para inyectar la dependencia.
    private TodoListService todoListService;

    @RequestMapping(value = "/todo-list", method = RequestMethod.GET, produces = "text/plain")
    //produces indica que en el header en content-type va a poner text/plain
    @ResponseBody
    public String getAllTodos() {
        return todoListService
                .getTodoList()
                .stream()
                .map(t->t.getUser() + " - " + t.getCreationDate() + ": " + t.getText())
                .collect(Collectors.joining("\n"));
    }

    @RequestMapping(value = "/todo-list/{date}", method = RequestMethod.GET, produces = "text/plain") //si se cumple esto, spring ejecurta getalltodos
    @ResponseBody //lo que spring le devuelve al usuario
    public String getAllTodosIn(@PathVariable String date) {
        return todoListService.getTodoListIn(date).stream().map(t->t.getCreationDate() + ": " + t.getText())
                .collect(Collectors.joining("\n"));
    }

    @RequestMapping(value = "/todo-list", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void insertTodo(@RequestParam("user") String user,
                             @RequestParam("text") String text){
        todoListService.addTodo(new Todo(user, text));
    }


}
