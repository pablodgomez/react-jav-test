package com.despegar.jav.backend.model;

import java.util.Date;

/* plain old Java object (POJO)
 * Sin comportamiento
 */
public class Todo {
    private String creationDate;
    private String text;
    private String user;

    public Todo() {
    }

    public Todo(String user, String text) {
        this.user = user;
        this.text = text;
        this.creationDate = new Date().toString();
    }

    public Todo(String user, String creationDate, String text) {
        this.user = user;
        this.text = text;
        this.creationDate = creationDate;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getText() {
        return text;
    }

    public String getUser() {
        return user;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Todo todo = (Todo) o;

        if (creationDate != null ? !creationDate.equals(todo.creationDate) : todo.creationDate != null) return false;
        return text != null ? text.equals(todo.text) : todo.text == null;
    }

    @Override
    public int hashCode() {
        int result = creationDate != null ? creationDate.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Todo{" +
                "creationDate='" + creationDate + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
