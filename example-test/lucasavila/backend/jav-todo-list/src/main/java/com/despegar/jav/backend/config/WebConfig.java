package com.despegar.jav.backend.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
public class WebConfig  implements WebMvcConfigurer {
    /*public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        System.out.println("registering jackson message converter");
        converters.add(new MappingJackson2HttpMessageConverter());
    }*/

}
