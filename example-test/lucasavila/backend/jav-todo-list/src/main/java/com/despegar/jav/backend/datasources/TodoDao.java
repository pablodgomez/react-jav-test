package com.despegar.jav.backend.datasources;

import com.despegar.jav.backend.model.Todo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Repository
public class TodoDao {
    private List<Todo> todos = new ArrayList<Todo>();
    {{
        todos.add(new Todo("admin", "2018-02-06 12:00:00","fixme amigo"));
        todos.add(new Todo("admin","2018-02-06 13:00:00","agregar comportamiento"));
        todos.add(new Todo("admin","2018-02-05 12:00:00","hello old friend"));
        todos.add(new Todo("admin","2018-02-05 13:00:00","darkness"));
        todos.add(new Todo("admin","2018-02-05 12:00:00","i'm batman"));
        todos.add(new Todo("admin","2018-02-05 13:00:00","GG"));
        todos.add(new Todo("admin","2018-02-04 12:00:00","Super"));
        todos.add(new Todo("admin","2018-02-04 13:00:00","Genius"));
        todos.add(new Todo("admin","2018-02-03 12:00:00","Be water my friend"));
        todos.add(new Todo("admin","2018-02-03 13:00:00","potato"));
    }}

    public List<Todo> getAll(){
        return todos;
    }

    public void add(Todo todo){
        todos.add(todo);
    }

    public List<Todo> getAllFrom(String date) {
        return todos.stream().filter(todo -> todo.getCreationDate().contains(date))
                .collect(Collectors.toList());
    }
}
