# Ancillaries (Trabajo Integrador Backend)

<p align="center">
  <a href="http://www.despegar.com">
    <img src="https://runwaygirlnetwork.com/wp-content/uploads/2015/12/62528848_thumbnail-800x415.jpg">
  </a>
</p>

<p align="center">
  <a href="https://github.com/despegar/jav-2018-q1-lucasavila/blob/develop/backend/Ancillaries/README.md#Introduction">
    Introduction
  </a>
</p>
<p align="center">
    <a href="https://github.com/despegar/jav-2018-q1-lucasavila/blob/develop/backend/Ancillaries/README.md#project-structure">
    Project Structure
  </a>
</p>
<p align="center">
    <a href="https://github.com/despegar/jav-2018-q1-lucasavila/blob/develop/backend/Ancillaries/README.md#how-to-use-it">
    How to use it
  </a>
</p>
<p align="center">
    <a href="https://github.com/despegar/jav-2018-q1-lucasavila/blob/develop/backend/Ancillaries/README.md#design-choices">
    Design Choices
  </a>
</p>

## Introduction

This project was built based on the information you can find right <a href="https://drive.google.com/file/d/1RrujmkWPnWnk0DQP0dfOjDocVvXflctx/view?usp=sharing">here</a>
. The project has four parts. 
* Part I: Ancillaries - Generate better itineraries for the clients based on their type.
* Part II: Booking - Be able to book a certain itinerary and its ancillaries.
* Part III: Reports - Generate a report to make better decisions in the future.
* Part IV: Bonus - Add a new client type to the system. 

### Project structure

The project structure is as follows: 
* <b>Connectors</b> - Here you'll be able to find all the connectors that the application uses. Their main functionality is to send and HTTP
request to a certain api and return the response of it. 
* <b>Controllers</b>
* <b>Exceptions</b> - Custom exceptions to provide more specfic information when something happens. 
* <b>Model</b> - All the main logic of the application will be here. From the itineraries to the booking elements that we need to use.
* <b>Mappers</b> - Useful to map from one thing to another. For example: transform the DTOs to objects from our model.
* <b>Services</b> - All entities that belong to another service (such as Antares) will be here. Mainly DTOs.
* <b>Utils</b> - Some handy classes such as validators. 

### How to use it

You will be able to find the inner mechanics in the document share at the beginning of this README.

#### Part I: Itineraries.

If you want the best itinerary you can get you should run the app and do the following:

```http
GET http://localhost:8080/despegar-it-jav/itinerary
parameters:
○ from: "BUE"
○ to: "MIA"
○ from date: "2017-07-12"
○ to date: "2017-07-20" (opcional si se requiere vuelta)
○ client type: "CAUTIOUS"
```
You'll get a JSON as a response to your request.

#### Part II: Booking.

To book an itinerary you have to run the app and do:

```http
GET http://localhost:8080/despegar-it-jav/bookings
parameters:
○ from: "BUE"
○ to: "MIA"
○ from date: "2017-07-12"
○ to date: "2017-07-20" (opcional si se requiere vuelta)
○ client type: "CAUTIOUS"
○ passenger: "Lucas Avila"
```
You'll get a JSON as a response to your request with the booking details.
Maybe it would be better to be a POST instead of a GET.

#### Part III: Reports.

This project does not persist data so... to see this part in action you should book an itinerary (run part II) prior to this:

```http
GET http://localhost:8080/despegar-it-jav/ancillaries/report
parameters:
○ from: "BUE"
○ to: "MIA"
○ month: "2018-07"
```
You'll get a JSON as a response to your request with the reports.


```diff
-WARNING: all the parameters use snake_case. So, for instance, if you want to specify the client type you should use "client_type". However, the passenger name admits spaces ' '.
```

## Design Choices

The following items are different design decisions made along the way. 

* ~~Singletons: there are several classes that are singletons. Among them we can find the Connectors, Mappers, 
Builders and Client Types. The main reason for this is that we only need one to work with. It's unneccesary more, at least for now.~~
Finally it was pretty complicate to work with Singletons. Especially when I tried to test or make mocks. So instead I ended up using Spring to instantiate everything. 
* The client types were at first implemented with enums. But then the behaviour was getting bigger and bigger. Finally the responsabilities
that the client types hold ended up being too much for just a simple enum. So I decided to use an abstract class and inheritance.
* To_date is optional. I think it's better to delegate the responsability to see if it exists or not in the connector. At the moment that the URL
is being generated to send the request, we checked if "toDate" is empty or not. 
* In case of an error, an ErrorMessage is generated to show the problem in a json. 
* To check the parameters, there is a ParamsValidator. It's true that the other applications make validations but I believe it doesn't
hurt to check them once more time. Besides, if something is wrong we stop the flow inmediatly and we don't have to wait until a service
tells us to. 
* For the tests, there are jsons files saved in the resources location. They are pretty useful to make the code look cleaner and more
understandable. 
* For the only repository in the project, SoldAncillariesRepository, I used a LinkedList. I think it's better for this case than ArrayList because we don't know how many space we will need for the reports and due to how the project is designed, there won't be any deleting from the elements of the list.  
* JodaTime is used instead of default Java 8 to manage dates. It was easier for me to work with JodaTime. 
* The recommendations are requested by airport, not by city. So in the request I send the from and to from the route. Example: if the initial request is "BUE" to "MIA" in the recommendations request would be "EZE" for "BUE". 
* The reports repository has AncillariesRequest items instead of the final object (Report) that its shown to the user. This is done to gain flexibility. Maybe we would want to filter or group by a different parameter so instead of directly saving one report, I saved all the sales and when requested I filter what I need. The disadvantage is the space "wasted" to save all the ancillaries sales.   
