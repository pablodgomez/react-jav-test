package com.despegar.javAncillaries.utils;

import com.despegar.javAncillaries.exceptions.ParameterException;
import com.despegar.javAncillaries.model.clients.ClientType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component
public class ParamsValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParamsValidator.class);

    public void checkInputParams(String from, String to, String fromDate, String toDate){
        checkPlace(from);
        checkPlace(to);
        checkDate(fromDate);
        if(!StringUtils.isEmpty(toDate)) {
            checkDatesCombination(fromDate, toDate);
        }
    }

    private void checkDatesCombination(String fromDate, String toDate) {
        Date firstDate = this.getDate(fromDate);
        Date secondDate = this.getDate(toDate);

        if(firstDate.compareTo(secondDate) > 0) {
            LOGGER.error("The to_date is in the past. Impossible to make the journey, sorry.");
            throw new ParameterException("You can't fly into the past.");
        }
    }

    private void checkDate(String date) {
        if(StringUtils.isEmpty(date)) {
            LOGGER.error("You need to specify the departure date. The arrival date is optional.");
            throw new ParameterException("You need to specify the departure date. The arrival date is optional.");
        }
        this.isAFutureDate(date);
    }

    private void isAFutureDate(String date) {
        Date now = new Date();
        Date flightDate = this.getDate(date);

        if(now.compareTo(flightDate) > 0) {
            LOGGER.error("The from_date is too old to perform the operation.");
            throw new ParameterException("Cannot perform a booking in the past (the from_date is too old).");
        }
    }

    private Date getDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        try {
            return dateFormat.parse(date);
        } catch (ParseException pe) {
            LOGGER.error("The date " + date + " does not match the format yyyy-MM-dd or is an invalid date.", pe);
            throw new ParameterException("The date " + date + " does not match the format yyyy-MM-dd or is an invalid date.", pe);
        }
    }

    public void checkPlace(String location) {
        if(StringUtils.isEmpty(location)){
            LOGGER.error("You need to specify the departure and destiny location.");
            throw new ParameterException("You need to specify the departure and destiny location.");
        }
        if(location.length() != 3) {
            LOGGER.error("The locations must be 3 characters long.");
            throw new ParameterException("The locations must be 3 characters long.");
        }
        if(!location.matches("[a-zA-Z]+")) {
            LOGGER.error("The locations must follow the regex '[a-zA-Z]+'");
            throw new ParameterException("The locations must follow the regex '[a-zA-Z]+'");
        }
        if(!isAValidLocation(location)) {
            LOGGER.error("The location '" + location + "' is not a valid one.");
            throw new ParameterException("The location '" + location + "' is not a valid one.");
        }
    }

    //TODO - When locations repository is available.
    private boolean isAValidLocation(String location) {
        HashMap<String, String> codes = new HashMap<>();
        //return codes.containsValue(location);
        return true;
    }

    public void checkName(String completeName) {
        if(!completeName.contains(" ")) {
            LOGGER.error("We need the complete name of the passenger " + completeName);
            throw new ParameterException("We need the complete name of the passenger " + completeName);
        }
    }

    public void checkClientType(String clientType) {
        try {
            ClientType.valueOf(clientType.toUpperCase());
        }catch (IllegalArgumentException iae) {
            throw new ParameterException("The client type '" + clientType + "' does not exist.");
        }
    }

    public void checkMonth(String month) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM");
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(month);
        } catch (ParseException pe) {
            LOGGER.error("Month parsing error.", pe);
            throw new ParameterException("The month does not match the format yyyy-mm.", pe);
        }
    }
}
