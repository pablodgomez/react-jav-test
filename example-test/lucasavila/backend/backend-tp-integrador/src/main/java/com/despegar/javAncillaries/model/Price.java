package com.despegar.javAncillaries.model;

import com.google.gson.annotations.Expose;

public class Price {

    @Expose
    private double itinerary;
    @Expose
    private double additionalAncillaries;
    @Expose
    private double total;

    public Price(double itinerary) {
        this.itinerary = itinerary;
        this.additionalAncillaries = 0;
        this.total = itinerary;
    }

    public Price(double itinerary, double additionalAncillaries) {
        this.itinerary = itinerary;
        this.additionalAncillaries = additionalAncillaries;
        this.total = itinerary + additionalAncillaries;
    }

    public double getItinerary() {
        return itinerary;
    }

    public double getTotal() {
        return this.itinerary+this.additionalAncillaries;
    }


}