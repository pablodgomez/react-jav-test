package com.despegar.javAncillaries.model.ancillaries;

public interface Ancillary {

    int getQuantity();
}
