package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceDetail {

    @Expose
    private double total;

    public double getTotal() {
        return total;
    }
}

