package com.despegar.javAncillaries.exceptions;

public class AncillariesBookingException extends RuntimeException {


    public AncillariesBookingException(String s, RuntimeException hre) {
        super(s, hre);
    }

    public AncillariesBookingException(String s) {
        super(s);
    }

}
