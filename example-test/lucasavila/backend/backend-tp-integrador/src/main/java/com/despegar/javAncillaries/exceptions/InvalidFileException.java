package com.despegar.javAncillaries.exceptions;

import java.io.IOException;

public class InvalidFileException extends RuntimeException {
    public InvalidFileException(String message, IOException ioe) {
        super(message, ioe);
    }
}
