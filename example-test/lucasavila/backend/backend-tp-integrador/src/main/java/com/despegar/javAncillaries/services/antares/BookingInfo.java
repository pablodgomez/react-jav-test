package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingInfo {

    @SerializedName("itinerary_id")
    @Expose
    private String itineraryId;

    public void setItineraryId(String itineraryId) {
        this.itineraryId = itineraryId;
    }

    public String getItineraryId() {
        return itineraryId;
    }
}