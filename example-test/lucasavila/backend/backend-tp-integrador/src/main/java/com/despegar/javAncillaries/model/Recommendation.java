package com.despegar.javAncillaries.model;

import com.despegar.javAncillaries.model.ancillaries.AncillaryType;

public class Recommendation {

    private AncillaryType type;
    private int quantity;
    private double unitCharge;

    public Recommendation(AncillaryType type, int quantity, double unitCharge) {
        this.type = type;
        this.quantity = quantity;
        this.unitCharge = unitCharge;
    }

    public void setType(AncillaryType type) {
        this.type = type;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setUnitCharge(double unitCharge) {
        this.unitCharge = unitCharge;
    }

    public AncillaryType getType() { return type; }

    public int getQuantity() {
        return quantity;
    }

    public double getUnitCharge() {
        return unitCharge;
    }
}
