package com.despegar.javAncillaries.mappers;

import com.despegar.javAncillaries.exceptions.RecommendationException;
import com.despegar.javAncillaries.model.*;
import com.despegar.javAncillaries.model.ancillaries.*;
import com.despegar.javAncillaries.services.antares.*;
import com.despegar.javAncillaries.services.recommendations.RecommendationResponse;
import com.google.common.collect.Iterables;
import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class MapperToModel {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapperToModel.class);

    public List<Recommendation> transformToRecommendation(List<RecommendationResponse> recommendations){
        LOGGER.info("Mapping recommendations.");
        List<Recommendation> mappedList =  new ArrayList<>();
        recommendations.stream().forEach(reco -> this.mapToRecommendation(reco, mappedList));

        return mappedList;
    }

    private void mapToRecommendation(RecommendationResponse reco, List<Recommendation> list) {
        if(EnumUtils.isValidEnum(AncillaryType.class, reco.getType().toUpperCase())){
            list.add(
                    new Recommendation(AncillaryType.valueOf(reco.getType().toUpperCase()),
                                                             reco.getQuantity(),
                                                             reco.getUnitCharge()));
        } else {
            // If not do nothing | Unknown type --> Recommendation is not created.
            LOGGER.info("The recommendation type is unknown (" + reco.getType() + "). Then this recommendation is not going to be mapped.");
        }
    }

    public List<ItineraryResponse> transformAntaresToModel(List<AntaresItinerary> itineraries){
        LOGGER.info("Mapping Antares Responses to Itinerary Responses.");
        return itineraries.stream()
                .map(itinerary -> this.mapFromAntaresToModel(itinerary))
                .collect(Collectors.toList());
    }

    private ItineraryResponse mapFromAntaresToModel(AntaresItinerary itinerary){
        List<Route> routes = this.getRoutes(itinerary);
        Price price = this.getPrice(itinerary.getPriceDetail());
        return new ItineraryResponse(itinerary.getId(), itinerary.getValidatingCarrier(), routes, price);
    }

    private Price getPrice(PriceDetail priceDetail) {
        return new Price(priceDetail.getTotal());
    }

    private List<Route> getRoutes(AntaresItinerary itinerary) {
        List<Route> routes = new ArrayList<>();
        Route outRoute;
        Route inRoute;

        outRoute = this.handleSegments(itinerary.getOutboundSegments(), itinerary.getOutboundChoices());
        routes.add(outRoute);

        if(!itinerary.getInboundSegments().isEmpty()) {
            inRoute = this.handleSegments(itinerary.getInboundSegments(), itinerary.getInboundChoices());
            routes.add(inRoute);
        }
        else
            LOGGER.info("There aren't any inbound segments for this itinerary ID: "
                    + itinerary.getItineraryId());

        return routes;
    }

    private Route handleSegments(List<Segment> segments, List<Choice> choices){
        Segment firstSegment = segments.get(0);
        Segment lastSegment = Iterables.getLast(segments);
        return this.createRoute(firstSegment, lastSegment, choices.get(0));
    }

    private Route createRoute(Segment starterSegment, Segment finalSegment, Choice choice) {
        return new Route(starterSegment.getFrom(),
                finalSegment.getTo(),
                choice.getDuration(),
                starterSegment.getDepartureDatetime(),
                finalSegment.getArrivalDatetime(),
                this.createAncillaries(choice),
                new Ancillaries());
    }

    private Ancillaries createAncillaries(Choice choice) {
        return new Ancillaries(new Baggage(choice.getBaggage().getQuantity()),
                new Meal(choice.getMeal().getQuantity()),
                new Entertainment(choice.getMeal().getQuantity()));
    }

}
