package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AntaresResponse {

    @SerializedName("items")
    @Expose
    private List<AntaresItinerary> itineraries;

    public AntaresResponse(List<AntaresItinerary> antaresItineraries) {
        this.itineraries = antaresItineraries;
    }

    public List<AntaresItinerary> getItineraries() {
        return itineraries;
    }

    public void setItineraries(List<AntaresItinerary> itineraries) {
        this.itineraries = itineraries;
    }

}
