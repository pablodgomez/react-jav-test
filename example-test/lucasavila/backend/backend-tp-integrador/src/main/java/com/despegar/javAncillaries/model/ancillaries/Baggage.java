package com.despegar.javAncillaries.model.ancillaries;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Baggage implements Ancillary {

    @SerializedName("quantity")
    @Expose
    private int quantity;

    public Baggage(int quantity){
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void add(int quantity) {
        this.quantity += quantity;
    }
}