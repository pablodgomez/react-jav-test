package com.despegar.javAncillaries.services.ancillaries;

import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.AncillaryType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

public class AncillariesRequest {

    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @Expose
    private String from;
    @Expose
    private String to;
    @Expose
    private AncillaryType type;
    @Expose
    private int quantity;
    private LocalDate date;

    public AncillariesRequest(String bookingId, String from, String to, AncillaryType type, int quantity) {
        this.bookingId = bookingId;
        this.from = from;
        this.to = to;
        this.type = type;
        this.quantity = quantity;
    }

    public AncillariesRequest(String from, String to, LocalDate date, AncillaryType type, int quantity) {
        this.from = from;
        this.to = to;
        this.type = type;
        this.quantity = quantity;
        this.date = date;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public AncillaryType getType() {
        return type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getBookingId() {
        return bookingId;
    }
}
