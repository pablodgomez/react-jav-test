package com.despegar.javAncillaries.connectors;

import com.despegar.javAncillaries.configurations.AppProperties;
import com.despegar.javAncillaries.services.recommendations.RecommendationResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Component
public class RecommendationsConnector extends Connector{

    @Value("${app.recommendationsUrl}")
    private String recommendationsUrl;
    @Autowired
    private Gson gson;
    private static final Logger LOGGER = LoggerFactory.getLogger(RecommendationsConnector.class);

    public List<RecommendationResponse> getResponse(String from, String to){

        String finalUrl = this.appendInfo(from, to);
        LOGGER.info(String.format("Sending request to Recommendations. Parameters: %s, %s", from, to));
        String json = sendRequest(Request.Get(finalUrl).addHeader("X-User-Id", "lucas.avila@despegar.com:ba104f2f-b930-4bfd-860e-c5827f77c32a"));
        LOGGER.info("Got Recommendations response. JSON: " + json);
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();
        return gson.fromJson(json, recommendationsType);
    }

    private String appendInfo(String from, String to) {
        LOGGER.info("Appended info to the URL: " + String.format(this.recommendationsUrl, from, to));
        return String.format(this.recommendationsUrl, from, to);
    }
}
