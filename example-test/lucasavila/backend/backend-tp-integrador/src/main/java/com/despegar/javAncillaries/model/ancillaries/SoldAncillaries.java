package com.despegar.javAncillaries.model.ancillaries;

import com.google.gson.annotations.Expose;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import java.util.Date;

public class SoldAncillaries {

    @Expose
    private String from;
    @Expose
    private String to;
    @Expose
    private LocalDate date;
    @Expose
    private int baggages;
    @Expose
    private int meals;
    @Expose
    private int entertainments;

    public SoldAncillaries(String from, String to, LocalDate date) {
        this.from = from;
        this.to = to;
        this.date = date;
        this.baggages = 0;
        this.meals = 0;
        this.entertainments = 0;
    }

    public void addBaggages(int quantity){
        this.baggages += quantity;
    }

    public void addMeals(int quantity){
        this.meals += quantity;
    }

    public void addEntertainments(int quantity){
        this.entertainments += quantity;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public int getBaggages() {
        return baggages;
    }

    public int getEntertainments() {
        return entertainments;
    }

    public int getMeals() {
        return meals;
    }
}
