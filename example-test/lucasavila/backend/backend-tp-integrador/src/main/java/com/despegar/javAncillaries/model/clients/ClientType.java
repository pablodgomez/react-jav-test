package com.despegar.javAncillaries.model.clients;

import com.despegar.javAncillaries.exceptions.RecommendationException;
import com.despegar.javAncillaries.model.*;
import com.despegar.javAncillaries.model.ancillaries.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;


public enum ClientType {

    AUSTERE {
        public AncillariesAndPrice setBestAncillariesMatch(Route route, List<Recommendation> recommendations) {
        /*Nothing to do here because this type
          of client doesn't add anything else
          to his flight.*/
            return new AncillariesAndPrice(new Ancillaries(), 0);
        }
    },
    ANXIOUS {
        public AncillariesAndPrice setBestAncillariesMatch(Route route, List<Recommendation> recommendationPack) {
            //Filter meals.
            recommendationPack = recommendationPack.stream()
                    .filter(reco -> reco.getType().equals(AncillaryType.MEAL))
                    .collect(Collectors.toList());
            return this.obtainFinalAncillaries(route, recommendationPack);
            //Creating route.
        }
    },
    STANDARD {
        public AncillariesAndPrice setBestAncillariesMatch(Route route, List<Recommendation> recommendationPack) {
            //Creating route.
            return this.obtainFinalAncillaries(route, recommendationPack);
        }
    },
    CAUTIOUS {
        public AncillariesAndPrice setBestAncillariesMatch(Route route, List<Recommendation> recommendationPack) {
            AncillariesAndPrice additionals = this.obtainFinalAncillaries(route, recommendationPack);
            return this.takeAnExtraSuitcase(additionals, recommendationPack);
        }

        private AncillariesAndPrice takeAnExtraSuitcase(AncillariesAndPrice ancillaries, List<Recommendation> recommendations) {
            List<Recommendation> recoFiltered = recommendations.stream().filter(reco -> reco.getType().equals(AncillaryType.BAGGAGE)).collect(Collectors.toList());

            if (recoFiltered.isEmpty()) {
                throw new RecommendationException("Baggage is empty, cannot infer the price.");
            }
            double totalPrice = ancillaries.getTotalPrice() + recoFiltered.get(0).getUnitCharge();
            return new AncillariesAndPrice(this.generateNewAncillary(ancillaries.getAncillaries(), AncillaryType.BAGGAGE, 1), totalPrice);
        }
    };

    /****** GENERAL BEHAVIOUR ******/

    public abstract AncillariesAndPrice setBestAncillariesMatch(Route route, List<Recommendation> recommendationPack);

    protected AncillariesAndPrice obtainFinalAncillaries(Route route, List<Recommendation> recommendationPack) {
        AncillariesAndPrice ancillariesAndPrice = new AncillariesAndPrice(new Ancillaries(), 0);
        recommendationPack = recommendationPack.stream()
                .filter(reco -> this.isSuitableToBeApplied(reco, route))
                .collect(Collectors.toList());
        // For every single reco suitable to be applied we obtain the AncillariesAndPrice updated.
        for (Recommendation reco : recommendationPack) {
            ancillariesAndPrice = this.applyAncillary(route, reco, ancillariesAndPrice);
        }

        return ancillariesAndPrice;
    }

    protected AncillariesAndPrice applyAncillary(Route route, Recommendation reco, AncillariesAndPrice ancillariesAndPrice) {
        int difference = reco.getQuantity() - route.getAncillaryQuantity(reco.getType());
        double totalPrice = ancillariesAndPrice.getTotalPrice() + difference * reco.getUnitCharge();
        return new AncillariesAndPrice(this.generateNewAncillary(ancillariesAndPrice.getAncillaries(), reco.getType(), difference), totalPrice);
    }

    protected Ancillaries generateNewAncillary(Ancillaries previousAncillaries, AncillaryType type, int quantity) {
        Ancillaries ancillaries = new Ancillaries();

        switch (type) {
            case BAGGAGE:
                return new Ancillaries(new Baggage(quantity + previousAncillaries.getSuitcasesQuantity()),
                        previousAncillaries.getMeal(), previousAncillaries.getEntertainment());
            case MEAL:
                return new Ancillaries(previousAncillaries.getBaggage(),
                        new Meal(quantity + previousAncillaries.getMealsQuantity()),
                        previousAncillaries.getEntertainment());
            case ENTERTAINMENT:
                return new Ancillaries(previousAncillaries.getBaggage(),
                        previousAncillaries.getMeal(),
                        new Entertainment(quantity + previousAncillaries.getEntertainmentQuantity()));
        }

        return ancillaries;
    }

    protected boolean isSuitableToBeApplied(Recommendation reco, Route route) {
        return reco.getQuantity() > route.getAncillaryQuantity(reco.getType());
    }

}