package com.despegar.javAncillaries.services.ancillaries;

import com.despegar.javAncillaries.connectors.AncillariesConnector;
import com.despegar.javAncillaries.exceptions.AncillariesBookingException;
import com.despegar.javAncillaries.exceptions.HttpRequestException;
import com.despegar.javAncillaries.mappers.MapperToRequest;
import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.ancillaries.Ancillaries;
import com.despegar.javAncillaries.repositories.SoldAncillariesRepository;
import com.despegar.javAncillaries.services.bookings.BookingService;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AncillariesService {

    @Autowired
    private BookingService bookingService;
    @Autowired
    private AncillariesConnector ancillariesConnector;
    @Autowired
    private SoldAncillariesRepository repository;
    @Autowired
    private MapperToRequest mapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(AncillariesService.class);

    public List<String> getAncillariesIds(String from, String to, ItineraryResponse itinerary, String bookId) {
        List<String> ancillariesIds = new ArrayList<>();
        List<AncillariesRequest> requests;

        LOGGER.info("Mapping process of Ancillaries to Requests begun.");
        requests = mapper.transformToAncillariesRequest(from, to, itinerary.getToRoute(), bookId);
        if(itinerary.hasAReturnTravel())
            requests.addAll(mapper.transformToAncillariesRequest(to, from, itinerary.getBackRoute(), bookId));
        LOGGER.info("Booking ancillaries...");
        this.bookAncillaries(requests, ancillariesIds, bookId);
        LOGGER.info("Finish booking ancillaries.");
        LocalDate date = new LocalDate();
        requests.stream().forEach(req -> req.setDate(date));
        repository.addAncillaries(requests);
        LOGGER.info("Ancillaries data saved in repository for reports.");
        return ancillariesIds;
    }

    private void bookAncillaries(List<AncillariesRequest> requests, List<String> ids, String bookId){
        try {
            requests.stream().forEach(request ->
                    ids.add(ancillariesConnector.getResponse(request)));
        } catch (HttpRequestException hre){
            LOGGER.error("An ancillaries booking failed.", hre);
            bookingService.cancel(bookId);
            throw new AncillariesBookingException("There was a problem while booking an ancillary. The booking was canceled.", hre);
        } catch (AncillariesBookingException abe){
            LOGGER.error("An ancillaries booking failed.", abe);
            bookingService.cancel(bookId);
            throw new AncillariesBookingException("There was a problem while booking an ancillary (" + abe.getMessage()
                   + "). The booking was canceled.", abe);
        }
    }

    public void setAncillariesConnector(AncillariesConnector ancillariesConnector) {
        this.ancillariesConnector = ancillariesConnector;
    }

    public void setRepository(SoldAncillariesRepository repository) {
        this.repository = repository;
    }

    public void setMapper(MapperToRequest mapper) {
        this.mapper = mapper;
    }

    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }
}
