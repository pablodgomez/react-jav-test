package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AntaresAdult {

    @Expose
    private int quantity;

    public AntaresAdult(int quantity){
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }
}
