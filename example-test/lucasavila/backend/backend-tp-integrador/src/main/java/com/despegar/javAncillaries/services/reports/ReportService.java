package com.despegar.javAncillaries.services.reports;

import com.despegar.javAncillaries.model.ancillaries.SoldAncillaries;
import com.despegar.javAncillaries.repositories.SoldAncillariesRepository;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReportService {

    @Autowired
    private SoldAncillariesRepository repository;

    public List<SoldAncillaries> getSoldAncillariesIn(String month, String from, String to) {
        DateTimeFormatter inputFormat = DateTimeFormat.forPattern("yyyy-MM");
        LocalDate date = LocalDate.parse(month, inputFormat);

        return repository.getSoldAncillariesFrom(date, from, to);
    }

    public void setRepository(SoldAncillariesRepository repository) {
        this.repository = repository;
    }
}
