package com.despegar.javAncillaries.model.booking;

import java.util.List;

public class ItineraryBooking {

    private String itineraryId;
    private List<Passenger> passengers;


    public ItineraryBooking(String itineraryId, List<Passenger> passengers) {
        this.itineraryId = itineraryId;
        this.passengers = passengers;
    }

    public String getItineraryId() {
        return itineraryId;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }
}
