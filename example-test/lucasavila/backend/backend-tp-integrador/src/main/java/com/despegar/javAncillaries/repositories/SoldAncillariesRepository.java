package com.despegar.javAncillaries.repositories;

import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ancillaries.SoldAncillaries;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SoldAncillariesRepository {

    @Autowired
    private MapperToResponse mapper;
    private List<AncillariesRequest> ancillaries = new LinkedList<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(SoldAncillariesRepository.class);

    public void addAncillaries(List<AncillariesRequest> additional){
        ancillaries.addAll(additional);
    }

    public List<SoldAncillaries> getSoldAncillariesFrom(LocalDate date, String from, String to){
        List<AncillariesRequest> filteredAncillaries = ancillaries.stream()
                .filter(ancillary -> wasSoldIn(ancillary, date, from, to))
                .collect(Collectors.toList());
        LOGGER.info(String.format("Filtered all sold ancillaries from: %s, %s, %s", date.toString(), from, to));
        return mapper.mapToSoldAncillaries(from, to, filteredAncillaries);
    }

    private boolean wasSoldIn(AncillariesRequest ancillary, LocalDate date, String from, String to) {
        return sameMonthAndYear(ancillary.getDate(), date)
                && ancillary.getFrom().equals(from)
                && ancillary.getTo().equals(to);
    }

    private boolean sameMonthAndYear(LocalDate date, LocalDate anotherDate) {
        return Months.monthsBetween(date, anotherDate).getMonths() == 0
                && Years.yearsBetween(date, anotherDate).getYears() == 0;
    }

    public void setAncillaries(List<AncillariesRequest> ancillaries) {
        this.ancillaries = ancillaries;
    }

    public void setMapper(MapperToResponse mapper) {
        this.mapper = mapper;
    }
}
