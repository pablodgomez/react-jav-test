package com.despegar.javAncillaries.controllers;

import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ancillaries.SoldAncillaries;
import com.despegar.javAncillaries.model.booking.BookingResponse;
import com.despegar.javAncillaries.services.reports.ReportService;
import com.despegar.javAncillaries.utils.ParamsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ReportController {

    @Autowired
    private ParamsValidator validator;
    @Autowired
    private ReportService reportService;
    @Autowired
    private MapperToResponse mapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

    @RequestMapping(value = "/despegar-it-jav/ancillaries/report", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getBookingResponse(@RequestParam("from") String from,
                                     @RequestParam("to") String to,
                                     @RequestParam("month") String month) {
        LOGGER.info(String.format("Checking input parameters: %s, %s, %s", from, to, month));
        validator.checkPlace(from);
        validator.checkPlace(to);
        validator.checkMonth(month);
        LOGGER.info("Input parameters checked.");

        List<SoldAncillaries> soldAncillaries = reportService.getSoldAncillariesIn(month, from, to);
        return mapper.mapToJson(soldAncillaries);
    }
}