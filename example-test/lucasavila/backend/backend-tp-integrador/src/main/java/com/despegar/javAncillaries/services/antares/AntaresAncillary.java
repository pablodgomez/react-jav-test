package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;

public class AntaresAncillary {

    @Expose
    private AntaresAdult adult = new AntaresAdult(0);

    public void setAdult(AntaresAdult adult) {
        this.adult = adult;
    }

    public int getQuantity(){
        return this.adult.getQuantity();
    }
}
