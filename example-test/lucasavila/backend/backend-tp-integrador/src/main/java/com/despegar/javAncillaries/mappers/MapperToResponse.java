package com.despegar.javAncillaries.mappers;

import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.Price;
import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.AncillariesPack;
import com.despegar.javAncillaries.model.ancillaries.AncillaryType;
import com.despegar.javAncillaries.model.ancillaries.SoldAncillaries;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class MapperToResponse {

    private Gson gson;
    private static final Logger LOGGER = LoggerFactory.getLogger(MapperToResponse.class);

    public MapperToResponse(){
        GsonBuilder gsonBuilder = new GsonBuilder()
                .registerTypeAdapter(LocalDate.class, new DateSerializer());
        this.gson = gsonBuilder.create();
    }

    public String mapToJson(Object object){
        LOGGER.info("Mapping object to json.");
        return this.gson.toJson(object);
    }

    public ItineraryResponse mapToFinalItineraryResponse(ItineraryResponse itinerary, AncillariesPack ancillariesPack){
        //Obtaining a new route with the ancillaries applied.
        Route routeTo = itinerary.getToRoute();
        Route routeToFinal = new Route(routeTo.getFrom(), routeTo.getTo(),
                routeTo.getDuration(),
                routeTo.getDepartureDate(), routeTo.getArrivalDate(),
                routeTo.getAncillaries(), ancillariesPack.getAncillariesAndPriceTo().getAncillaries());
        LOGGER.info("Final TO route generated for the itinerary: " + itinerary.getItineraryId());
        List<Route> routes = new ArrayList<>();
        routes.add(routeToFinal);
        //If it has another route (return route).
        if (itinerary.hasAReturnTravel()) {
            Route routeBack = itinerary.getBackRoute();
            Route routeBackFinal = new Route(routeBack.getFrom(), routeBack.getTo(),
                    routeBack.getDuration(),
                    routeBack.getDepartureDate(), routeBack.getArrivalDate(),
                    routeBack.getAncillaries(), ancillariesPack.getAncillariesAndPriceBack().getAncillaries());
            routes.add(routeBackFinal);
            LOGGER.info("Final BACK route generated for the itinerary: " + itinerary.getItineraryId());
        }
        Price finalPrice = new Price(itinerary.getPrice().getItinerary(), ancillariesPack.getTotalPrice());
        LOGGER.info("Final price generated for the itinerary: " + itinerary.getItineraryId());
        //Finally we create the itinerary response.
        return new ItineraryResponse(itinerary.getItineraryId(), itinerary.getValidatingCarrier(), routes, finalPrice);
    }

    public List<SoldAncillaries> mapToSoldAncillaries(String from, String to, List<AncillariesRequest> sold){
        //First we get all the possible dates (different days).
        List<SoldAncillaries> soldAncillaries = new ArrayList<>();
        LOGGER.info("Getting all dates from sold ancillaries");
        Set<LocalDate> dates = sold.stream().map(ancillary -> ancillary.getDate()).collect(Collectors.toSet());
        //Then we create what would be our responses for every single date.
        LOGGER.info("Generating a sold ancillary response for each date.");
        dates.stream().forEach(date -> soldAncillaries.add(this.createSoldAncillaries(from, to, date)));
        // Finally for each response, we increase its quantity depending
        // on the ancillary information received.
        soldAncillaries.stream().forEach(soldAnc -> this.addQuantity(soldAnc, sold));
        LOGGER.info("Finished all the SoldAncillaries responses.");
        return soldAncillaries;
    }

    private void addQuantity(SoldAncillaries sold, List<AncillariesRequest> ancillaries) {
        ancillaries.stream()
                .filter(ancillary -> ancillary.getDate().compareTo(sold.getDate()) == 0)
                .forEach(ancillary -> this.increase(sold, ancillary.getType(), ancillary.getQuantity()));
    }

    private void increase(SoldAncillaries sold, AncillaryType type, int quantity) {
        switch (type){
            case BAGGAGE:
                sold.addBaggages(quantity);
                break;
            case MEAL:
                sold.addMeals(quantity);
                break;
            case ENTERTAINMENT:
                sold.addEntertainments(quantity);
                break;
        }
    }

    private SoldAncillaries createSoldAncillaries(String from, String to, LocalDate date) {
        return new SoldAncillaries(from, to, date);
    }
}
