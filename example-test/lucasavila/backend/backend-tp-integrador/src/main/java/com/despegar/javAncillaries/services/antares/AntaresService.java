package com.despegar.javAncillaries.services.antares;

import com.despegar.javAncillaries.connectors.AntaresConnector;
import com.despegar.javAncillaries.mappers.MapperToModel;
import com.despegar.javAncillaries.model.ItineraryResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AntaresService {

    @Autowired
    private AntaresConnector antaresConnector;
    @Autowired
    private MapperToModel mapper;

    public List<ItineraryResponse> getItineraries(String from, String to, String fromDate, String toDate){
        AntaresResponse antaresResponse =
                antaresConnector.getResponse(from, to, fromDate, toDate);
        return mapper.transformAntaresToModel(antaresResponse.getItineraries());
    }

    public void setMapper(MapperToModel mapper) {
        this.mapper = mapper;
    }

    public void setAntaresConnector(AntaresConnector antaresConnector) {
        this.antaresConnector = antaresConnector;
    }
}
