package com.despegar.javAncillaries.model.ancillaries;

public class AncillariesPack {

    private AncillariesAndPrice ancillariesAndPriceTo;
    private AncillariesAndPrice ancillariesAndPriceBack;

    public AncillariesPack(){}

    public AncillariesPack(AncillariesAndPrice ancillariesAndPriceTo, AncillariesAndPrice ancillariesAndPriceBack) {
        this.ancillariesAndPriceTo = ancillariesAndPriceTo;
        this.ancillariesAndPriceBack = ancillariesAndPriceBack;
    }

    public void setAncillariesAndPriceTo(AncillariesAndPrice ancillariesAndPriceTo) {
        this.ancillariesAndPriceTo = ancillariesAndPriceTo;
    }

    public void setAncillariesAndPriceBack(AncillariesAndPrice ancillariesAndPriceBack) {
        this.ancillariesAndPriceBack = ancillariesAndPriceBack;
    }

    public AncillariesAndPrice getAncillariesAndPriceTo() {
        return ancillariesAndPriceTo;
    }

    public AncillariesAndPrice getAncillariesAndPriceBack() {
        return ancillariesAndPriceBack;
    }

    public double getTotalPrice() {
        if(ancillariesAndPriceBack != null)
            return ancillariesAndPriceTo.getTotalPrice() + ancillariesAndPriceBack.getTotalPrice();
        return ancillariesAndPriceTo.getTotalPrice();
    }
}
