package com.despegar.javAncillaries.model.ancillaries;

import com.google.gson.annotations.SerializedName;

public enum AncillaryType {
    @SerializedName("baggage")
    BAGGAGE,
    @SerializedName("meal")
    MEAL,
    @SerializedName("entertainment")
    ENTERTAINMENT;
}
