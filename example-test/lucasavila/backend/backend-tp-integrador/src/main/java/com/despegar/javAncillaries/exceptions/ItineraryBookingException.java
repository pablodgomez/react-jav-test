package com.despegar.javAncillaries.exceptions;

public class ItineraryBookingException extends RuntimeException {

    public ItineraryBookingException(String s, HttpRequestException hte) {
        super(s, hte);
    }
}
