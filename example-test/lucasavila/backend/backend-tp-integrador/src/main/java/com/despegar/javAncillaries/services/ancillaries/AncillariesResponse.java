package com.despegar.javAncillaries.services.ancillaries;

import com.google.gson.annotations.Expose;

public class AncillariesResponse {

    @Expose
    private String id;

    public String getId() {
        return id;
    }
}
