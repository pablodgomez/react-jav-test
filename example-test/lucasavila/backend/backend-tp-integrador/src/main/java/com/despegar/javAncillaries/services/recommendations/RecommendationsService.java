package com.despegar.javAncillaries.services.recommendations;

import com.despegar.javAncillaries.connectors.RecommendationsConnector;
import com.despegar.javAncillaries.mappers.MapperToModel;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.LocationPack;
import com.despegar.javAncillaries.model.Recommendation;
import com.despegar.javAncillaries.model.Route;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class RecommendationsService {

    @Autowired
    private RecommendationsConnector recommendationConnector;
    @Autowired
    private MapperToModel mapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(RecommendationsService.class);

    public Map<LocationPack, List<Recommendation>> getRecommendations(Set<LocationPack> locations){

        Map<LocationPack, List<Recommendation>> recommendationsByLocationPack =
                locations.stream().collect(Collectors.toMap(location -> location, location -> this.addRecommendation(location)));
        LOGGER.info("All recommendations received for every single route.");
        return recommendationsByLocationPack;
    }

    private List<Recommendation> addRecommendation(LocationPack location) {
        LOGGER.info("Requesting recommendation for: " + location.getFrom() + ", " + location.getTo());
        return mapper.transformToRecommendation(recommendationConnector.getResponse(location.getFrom(), location.getTo()));
    }

}
