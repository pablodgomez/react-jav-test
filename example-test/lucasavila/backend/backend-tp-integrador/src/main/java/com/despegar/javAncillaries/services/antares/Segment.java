package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Segment {

    @Expose
    private String from;
    @Expose
    private String to;
    @Expose
    private String duration;
    @SerializedName("departure_datetime")
    @Expose
    private String departureDatetime;
    @SerializedName("arrival_datetime")
    @Expose
    private String arrivalDatetime;

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getDepartureDatetime() {
        return departureDatetime;
    }

    public String getArrivalDatetime() {
        return arrivalDatetime;
    }

}