package com.despegar.javAncillaries.services.bookings;

import com.despegar.javAncillaries.connectors.BookingConnector;
import com.despegar.javAncillaries.mappers.MapperToRequest;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.booking.BookingResponse;
import com.despegar.javAncillaries.services.ancillaries.AncillariesService;
import com.despegar.javAncillaries.services.ancillaries.ItineraryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookingService {

    @Autowired
    private BookingConnector bookingConnector;
    @Autowired
    private ItineraryService itineraryService;
    @Autowired
    private AncillariesService ancillariesService;
    @Autowired
    private MapperToRequest mapperToRequest;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingService.class);

    public BookingResponse getResponse(String from, String to, String fromDate, String toDate, String clientType, String passenger) {
        ItineraryResponse itinerary = itineraryService.getResponse(from, to, fromDate, toDate, clientType);
        LOGGER.info("Got the best itinerary. Itinerary ID: " + itinerary.getItineraryId());
        String bookId = this.book(itinerary.getItineraryId(), passenger).getBookId();
        LOGGER.info("The itinerary has been booked. Booking ID: " + bookId);
        List<String> ancillaries = ancillariesService.getAncillariesIds(from, to, itinerary, bookId);
        LOGGER.info("All ancillaries have been booked. Booking ID: " + bookId);
        return new BookingResponse(bookId, ancillaries, itinerary);
    }

    public ItineraryBookingResponse book(String itinerary_id, String passenger) {
        return bookingConnector.getResponse(mapperToRequest.transformToItineraryBookingRequest(itinerary_id, passenger));
    }

    public void cancel(String bookId) {
        bookingConnector.cancelItinerary(bookId);
    }

    public void setItineraryService(ItineraryService itineraryService) {
        this.itineraryService = itineraryService;
    }

    public void setAncillariesService(AncillariesService ancillariesService) {
        this.ancillariesService = ancillariesService;
    }

    public void setBookingConnector(BookingConnector bookingConnector) {
        this.bookingConnector = bookingConnector;
    }

    public void setMapperToRequest(MapperToRequest mapperToRequest) {
        this.mapperToRequest = mapperToRequest;
    }
}
