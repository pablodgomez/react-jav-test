package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;

import java.util.List;

public class Choice {

    @Expose
    private String duration;
    @Expose
    private List<Segment> segments;
    @Expose
    private AntaresAncillary baggage = new AntaresAncillary();
    @Expose
    private AntaresAncillary meal = new AntaresAncillary();
    @Expose
    private AntaresAncillary entertainment = new AntaresAncillary();

    public void setBaggage(AntaresAncillary baggage) {
        this.baggage = baggage;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public AntaresAncillary getBaggage() {
        return baggage;
    }

    public AntaresAncillary getMeal() {
        return meal;
    }

    public AntaresAncillary getEntertainment() {
        return entertainment;
    }

    public String getDuration() {
        return duration;
    }
}