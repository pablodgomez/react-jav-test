package com.despegar.javAncillaries.services.ancillaries;

import com.despegar.javAncillaries.exceptions.RecommendationException;
import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.LocationPack;
import com.despegar.javAncillaries.model.Recommendation;
import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.AncillariesAndPrice;
import com.despegar.javAncillaries.model.ancillaries.AncillariesPack;
import com.despegar.javAncillaries.model.clients.ClientType;
import com.despegar.javAncillaries.services.antares.AntaresService;
import com.despegar.javAncillaries.services.recommendations.RecommendationsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ItineraryService {

    @Autowired
    private AntaresService antaresService;
    @Autowired
    private MapperToResponse mapperToResponse;
    @Autowired
    protected RecommendationsService recommendationsService;
    private static final Logger LOGGER = LoggerFactory.getLogger(ItineraryService.class);

    public ItineraryResponse getResponse(String from, String to, String fromDate, String toDate, String clientType) {
        LOGGER.info("Getting possibles itineraries.");
        List<ItineraryResponse> possibleAnswers = antaresService.getItineraries(from, to, fromDate, toDate);
        LOGGER.info("Getting best itinerary for the flight according to type " + clientType);
        LOGGER.info("Generating location packs (from, to) for every single route.");

        Set<LocationPack> locations = possibleAnswers.stream()
                .map(itinerary -> generateLocationPack(itinerary.getToRoute())).collect(Collectors.toSet());
        if(!StringUtils.isEmpty(toDate))
            locations.addAll(possibleAnswers.stream()
                    .map(itinerary -> generateLocationPack(itinerary.getBackRoute())).collect(Collectors.toSet()));

        LOGGER.info("Location Packs generated without repeated elements.");
        Map<LocationPack, List<Recommendation>> recommendations = recommendationsService.getRecommendations(locations);
        ClientType client = ClientType.valueOf(clientType.toUpperCase());
        List<ItineraryResponse> finalItineraries;

        finalItineraries = applyRecommendations(possibleAnswers, client, recommendations);

        LOGGER.info("All ancillaries applied to the ItineraryResponses");
        finalItineraries.sort(Comparator.comparing(ItineraryResponse::getTotalPrice));
        LOGGER.info("ItineraryResponses sorted by Total Price.");
        return finalItineraries.get(0);
    }

    private List<ItineraryResponse> applyRecommendations(List<ItineraryResponse> possibleAnswers, ClientType client, Map<LocationPack, List<Recommendation>> recommendations){
        List<ItineraryResponse> finalItineraries = new ArrayList<>();
        try {
            for(ItineraryResponse itinerary : possibleAnswers){
                AncillariesPack ancillariesPack = new AncillariesPack();
                LOGGER.info("Applying best ancillaries for the itinerary ID (TO route): " + itinerary.getItineraryId());
                AncillariesAndPrice ancillaries = client.setBestAncillariesMatch(itinerary.getToRoute(), recommendations.get(new LocationPack(itinerary.getToRoute().getFrom(), itinerary.getToRoute().getTo())));
                ancillariesPack.setAncillariesAndPriceTo(ancillaries);

                if(itinerary.hasAReturnTravel()) {
                    LOGGER.info("Applying best ancillaries for the itinerary ID (BACK route): " + itinerary.getItineraryId());
                    ancillaries = client.setBestAncillariesMatch(itinerary.getBackRoute(), recommendations.get(new LocationPack(itinerary.getToRoute().getFrom(), itinerary.getToRoute().getTo())));
                    ancillariesPack.setAncillariesAndPriceBack(ancillaries);
                }

                LOGGER.info("Ancillaries applied for the itinerary ID: " + itinerary.getItineraryId());
                LOGGER.info("Generating final itinerary response for the itinerary ID: " + itinerary.getItineraryId());
                finalItineraries.add(mapperToResponse.mapToFinalItineraryResponse(itinerary, ancillariesPack));
            }
        } catch (RecommendationException re) {
            LOGGER.error("There was a problem while adding an extra suitcase.", re);
            throw new RecommendationException("Baggage is empty, cannot infer the price.", re);
        }

        return finalItineraries;
    }

    private LocationPack generateLocationPack(Route route) {
        return new LocationPack(route.getFrom(), route.getTo());
    }

    public void setAntaresService(AntaresService antaresService) {
        this.antaresService = antaresService;
    }

    public void setRecommendationsService(RecommendationsService recommendationsService) {
        this.recommendationsService = recommendationsService;
    }

    public void setMapperToResponse(MapperToResponse mapperToResponse) {
        this.mapperToResponse = mapperToResponse;
    }
}
