package com.despegar.javAncillaries.services.recommendations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceRecommendation {
    
    @Expose
    private String currency;
    @Expose @SerializedName("amount")
    private double unitCharge;

    public double getUnitCharge() {
        return unitCharge;
    }
}
