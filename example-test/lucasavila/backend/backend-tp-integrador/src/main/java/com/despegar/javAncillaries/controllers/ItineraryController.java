package com.despegar.javAncillaries.controllers;

import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.utils.ParamsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.despegar.javAncillaries.services.ancillaries.ItineraryService;

@Controller
public class ItineraryController {

    @Autowired
    private ItineraryService itineraryService;
    @Autowired
    private ParamsValidator validator;
    @Autowired
    private MapperToResponse mapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(ItineraryController.class);

    @RequestMapping(value = "/despegar-it-jav/itinerary", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getItinerariesResponse(@RequestParam("from") String from,
                                         @RequestParam("to") String to,
                                         @RequestParam("from_date") String fromDate,
                                         @RequestParam(required = false, name = "to_date") String toDate,
                                         @RequestParam("client_type") String clientType) {
        LOGGER.info(String.format("Checking input parameters: %s, %s, %s, %s, %s", from, to, fromDate, toDate, clientType));
        validator.checkInputParams(from, to, fromDate, toDate);
        validator.checkClientType(clientType);
        LOGGER.info("Input parameters checked.");

        ItineraryResponse itineraryResponse = itineraryService.getResponse(from, to, fromDate, toDate, clientType);
        return mapper.mapToJson(itineraryResponse);
    }

    public void setItineraryService(ItineraryService itineraryService) {
        this.itineraryService = itineraryService;
    }

    public void setValidator(ParamsValidator validator) {
        this.validator = validator;
    }
}
