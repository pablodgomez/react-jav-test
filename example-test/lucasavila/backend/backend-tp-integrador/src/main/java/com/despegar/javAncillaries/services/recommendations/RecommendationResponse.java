package com.despegar.javAncillaries.services.recommendations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecommendationResponse {

    @Expose
    private String type;
    @Expose
    private int quantity;
    @SerializedName("unit_charge")
    @Expose
    private double unitCharge;
    @Expose
    private PriceRecommendation price;

    public void setType(String type) {
        this.type = type;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setUnitCharge(double unitCharge) {
        this.unitCharge = unitCharge;
    }

    public String getType() { return type; }

    public int getQuantity() { return quantity; }

    public double getUnitCharge() { return price.getUnitCharge(); }
}
