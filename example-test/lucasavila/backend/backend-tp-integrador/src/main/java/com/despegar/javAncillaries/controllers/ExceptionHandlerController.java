package com.despegar.javAncillaries.controllers;

import com.despegar.javAncillaries.exceptions.*;
import com.google.gson.Gson;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @Override
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex,
            HttpHeaders headers,
            HttpStatus status, WebRequest request) {

        String error = "'" + ex.getParameterName() + "' parameter is missing.";
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, error);

        return new ResponseEntity<Object>(errorMessage, errorMessage.getStatus());
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex) {

        String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, error);

        return new ResponseEntity<Object>(errorMessage, errorMessage.getStatus());
    }

    @ExceptionHandler({ HttpRequestException.class })
    public ResponseEntity<Object> handleHttpRequestFail(HttpRequestException ex) {

        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.SERVICE_UNAVAILABLE, ex.getMessage());

        return new ResponseEntity<Object>(errorMessage, errorMessage.getStatus());
    }

    @ExceptionHandler({ ParameterException.class })
    public ResponseEntity<Object> handleParametersException(ParameterException ex) {

        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.BAD_REQUEST, ex.getMessage());

        return new ResponseEntity<Object>(errorMessage, errorMessage.getStatus());
    }

    @ExceptionHandler({ AncillariesBookingException.class })
    public ResponseEntity<Object> handleAncillariesBookingException(AncillariesBookingException ex) {

        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());

        return new ResponseEntity<Object>(errorMessage, errorMessage.getStatus());
    }

    @ExceptionHandler({ RecommendationException.class })
    public ResponseEntity<Object> handleRecommendationException(RecommendationException ex) {

        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());

        return new ResponseEntity<Object>(errorMessage, errorMessage.getStatus());
    }

    @ExceptionHandler({ ItineraryBookingException.class })
    public ResponseEntity<Object> handleItineraryBookingException(ItineraryBookingException ex) {

        ErrorMessage errorMessage = new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());

        return new ResponseEntity<Object>(errorMessage, errorMessage.getStatus());
    }

}