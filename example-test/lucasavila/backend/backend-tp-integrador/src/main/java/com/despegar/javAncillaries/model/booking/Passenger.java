package com.despegar.javAncillaries.model.booking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Passenger {

    @Expose
    private String name;
    @SerializedName("last_name")
    @Expose
    private String lastName;

    public Passenger(String completeName) {
        String[] components = completeName.split(" ");
        this.name = components[0];
        this.lastName = components[1];
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }
}
