package com.despegar.javAncillaries.model.ancillaries;

public class AncillariesAndPrice {

    private Ancillaries ancillaries;
    private double totalPrice = 0;

    public AncillariesAndPrice(Ancillaries ancillaries, double totalPrice) {
        this.ancillaries = ancillaries;
        this.totalPrice = totalPrice;
    }

    public Ancillaries getAncillaries() {
        return ancillaries;
    }

    public double getTotalPrice() {
        return totalPrice;
    }
}
