package com.despegar.javAncillaries.configurations;


import com.google.gson.Gson;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MainConfiguration {

    private Gson gson = new Gson();

    @Bean
    public Gson gsonInstance() {
        return this.gson;
    }
}
