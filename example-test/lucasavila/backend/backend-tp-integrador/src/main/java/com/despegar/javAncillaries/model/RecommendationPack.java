package com.despegar.javAncillaries.model;

import java.util.List;

public class RecommendationPack {

    private List<Recommendation> recommendationsOut;
    private List<Recommendation> recommendationsIn;

    public RecommendationPack(List<Recommendation> recommendationsOut, List<Recommendation> recommendationsIn) {
        this.recommendationsOut = recommendationsOut;
        this.recommendationsIn = recommendationsIn;
    }

    public List<Recommendation> getRecommendationsOut() {
        return recommendationsOut;
    }

    public List<Recommendation> getRecommendationsIn() {
        return recommendationsIn;
    }
}
