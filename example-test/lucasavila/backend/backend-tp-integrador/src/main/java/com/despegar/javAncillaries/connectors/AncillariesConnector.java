package com.despegar.javAncillaries.connectors;

import com.despegar.javAncillaries.configurations.AppProperties;
import com.despegar.javAncillaries.exceptions.HttpRequestException;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import com.despegar.javAncillaries.services.ancillaries.AncillariesResponse;
import com.google.gson.Gson;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class AncillariesConnector extends Connector{

    @Value("${app.ancillariesBookingUrl}")
    private String ancillariesBookingUrl;
    @Autowired
    private Gson gson;
    private static final Logger LOGGER = LoggerFactory.getLogger(AncillariesConnector.class);

    public String getResponse(AncillariesRequest ancillariesRequest){

        LOGGER.info("Sending request to Ancillaries.");
        String requestJson = gson.toJson(ancillariesRequest);

        try {
            String responseJson = sendRequest(Request.Post(this.ancillariesBookingUrl)
                    .bodyString(requestJson, ContentType.APPLICATION_JSON));
            LOGGER.info("Got ancillaries (booking) response. JSON: " + responseJson);
            return gson.fromJson(responseJson, AncillariesResponse.class).getId();
        }
        catch (HttpRequestException hre){
            LOGGER.error("Failed to book the ancillaries. Book id: " + ancillariesRequest.getBookingId(), hre);
            throw new HttpRequestException
                    ("The ancillaries booking failed due to an ancillary booking (" + ancillariesRequest.getType() + ").", hre);
        }
    }

}
