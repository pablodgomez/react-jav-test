package com.despegar.javAncillaries.model;

import com.despegar.javAncillaries.model.ancillaries.Ancillaries;
import com.despegar.javAncillaries.model.ancillaries.AncillaryType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Route {

    @Expose
    private String from;
    @Expose
    private String to;
    @Expose
    private String duration;
    @SerializedName("departure_date")
    @Expose
    private String departureDate;
    @SerializedName("arrival_date")
    @Expose
    private String arrivalDate;
    @Expose
    private Ancillaries ancillaries;
    @SerializedName("additional_ancillaries")
    @Expose
    private Ancillaries additionalAncillaries;


    public Route(String from, String to, String duration, String departureDate, String arrivalDate, Ancillaries ancillaries, Ancillaries additionalAncillaries) {
        this.from = from;
        this.to = to;
        this.duration = duration;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.ancillaries = ancillaries;
        this.additionalAncillaries = additionalAncillaries;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getDuration() {
        return duration;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public Ancillaries getAncillaries() {
        return ancillaries;
    }

    public Ancillaries getAdditionalAncillaries() { return additionalAncillaries; }

    public int getSuitcasesQuantity(){
        return this.ancillaries.getSuitcasesQuantity();
    }

    public int getAncillaryQuantity(AncillaryType type){
        switch (type){
            case BAGGAGE:
                return ancillaries.getSuitcasesQuantity();
            case MEAL:
                return ancillaries.getMealsQuantity();
            case ENTERTAINMENT:
                return ancillaries.getEntertainmentQuantity();
        }
        return 0;
    }
}