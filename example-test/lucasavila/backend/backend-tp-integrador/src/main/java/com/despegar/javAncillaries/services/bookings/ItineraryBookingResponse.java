package com.despegar.javAncillaries.services.bookings;

import com.despegar.javAncillaries.model.booking.ItineraryBooking;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItineraryBookingResponse {

    @SerializedName("booking_id")
    @Expose
    private String bookingId;

    public ItineraryBookingResponse(String bookingId){
        this.bookingId = bookingId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public String getBookId() {
        return this.bookingId;
    }
}
