package com.despegar.javAncillaries.model.ancillaries;

import com.google.gson.annotations.Expose;
import org.apache.http.util.EntityUtils;

public class Ancillaries {

    @Expose
    private Baggage baggage;
    @Expose
    private Meal meal;
    @Expose
    private Entertainment entertainment;

    public Ancillaries(){
        this.baggage = new Baggage(0);
        this.meal = new Meal(0);
        this.entertainment= new Entertainment(0);
    }

    public Ancillaries(Baggage baggage, Meal meal, Entertainment entertainment) {
        this.baggage = baggage;
        this.meal = meal;
        this.entertainment = entertainment;
    }

    public Baggage getBaggage() {
        return baggage;
    }

    public Meal getMeal() {
        return meal;
    }

    public Entertainment getEntertainment() {
        return entertainment;
    }

    public int getSuitcasesQuantity() {
        return this.baggage.getQuantity();
    }

    public int getMealsQuantity() {
        return this.meal.getQuantity();
    }

    public int getEntertainmentQuantity() {
        return this.entertainment.getQuantity();
    }

    public void addQuantity(AncillaryType type, int quantity) {
        switch (type){
            case BAGGAGE:
                this.baggage.add(quantity);
                break;
            case MEAL:
                this.meal.add(quantity);
                break;
            case ENTERTAINMENT:
                this.entertainment.add(quantity);
                break;
        }
    }
}