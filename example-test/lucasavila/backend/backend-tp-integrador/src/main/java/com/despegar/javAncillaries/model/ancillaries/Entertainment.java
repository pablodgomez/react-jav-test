package com.despegar.javAncillaries.model.ancillaries;

import com.google.gson.annotations.Expose;

public class Entertainment implements Ancillary {

    @Expose
    private int quantity;

    public Entertainment(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void add(int quantity) {
        this.quantity += quantity;
    }
}