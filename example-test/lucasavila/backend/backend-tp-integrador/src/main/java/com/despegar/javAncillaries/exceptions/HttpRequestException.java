package com.despegar.javAncillaries.exceptions;

import java.io.IOException;

public class HttpRequestException extends RuntimeException {
    public HttpRequestException(String s, Exception e) {
        super(s, e);
    }

    public HttpRequestException(String s) {
        super(s);
    }
}
