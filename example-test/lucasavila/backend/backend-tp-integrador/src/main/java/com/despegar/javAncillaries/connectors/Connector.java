package com.despegar.javAncillaries.connectors;

import com.despegar.javAncillaries.exceptions.HttpRequestException;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;

import java.io.IOException;

public abstract class Connector {

    protected Logger LOGGER;

    public String sendRequest(Request request) {
        try {
            HttpResponse response = request.execute().returnResponse();
            this.checkStatusCode(response);
            return EntityUtils.toString(response.getEntity());
        }catch (IOException ioe) {
            LOGGER.error("There was an error with the request.", ioe);
            throw new HttpRequestException("GET to API failed (IOE).", ioe);
        }
    }

    private void checkStatusCode(HttpResponse response){
        int statusCode = response.getStatusLine().getStatusCode();
        if(statusCode < 200 || statusCode > 299)
            throw new HttpRequestException("Everything is not OK...Error Status Code:" + statusCode);
    }

}
