package com.despegar.javAncillaries.exceptions;

public class ClientException extends RuntimeException {
    public ClientException(String s) {
        super(s);
    }
}
