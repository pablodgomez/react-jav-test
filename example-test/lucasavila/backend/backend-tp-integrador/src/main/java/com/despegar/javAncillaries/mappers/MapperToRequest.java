package com.despegar.javAncillaries.mappers;

import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.Ancillaries;
import com.despegar.javAncillaries.model.ancillaries.AncillaryType;
import com.despegar.javAncillaries.model.booking.ItineraryBooking;
import com.despegar.javAncillaries.model.booking.Passenger;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import com.despegar.javAncillaries.utils.ParamsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MapperToRequest {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapperToRequest.class);

    public ItineraryBooking transformToItineraryBookingRequest(String itineraryId, String name){
        Passenger passenger = new Passenger(name);
        List<Passenger> passengers = new ArrayList<Passenger>();
        passengers.add(passenger);
        return new ItineraryBooking(itineraryId, passengers);
    }

    public List<AncillariesRequest> transformToAncillariesRequest(String from, String to, Route route, String bookingId) {
        List<AncillariesRequest> requests = new ArrayList<>();
        Ancillaries ancillaries = route.getAdditionalAncillaries();

        if(ancillaries.getSuitcasesQuantity() != 0)
            requests.add(new AncillariesRequest(bookingId, from, to, AncillaryType.BAGGAGE, ancillaries.getSuitcasesQuantity()));
        if(ancillaries.getMealsQuantity() != 0)
            requests.add(new AncillariesRequest(bookingId, from, to, AncillaryType.MEAL, ancillaries.getMealsQuantity()));
        if(ancillaries.getEntertainmentQuantity() != 0)
            requests.add(new AncillariesRequest(bookingId, from, to, AncillaryType.ENTERTAINMENT, ancillaries.getEntertainmentQuantity()));

        return requests;
    }
}
