package com.despegar.javAncillaries.exceptions;

public class RecommendationException extends RuntimeException {
    public RecommendationException(String s) {
        super(s);
    }

    public RecommendationException(String s, Exception e) {
        super(s, e);
    }
}
