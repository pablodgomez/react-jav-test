package com.despegar.javAncillaries.connectors;

import com.despegar.javAncillaries.configurations.AppProperties;
import com.despegar.javAncillaries.services.antares.AntaresResponse;
import com.google.gson.Gson;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class AntaresConnector extends Connector{

    @Autowired
    private Gson gson;
    @Value("${app.antaresUrl}")
    private String antaresUrl;
    private static final Logger LOGGER = LoggerFactory.getLogger(AntaresConnector.class);

    public AntaresResponse getResponse(String from, String to, String origin, String destiny){
        String finalUrl = this.appendInfo(from, to, origin, destiny);
        LOGGER.info(String.format("Sending request to Antares. Parameters: from %s, to %s, origin %s, destiny %s", from, to, origin, destiny));
        String json = sendRequest(Request.Get(finalUrl));
        LOGGER.info("Got Antares response. JSON: " + json);
        return gson.fromJson(json, AntaresResponse.class);
    }

    private String appendInfo(String from, String to, String from_date, String to_date) {
        String finalUrl = String.format(this.antaresUrl, from_date, "%s", from, to);

        if(!StringUtils.isEmpty(to_date)){
            finalUrl = String.format(finalUrl, "return_date="+to_date);
        }else{
            finalUrl = String.format(finalUrl, "");
        }

        LOGGER.info("Appended info to the URL: " + finalUrl);

        return finalUrl;
    }
}
