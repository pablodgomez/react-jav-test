package com.despegar.javAncillaries.model.booking;

import com.despegar.javAncillaries.model.ItineraryResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookingResponse {

    @SerializedName("book_id")
    @Expose
    private String bookId;
    @SerializedName("additional_ancillaries")
    @Expose
    private List<String> additionalAncillariesIds;
    @Expose
    private ItineraryResponse itinerary;


    public BookingResponse(String bookId, List<String> ancillaries, ItineraryResponse itinerary) {
        this.bookId = bookId;
        this.additionalAncillariesIds = ancillaries;
        this.itinerary = itinerary;
    }

    public String getBookId() {
        return bookId;
    }

    public List<String> getAdditionalAncillariesIds() {
        return additionalAncillariesIds;
    }

    public ItineraryResponse getItinerary() {
        return itinerary;
    }
}
