package com.despegar.javAncillaries.controllers;

import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.booking.BookingResponse;
import com.despegar.javAncillaries.services.bookings.BookingService;
import com.despegar.javAncillaries.utils.ParamsValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class BookingController {

    @Autowired
    private BookingService bookingService;
    @Autowired
    private ParamsValidator validator;
    @Autowired
    private MapperToResponse mapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingController.class);

    @RequestMapping(value = "/despegar-it-jav/bookings", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getBookingResponse(@RequestParam("from") String from,
                                         @RequestParam("to") String to,
                                         @RequestParam("from_date") String fromDate,
                                         @RequestParam(required = false, name = "to_date") String toDate,
                                         @RequestParam("client_type") String clientType,
                                         @RequestParam("passenger") String passenger) {
        LOGGER.info("Booking process begun.");
        LOGGER.info(String.format("Checking input parameters: %s, %s, %s, %s, %s, %s", from, to, fromDate, toDate, clientType, passenger));
        validator.checkInputParams(from, to, fromDate, toDate);
        validator.checkName(passenger);
        validator.checkClientType(clientType);
        LOGGER.info("Input parameters checked.");

        BookingResponse bookingResponse = bookingService.getResponse(from, to, fromDate, toDate, clientType, passenger);
        LOGGER.info("The booking has finished successfully for the passenger: " + passenger);
        return mapper.mapToJson(bookingResponse);
    }
}
