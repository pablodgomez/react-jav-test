package com.despegar.javAncillaries.model;

import java.util.List;

import com.despegar.javAncillaries.model.ancillaries.Ancillaries;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItineraryResponse {

    private String itineraryId;
    @SerializedName("validating_carrier")
    @Expose
    private String validatingCarrier;
    @Expose
    private List<Route> routes;
    @Expose
    private Price price;

    public ItineraryResponse(String itineraryId, String validatingCarrier, List<Route> routes, Price price){
        this.itineraryId = itineraryId;
        this.validatingCarrier = validatingCarrier;
        this.routes = routes;
        this.price = price;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public Price getPrice() {
        return price;
    }

    public boolean hasAReturnTravel(){
        return routes.size() == 2;
    }

    public Route getToRoute(){
        return this.routes.get(0);
    }


    public Route getBackRoute(){
        return this.routes.get(1);
    }

    public double getTotalPrice(){
        return this.price.getTotal();
    }

    public String getValidatingCarrier() {
        return validatingCarrier;
    }

    public String getItineraryId() {
        return itineraryId;
    }

    public Ancillaries getOutAdditionalAncillaries() {
        return this.routes.get(0).getAdditionalAncillaries();
    }

    public Ancillaries getInAdditionalAncillaries() {
        return this.routes.get(1).getAdditionalAncillaries();
    }
}