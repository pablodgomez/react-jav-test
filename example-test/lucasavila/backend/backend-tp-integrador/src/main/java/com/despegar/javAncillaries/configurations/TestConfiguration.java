package com.despegar.javAncillaries.configurations;

import com.despegar.javAncillaries.connectors.AntaresConnector;
import com.despegar.javAncillaries.connectors.RecommendationsConnector;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import static org.mockito.Mockito.*;

@Profile("test")
@Configuration
public class TestConfiguration {

    @Bean
    @Primary
    public AntaresConnector antaresConnectorMock() {
        return mock(AntaresConnector.class);
    }

    @Bean
    @Primary
    public RecommendationsConnector recommendationsConnector() {
        return mock(RecommendationsConnector.class);
    }
}
