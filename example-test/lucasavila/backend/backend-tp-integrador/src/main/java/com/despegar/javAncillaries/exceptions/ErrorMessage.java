package com.despegar.javAncillaries.exceptions;

import com.google.gson.annotations.Expose;
import org.springframework.http.HttpStatus;

public class ErrorMessage {

    @Expose
    private HttpStatus status;
    @Expose
    private String message;

    public ErrorMessage(HttpStatus status, String message) {
        super();
        this.status = status;
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
