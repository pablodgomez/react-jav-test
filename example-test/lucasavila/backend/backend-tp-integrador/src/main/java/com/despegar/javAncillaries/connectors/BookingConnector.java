package com.despegar.javAncillaries.connectors;

import com.despegar.javAncillaries.configurations.AppProperties;
import com.despegar.javAncillaries.exceptions.HttpRequestException;
import com.despegar.javAncillaries.exceptions.ItineraryBookingException;
import com.despegar.javAncillaries.model.booking.ItineraryBooking;
import com.despegar.javAncillaries.services.bookings.ItineraryBookingResponse;
import com.google.gson.Gson;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class BookingConnector extends Connector {

    @Value("${app.itinerariesBookingUrl}")
    private String itinerariesBookingUrl;
    @Autowired
    private Gson gson;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookingConnector.class);

    public ItineraryBookingResponse getResponse(ItineraryBooking itineraryBooking){

        LOGGER.info("Sending request to Bookings | Itinerary ID: " + itineraryBooking.getItineraryId());
        String requestJson = gson.toJson(itineraryBooking);

        try {
            String responseJson = this.sendRequest(Request.Post(this.itinerariesBookingUrl)
                    .bodyString(requestJson, ContentType.APPLICATION_JSON));
            LOGGER.info("Got Booking response. JSON: " + responseJson);
            return gson.fromJson(responseJson, ItineraryBookingResponse.class);
        } catch (HttpRequestException hre){
            LOGGER.error("Failed to book the itinerary ID: " + itineraryBooking.getItineraryId(), hre);
            throw new HttpRequestException("The itinerary booking failed.", hre);
        }
    }

    public void cancelItinerary(String bookingId){

        String finalUrl = this.appendInfo(bookingId);
        LOGGER.info("Canceling Itinerary ID: " + bookingId);
        Request request = Request.Delete(finalUrl);

        try {
            sendRequest(request);
        }
        catch (HttpRequestException hte){
            LOGGER.error("Failed to cancel the itinerary booking " + bookingId, hte);
            throw new ItineraryBookingException("The itinerary booking cancellation failed.", hte);
        }
    }

    private String appendInfo(String info) {
        return this.itinerariesBookingUrl + "/" + info;
    }

}
