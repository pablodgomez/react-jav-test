package com.despegar.javAncillaries.services.antares;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AntaresItinerary {

    @SerializedName("outbound_choices")
    @Expose
    private List<Choice> outboundChoices;
    @SerializedName("inbound_choices")
    @Expose
    private List<Choice> inboundChoices;
    @SerializedName("price_detail")
    @Expose
    private PriceDetail priceDetail;
    @SerializedName("validating_carrier")
    @Expose
    private String validatingCarrier;
    @SerializedName("booking_info")
    @Expose
    private List<BookingInfo> bookingInfo;

    public List<Choice> getOutboundChoices() {
        return outboundChoices;
    }

    public List<Choice> getInboundChoices() {
        return inboundChoices;
    }

    public List<Segment> getOutboundSegments() {
        return outboundChoices.get(0).getSegments();
    }

    public List<Segment> getInboundSegments() {
        if(inboundChoices == null)
            return new ArrayList<Segment>();
        return getInboundChoices().get(0).getSegments();
    }

    public PriceDetail getPriceDetail() {
        return priceDetail;
    }

    public String getValidatingCarrier() {
        return validatingCarrier;
    }

    public String getItineraryId(){
        return this.bookingInfo.get(0).getItineraryId();
    }

    public String getId() {
        return bookingInfo.get(0).getItineraryId();
    }
}