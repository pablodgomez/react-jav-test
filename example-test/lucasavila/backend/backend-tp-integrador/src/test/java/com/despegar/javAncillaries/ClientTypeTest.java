package com.despegar.javAncillaries;

import com.despegar.javAncillaries.exceptions.RecommendationException;
import com.despegar.javAncillaries.mappers.MapperToModel;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.LocationPack;
import com.despegar.javAncillaries.model.Recommendation;
import com.despegar.javAncillaries.model.ancillaries.Ancillaries;
import com.despegar.javAncillaries.model.ancillaries.AncillariesAndPrice;
import com.despegar.javAncillaries.model.clients.*;
import com.despegar.javAncillaries.services.recommendations.RecommendationsService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class ClientTypeTest extends TestUtils{

    private Gson gson;
    private MapperToModel mapper;

    @Before
    public void setUp(){
        gson = new Gson();
        mapper = new MapperToModel();
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldDoNothingWithAustere(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("AUSTERE");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getToRecommendations());
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getBackRecommendations());

        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getMealsQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldNotAddBaggagesToAnxious(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("ANXIOUS");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getBaggageRecommendation(5));
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getBaggageRecommendation(5));

        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getMealsQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldNotAddEntertainmentsToAnxious(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("ANXIOUS");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getEntertainmentRecommendation(1));
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getEntertainmentRecommendation(1));

        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getMealsQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldAddMealsToAnxious(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("ANXIOUS");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getMealRecommendation(2));
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getMealRecommendation(2));

        assertEquals(1, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(1, ancillariesBack.getAncillaries().getMealsQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldNotAddMealsToAnxious(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("ANXIOUS");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getMealRecommendation(1));
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getMealRecommendation(1));

        //The itinerary has one meal already.
        assertEquals(0, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getMealsQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldOnlyApllyMealsToAnxious(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("ANXIOUS");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), new ArrayList<Recommendation>());
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getBackRecommendations());

        //The recommendation has baggage and meals.
        assertEquals(0, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(1, ancillariesBack.getAncillaries().getMealsQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldAddBaggageToStandard(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("STANDARD");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getBaggageRecommendation(5));
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getBaggageRecommendation(5));


        assertEquals(4, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(4, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getMealsQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldAddMealToStandard(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("STANDARD");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getMealRecommendation(2));
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getMealRecommendation(2));

        assertEquals(1, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(1, ancillariesBack.getAncillaries().getMealsQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getEntertainmentQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldAddEntertainmentToStandard(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("STANDARD");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(),this.getEntertainmentRecommendation(3));
        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getEntertainmentRecommendation(3));


        assertEquals(2, ancillariesTo.getAncillaries().getEntertainmentQuantity());
        assertEquals(2, ancillariesBack.getAncillaries().getEntertainmentQuantity());
        // Additional checkings...
        assertEquals(0, ancillariesBack.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(0, ancillariesTo.getAncillaries().getMealsQuantity());
        assertEquals(0, ancillariesBack.getAncillaries().getMealsQuantity());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldSetBestMatchWithStandardClient(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("STANDARD");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getToRecommendations());

        assertEquals(2, ancillariesTo.getAncillaries().getSuitcasesQuantity());
        assertEquals(113.74, ancillariesTo.getTotalPrice(), 2);
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldSetBestMatchWithCautiousClient_AddingOneExtraSuitcase(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("CAUTIOUS");

        AncillariesAndPrice ancillariesTo = client.setBestAncillariesMatch(response.getToRoute(), this.getToRecommendations());

        assertEquals(3, ancillariesTo.getAncillaries().getSuitcasesQuantity());
    }

    @Test(expected = RecommendationException.class)
    public void setBestAncillariesMatch_routePriceReco_shouldThrowExceptionWhile_AddingOneExtraSuitcase(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("CAUTIOUS");

        client.setBestAncillariesMatch(response.getToRoute(), new ArrayList<Recommendation>());
    }

    @Test
    public void setBestAncillariesMatch_routePriceReco_shouldSetBestMatchWithAnxiousClient_TakingAllMeals(){
        ItineraryResponse response = this.getItinerariesResponseSample();
        ClientType client = ClientType.valueOf("CAUTIOUS");

        AncillariesAndPrice ancillariesBack = client.setBestAncillariesMatch(response.getBackRoute(), this.getBackRecommendations());

        assertEquals(1, ancillariesBack.getAncillaries().getMealsQuantity());
    }

}
