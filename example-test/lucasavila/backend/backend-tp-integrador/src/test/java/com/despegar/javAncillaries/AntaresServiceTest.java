package com.despegar.javAncillaries;

import com.despegar.javAncillaries.connectors.AntaresConnector;
import com.despegar.javAncillaries.mappers.MapperToModel;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.services.antares.AntaresItinerary;
import com.despegar.javAncillaries.services.antares.AntaresResponse;
import com.despegar.javAncillaries.services.antares.AntaresService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class AntaresServiceTest {

    private AntaresService service = new AntaresService();
    private AntaresConnector antaresConnectorMock;
    private MapperToModel mapperMock;

    @Before
    public void setUp(){
        antaresConnectorMock = mock(AntaresConnector.class);
        mapperMock = mock(MapperToModel.class);
    }

    @Test
    public void getItineraries_params_shouldReturnAListOfItinerariesResponses(){
        when(antaresConnectorMock.getResponse(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(new AntaresResponse(new ArrayList<AntaresItinerary>()));
        when(mapperMock.transformAntaresToModel(anyList()))
                .thenReturn(new ArrayList<ItineraryResponse>());
        service.setMapper(mapperMock);
        service.setAntaresConnector(antaresConnectorMock);
        List<ItineraryResponse> list = service.getItineraries("BUE", "MIA", "2018-03-10", "2018-03-15");

        assertNotNull(list);
        verify(antaresConnectorMock).getResponse(anyString(), anyString(), anyString(), anyString());
        verify(mapperMock).transformAntaresToModel(anyList());
    }
}
