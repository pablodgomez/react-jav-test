package com.despegar.javAncillaries;

import com.despegar.javAncillaries.exceptions.InvalidFileException;
import com.despegar.javAncillaries.exceptions.RecommendationException;
import com.despegar.javAncillaries.mappers.MapperToModel;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.AncillaryType;
import com.despegar.javAncillaries.services.antares.AntaresResponse;
import com.despegar.javAncillaries.services.recommendations.RecommendationResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


public class MapperToModelTest {

    private MapperToModel mapper;
    private Gson gson = new Gson();

    @Before
    public void setUp(){
        mapper = new MapperToModel();
    }

    /************** ---Itineraries--- **************/

    @Test
    public void transformToModel_itineraries_shouldReturnAList(){
        String json = this.getFileContent("jsons/itineraries/oneItinerary");
        AntaresResponse antaresResponse = gson.fromJson(json, AntaresResponse.class);
        List<ItineraryResponse> response = mapper.transformAntaresToModel(antaresResponse.getItineraries());

        assertFalse(response.isEmpty());
    }

    @Test
    public void transformToModel_itineraries_shouldReturnAnEmptyList(){
        AntaresResponse antaresResponse = gson.fromJson("{\"items\":[]}", AntaresResponse.class);
        List<ItineraryResponse> response = mapper.transformAntaresToModel(antaresResponse.getItineraries());

        assertTrue(response.isEmpty());
    }

    @Test
    public void transformToModel_itineraries_shouldReturnAListWithOneElement(){
        String json = this.getFileContent("jsons/itineraries/oneItinerary");
        AntaresResponse antaresResponse = gson.fromJson(json, AntaresResponse.class);
        List<ItineraryResponse> response = mapper.transformAntaresToModel(antaresResponse.getItineraries());

        assertEquals(response.size(), 1);
    }

    @Test
    public void transformToModel_itineraries_shouldHandleMoreThanOneSegment(){
        String json = this.getFileContent("jsons/itineraries/itineraryWithSeveralSegments");
        AntaresResponse antaresResponse = gson.fromJson(json, AntaresResponse.class);
                List<ItineraryResponse> response = mapper.transformAntaresToModel(antaresResponse.getItineraries());

        ItineraryResponse firstOption = response.get(0);

        Route outRoute = firstOption.getRoutes().get(0);
        Route inRoute = firstOption.getRoutes().get(1);

        assertEquals("EZE", outRoute.getFrom());
        assertEquals("ORD", outRoute.getTo());
        assertEquals("14:10", outRoute.getDuration());
        assertEquals("2018-07-01T20:20:00.000-03:00", outRoute.getDepartureDate());
        assertEquals("2018-07-02T08:30:00.000-05:00", outRoute.getArrivalDate());
        assertEquals("ORD", inRoute.getFrom());
        assertEquals("EZE", inRoute.getTo());
        assertEquals("12:50", inRoute.getDuration());
        assertEquals("2018-07-22T18:30:00.000-05:00", inRoute.getDepartureDate());
        assertEquals("2018-07-23T09:20:00.000-03:00", inRoute.getArrivalDate());
    }

    @Test
    public void transformToModel_itinerariesWithEmptyBaggage_shouldReturnBaggageQuantity0(){
        String json = this.getFileContent("jsons/itineraries/itineraryEmptyBaggage");
        AntaresResponse antaresResponse = gson.fromJson(json, AntaresResponse.class);
        List<ItineraryResponse> response = mapper.transformAntaresToModel(antaresResponse.getItineraries());

        ItineraryResponse answer = response.get(0);
        int quantity = answer.getRoutes().get(0).getAncillaries().getBaggage().getQuantity();

        assertEquals(0, quantity);
    }

    @Test
    public void transformToModel_itinerariesWithoutMeal_shouldReturnMealQuantity0(){
        String json = this.getFileContent("jsons/itineraries/itineraryEmptyMeal");
        AntaresResponse antaresResponse = gson.fromJson(json, AntaresResponse.class);
        List<ItineraryResponse> response = mapper.transformAntaresToModel(antaresResponse.getItineraries());

        ItineraryResponse answer = response.get(0);
        int quantity = answer.getRoutes().get(0).getAncillaries().getMeal().getQuantity();

        assertEquals(0, quantity);
    }

    /************** ---Recommendations--- **************/

    @Test
    public void transformToRecommendation_recommendations_shouldReturnABaggageRecommendation(){
        String json = this.getFileContent("jsons/recommendations/baggageRecommendationQuantity5");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();

        List<RecommendationResponse> response = gson.fromJson(json, recommendationsType);

        assertEquals(AncillaryType.BAGGAGE, mapper.transformToRecommendation(response).get(0).getType());
    }

    @Test
    public void transformToRecommendation_recommendations_shouldReturnAMealRecommendation(){
        String json = this.getFileContent("jsons/recommendations/mealRecommendationQuantity2");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();

        List<RecommendationResponse> response = gson.fromJson(json, recommendationsType);

        assertEquals(AncillaryType.MEAL, mapper.transformToRecommendation(response).get(0).getType());
    }

    @Test
    public void transformToRecommendation_recommendations_shouldReturnAEntertainmentRecommendation(){
        String json = this.getFileContent("jsons/recommendations/entertainmentRecommendationQuantity1");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();

        List<RecommendationResponse> response = gson.fromJson(json, recommendationsType);

        assertEquals(AncillaryType.ENTERTAINMENT, mapper.transformToRecommendation(response).get(0).getType());
    }

    @Test
    public void transformToRecommendation_recommendations_shouldReturnListSize2(){
        String json = this.getFileContent("jsons/recommendations/backRecommendations");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();

        List<RecommendationResponse> response = gson.fromJson(json, recommendationsType);

        assertEquals(2, mapper.transformToRecommendation(response).size());
    }

    @Test
    public void transformToRecommendation_emptyList_shouldReturnAnEmptyList(){
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();
        List<RecommendationResponse> response = gson.fromJson("[]", recommendationsType);

        assertEquals(0, mapper.transformToRecommendation(response).size());
    }

    @Test
    public void transformToRecommendation_fourRecommendations_shouldReturnThreeRecommendations(){
        String json = this.getFileContent("jsons/recommendations/fourRecommendations");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();

        List<RecommendationResponse> response = gson.fromJson(json, recommendationsType);

        assertEquals(3, mapper.transformToRecommendation(response).size());
    }

    /************** ---HELPFUL METHODS--- **************/

    private String getFileContent(String location){
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(location).getFile());
            String str = FileUtils.readFileToString(file);
            return str;
        } catch (IOException ioe) {
            throw new InvalidFileException("The file location is invalid", ioe);
        }
    }

}
