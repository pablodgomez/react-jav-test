package com.despegar.javAncillaries;

import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ancillaries.AncillaryType;
import com.despegar.javAncillaries.model.ancillaries.SoldAncillaries;
import com.despegar.javAncillaries.repositories.SoldAncillariesRepository;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class SoldAncillariesRepositoryTest {

    private SoldAncillariesRepository repository;
    private DateTimeFormatter inputFormat = DateTimeFormat.forPattern("yyyy-MM");
    private DateTimeFormatter inputFormatWithDay = DateTimeFormat.forPattern("yyyy-MM-dd");

    @Before
    public void setUp(){
        repository = new SoldAncillariesRepository();
        repository.setAncillaries(new ArrayList<>());
        repository.setMapper(new MapperToResponse());
        List<AncillariesRequest> ancillaries = new ArrayList<>();
        LocalDate date = LocalDate.parse("2001-03", inputFormat);
        ancillaries.add(new AncillariesRequest("BUE", "MIA", date, AncillaryType.BAGGAGE, 3));
        ancillaries.add(new AncillariesRequest("BUE", "MIA", date, AncillaryType.MEAL, 2));
        date = LocalDate.parse("2018-07", inputFormat);
        ancillaries.add(new AncillariesRequest("BUE", "MIA", date, AncillaryType.MEAL, 1));
        ancillaries.add(new AncillariesRequest("BUE", "MIA", date, AncillaryType.MEAL, 9));
        date = LocalDate.parse("2015-03-10", inputFormatWithDay);
        ancillaries.add(new AncillariesRequest("SIN", "DUB", date, AncillaryType.ENTERTAINMENT, 2));
        ancillaries.add(new AncillariesRequest("SIN", "DUB", date, AncillaryType.MEAL, 4));
        date = LocalDate.parse("2015-03-12", inputFormatWithDay);
        ancillaries.add(new AncillariesRequest("SIN", "DUB", date, AncillaryType.BAGGAGE, 10));
        date = LocalDate.parse("2017-12", inputFormat);
        ancillaries.add(new AncillariesRequest("MAD", "TOK", date, AncillaryType.BAGGAGE, 7));
        date = LocalDate.parse("2011-11", inputFormat);
        ancillaries.add(new AncillariesRequest("RUM", "VEN", date, AncillaryType.ENTERTAINMENT, 3));
        ancillaries.add(new AncillariesRequest("RUM", "VEN", date, AncillaryType.MEAL, 5));

        repository.addAncillaries(ancillaries);
    }

    @Test
    public void getSoldAncillariesFrom_wrongDate_shouldReturnAnEmptyList(){
        LocalDate date = LocalDate.parse("2007-01", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "BUE", "MIA");

        assertEquals(0, ancillaries.size());
    }

    @Test
    public void getSoldAncillariesFrom_wrongFrom_shouldReturnAnEmptyList(){
        LocalDate date = LocalDate.parse("2001-03", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "RUM", "MIA");

        assertEquals(0, ancillaries.size());
    }

    @Test
    public void getSoldAncillariesFrom_wrongTo_shouldReturnAnEmptyList(){
        LocalDate date = LocalDate.parse("2001-03", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "BUE", "MIO");

        assertEquals(0, ancillaries.size());
    }

    @Test
    public void getSoldAncillariesFrom_dateFromTo_shouldReturnElementsMatchingFromAndTo(){
        LocalDate date = LocalDate.parse("2017-12", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "MAD", "TOK");

        assertEquals(1, ancillaries.size());
        assertEquals("MAD", ancillaries.get(0).getFrom());
        assertEquals("TOK", ancillaries.get(0).getTo());
    }

    @Test
    public void getSoldAncillariesFrom_dateFromTo_shouldReturnOneElementMatchingBaggages(){
        LocalDate date = LocalDate.parse("2017-12", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "MAD", "TOK");

        assertEquals(1, ancillaries.size());
        assertEquals(7, ancillaries.get(0).getBaggages());
    }

    @Test
    public void getSoldAncillariesFrom_dateFromTo_shouldCombineTwoIntoOneElement(){
        LocalDate date = LocalDate.parse("2011-11", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "RUM", "VEN");

        assertEquals(1, ancillaries.size());
        assertEquals(5, ancillaries.get(0).getMeals());
        assertEquals(3, ancillaries.get(0).getEntertainments());
    }

    @Test
    public void getSoldAncillariesFrom_dateFromTo_shouldReturnOneElementWithAncillariesCombined(){
        LocalDate date = LocalDate.parse("2018-07", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "BUE", "MIA");

        assertEquals(1, ancillaries.size());
        assertEquals(10, ancillaries.get(0).getMeals());
    }

    @Test
    public void getSoldAncillariesFrom_dateFromTo_shouldReturnTwoElements(){
        LocalDate date = LocalDate.parse("2015-03", inputFormat);
        List<SoldAncillaries> ancillaries = repository.getSoldAncillariesFrom(date, "SIN", "DUB");

        assertEquals(2, ancillaries.size());

        SoldAncillaries first = ancillaries.stream().filter(a -> a.getDate().toString().equals("2015-03-10")).collect(Collectors.toList()).get(0);
        SoldAncillaries second = ancillaries.stream().filter(a -> a.getDate().toString().equals("2015-03-12")).collect(Collectors.toList()).get(0);

        assertEquals(4, first.getMeals());
        assertEquals(2, first.getEntertainments());
        assertEquals(10, second.getBaggages());
    }

}
