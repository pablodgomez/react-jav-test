package com.despegar.javAncillaries;

import com.despegar.javAncillaries.exceptions.InvalidFileException;
import com.despegar.javAncillaries.mappers.MapperToModel;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.Recommendation;
import com.despegar.javAncillaries.model.RecommendationPack;
import com.despegar.javAncillaries.services.antares.AntaresResponse;
import com.despegar.javAncillaries.services.recommendations.RecommendationResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public abstract class TestUtils {

    private Gson gson = new Gson();
    private MapperToModel mapper = new MapperToModel();

    protected String getFileContent(String location) {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource(location).getFile());
            String str = FileUtils.readFileToString(file);
            return str;
        } catch (IOException ioe) {
            throw new InvalidFileException("The file location is invalid", ioe);
        }
    }

    protected List<RecommendationResponse> getRecommendationResponse() {
        String json = this.getFileContent("jsons/recommendations/toRecommendation");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>() {
        }.getType();
        return gson.fromJson(json, recommendationsType);
    }

    protected List<Recommendation> getToRecommendations() {
        String json = this.getFileContent("jsons/recommendations/toRecommendation");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>() {
        }.getType();
        List<RecommendationResponse> recommendations = gson.fromJson(json, recommendationsType);
        return mapper.transformToRecommendation(recommendations);
    }

    protected List<Recommendation> getMealRecommendation(int quantity) {
        String json = this.getFileContent("jsons/recommendations/mealRecommendationQuantity" + quantity);
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>() {
        }.getType();
        List<RecommendationResponse> recommendations = gson.fromJson(json, recommendationsType);
        return mapper.transformToRecommendation(recommendations);
    }

    protected List<Recommendation> getEntertainmentRecommendation(int quantity) {
        String json = this.getFileContent("jsons/recommendations/entertainmentRecommendationQuantity" + quantity);
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>() {
        }.getType();
        List<RecommendationResponse> recommendations = gson.fromJson(json, recommendationsType);
        return mapper.transformToRecommendation(recommendations);
    }

    protected List<Recommendation> getBaggageRecommendation(int quantity){
        String json = this.getFileContent("jsons/recommendations/baggageRecommendationQuantity" + quantity);
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();
        List<RecommendationResponse> recommendations = gson.fromJson(json, recommendationsType);
        return mapper.transformToRecommendation(recommendations);
    }

    protected List<Recommendation> getBackRecommendations(){
        String json = this.getFileContent("jsons/recommendations/backRecommendations");
        Type recommendationsType = new TypeToken<ArrayList<RecommendationResponse>>(){}.getType();
        List<RecommendationResponse> recommendations = gson.fromJson(json, recommendationsType);
        return mapper.transformToRecommendation(recommendations);
    }

    protected ItineraryResponse getItinerariesResponseSample(){
        String json = this.getFileContent("jsons/itinerariesResponses/sampleResponse");
        return gson.fromJson(json, ItineraryResponse.class);
    }

    protected List<ItineraryResponse> getItineraryResponses(){
        String json = this.getFileContent("jsons/itinerariesResponses/threeResponses");
        Type ancillariesResponseType = new TypeToken<ArrayList<ItineraryResponse>>(){}.getType();

        return gson.fromJson(json, ancillariesResponseType);
    }

    protected AntaresResponse getAntaresResponse(){
        String json = this.getFileContent("jsons/itineraries/50itineraries");

        return gson.fromJson(json, AntaresResponse.class);
    }

}
