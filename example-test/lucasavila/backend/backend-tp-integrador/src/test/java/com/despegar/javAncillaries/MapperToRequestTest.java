package com.despegar.javAncillaries;

import com.despegar.javAncillaries.mappers.MapperToRequest;
import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.*;
import com.despegar.javAncillaries.model.booking.ItineraryBooking;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class MapperToRequestTest {

    private MapperToRequest mapper;
    private Route routeMock;

    @Before
    public void setUp(){
        mapper = new MapperToRequest();
        routeMock = mock(Route.class);
    }

    @Test
    public void transformToItineraryBookingRequest_idAndName_shouldReturnAnItineraryBooking(){
        ItineraryBooking itineraryBooking = mapper.transformToItineraryBookingRequest("test", "Max Power");

        assertEquals("test", itineraryBooking.getItineraryId());
        assertEquals("Max", itineraryBooking.getPassengers().get(0).getName());
        assertEquals("Power", itineraryBooking.getPassengers().get(0).getLastName());
    }

    @Test
    public void transformToAncillariesRequest_inputParams_shouldReturnAncillariesRequestWithBaggage(){
        Ancillaries ancillaries = new Ancillaries(new Baggage(3), new Meal(0), new Entertainment(0));
        when(routeMock.getAdditionalAncillaries()).thenReturn(ancillaries);

        List<AncillariesRequest> requests = mapper.transformToAncillariesRequest("BUE", "MIA", routeMock, "007");

        assertTrue(!requests.isEmpty());
        assertEquals(1, requests.size());
        assertEquals(AncillaryType.BAGGAGE, requests.get(0).getType());
        assertEquals(3, requests.get(0).getQuantity());
    }

    @Test
    public void transformToAncillariesRequest_inputParams_shouldReturnAncillariesRequestWithMeal(){
        Ancillaries ancillaries = new Ancillaries(new Baggage(0), new Meal(1), new Entertainment(0));
        when(routeMock.getAdditionalAncillaries()).thenReturn(ancillaries);

        List<AncillariesRequest> requests = mapper.transformToAncillariesRequest("BUE", "MIA", routeMock, "007");

        assertTrue(!requests.isEmpty());
        assertEquals(1, requests.size());
        assertEquals(AncillaryType.MEAL, requests.get(0).getType());
        assertEquals(1, requests.get(0).getQuantity());
    }

    @Test
    public void transformToAncillariesRequest_inputParams_shouldReturnAncillariesRequestWithEntertainment(){
        Ancillaries ancillaries = new Ancillaries(new Baggage(0), new Meal(0), new Entertainment(3));
        when(routeMock.getAdditionalAncillaries()).thenReturn(ancillaries);

        List<AncillariesRequest> requests = mapper.transformToAncillariesRequest("BUE", "MIA", routeMock, "007");

        assertTrue(!requests.isEmpty());
        assertEquals(1, requests.size());
        assertEquals(AncillaryType.ENTERTAINMENT, requests.get(0).getType());
        assertEquals(3, requests.get(0).getQuantity());
    }

    @Test
    public void transformToAncillariesRequest_inputParams_shouldReturnAncillariesRequestWithAllOfThem(){
        Ancillaries ancillaries = new Ancillaries(new Baggage(1), new Meal(2), new Entertainment(3));
        when(routeMock.getAdditionalAncillaries()).thenReturn(ancillaries);

        List<AncillariesRequest> requests = mapper.transformToAncillariesRequest("BUE", "MIA", routeMock, "007");

        assertTrue(!requests.isEmpty());
        assertEquals(3, requests.size());
        assertEquals(AncillaryType.BAGGAGE, requests.get(0).getType());
        assertEquals(1, requests.get(0).getQuantity());
        assertEquals(AncillaryType.MEAL, requests.get(1).getType());
        assertEquals(2, requests.get(1).getQuantity());
        assertEquals(AncillaryType.ENTERTAINMENT, requests.get(2).getType());
        assertEquals(3, requests.get(2).getQuantity());
    }

}
