package com.despegar.javAncillaries;

import com.despegar.javAncillaries.exceptions.ParameterException;
import com.despegar.javAncillaries.utils.ParamsValidator;
import org.junit.Test;

public class ParamsValidatorTest {

    private ParamsValidator validator = new ParamsValidator();

    @Test(expected = ParameterException.class)
    public void checkName_aName_shouldThrowException(){
        validator.checkName("Max");
    }

    @Test
    public void checkName_aName_shouldPass(){
        validator.checkName("Max Power");
    }

    @Test(expected = ParameterException.class)
    public void checkMonth_aMonth_shouldThrowException_dueToLongLength(){
        validator.checkMonth("2018---");
    }

    @Test(expected = ParameterException.class)
    public void checkMonth_aMonth_shouldThrowException_dueToSmallLength(){
        validator.checkMonth("2018");
    }

    @Test(expected = ParameterException.class)
    public void checkMonth_aMonth_shouldThrowException_dueToParseException(){
        validator.checkMonth("201810-");
    }

    @Test
    public void checkMonth_aMonth_shouldPass(){
        validator.checkMonth("2018-01");
    }

    @Test(expected = ParameterException.class)
    public void checkClientType_type_shouldThrowException_wrongType(){
        validator.checkClientType("AUSTEREEE");
    }

    @Test(expected = ParameterException.class)
    public void checkClientType_type_shouldThrowException_wrongTypev2(){
        validator.checkClientType("ANSIOUS");
    }

    @Test
    public void checkClientType_type_shouldPass_lowCaseType(){
        validator.checkClientType("standard");
    }

    @Test
    public void checkClientType_type_shouldPass_mixedCase(){
        validator.checkClientType("CaUtIoUs");
    }

    @Test(expected = ParameterException.class)
    public void checkPlace_aLocation_shouldThrowException_emptyString(){
        validator.checkPlace("");
    }

    @Test(expected = ParameterException.class)
    public void checkPlace_aLocation_shouldThrowException_null(){
        validator.checkPlace(null);
    }

    @Test(expected = ParameterException.class)
    public void checkPlace_aLocation_shouldThrowException_wrongLength(){
        validator.checkPlace("RIOO");
    }

    @Test(expected = ParameterException.class)
    public void checkPlace_aLocation_shouldThrowException_wrongRegex(){
        validator.checkPlace("R1O");
    }

    @Test
    public void checkPlace_aLocation_shouldPass(){
        validator.checkPlace("RIO");
    }

    @Test(expected = ParameterException.class)
    public void checkInputParams_fromDate_shouldThrowException_fromDateEmpty(){
        validator.checkInputParams("BUE", "JAP", "", "2018-10-10");
    }

    @Test
    public void checkInputParams_fromDate_shouldPass_toDateEmpty(){
        validator.checkInputParams("BUE", "JAP", "2020-03-05", "");
    }

    @Test(expected = ParameterException.class)
    public void checkInputParams_fromDate_shouldThrowException_fromDateIsToOld(){
        validator.checkInputParams("BUE", "JAP", "2017-03-05", "");
    }

    @Test(expected = ParameterException.class)
    public void checkInputParams_fromDate_shouldThrowException_cannotFlyToThePast(){
        validator.checkInputParams("BUE", "JAP", "2020-03-05", "2017-03-03");
    }

    @Test
    public void checkInputParams_fromDate_shouldPass(){
        validator.checkInputParams("BUE", "JAP", "2020-03-05", "2025-03-03");
    }

    @Test(expected = ParameterException.class)
    public void checkInputParams_fromDate_shouldThrowException_InvalidDate29Feb(){
        validator.checkInputParams("BUE", "JAP", "2019-02-29", "2019-03-03");
    }
}
