package com.despegar.javAncillaries;

import com.despegar.javAncillaries.connectors.AncillariesConnector;
import com.despegar.javAncillaries.exceptions.AncillariesBookingException;
import com.despegar.javAncillaries.exceptions.HttpRequestException;
import com.despegar.javAncillaries.mappers.MapperToRequest;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.AncillaryType;
import com.despegar.javAncillaries.repositories.SoldAncillariesRepository;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import com.despegar.javAncillaries.services.ancillaries.AncillariesService;
import com.despegar.javAncillaries.services.ancillaries.ItineraryService;
import com.despegar.javAncillaries.services.bookings.BookingService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class AncillariesServiceTest extends TestUtils{

    private AncillariesService service = new AncillariesService();
    private AncillariesConnector ancillariesConnectorMock;
    private BookingService bookingServiceMock;
    private SoldAncillariesRepository repositoryMock;
    private MapperToRequest mapperMock;
    private List<AncillariesRequest> firstRequests;
    private List<AncillariesRequest> secondRequests;

    @Before
    public void setUp(){
        repositoryMock = mock(SoldAncillariesRepository.class);
        mapperMock = mock(MapperToRequest.class);
        ancillariesConnectorMock = mock(AncillariesConnector.class);
        bookingServiceMock = mock(BookingService.class);

        firstRequests = new ArrayList<>();
        secondRequests = new ArrayList<>();
        firstRequests.add(new AncillariesRequest("003", "MAD", "USA", AncillaryType.BAGGAGE, 3));
        firstRequests.add(new AncillariesRequest("001", "MAD", "USA", AncillaryType.MEAL, 1));
        firstRequests.add(new AncillariesRequest("002", "GER", "UKI", AncillaryType.MEAL, 1));
        secondRequests.add(new AncillariesRequest("005", "MAD", "JAP", AncillaryType.ENTERTAINMENT, 2));
        secondRequests.add(new AncillariesRequest("021", "MAD", "JAP", AncillaryType.ENTERTAINMENT, 2));
        secondRequests.add(new AncillariesRequest("101", "ARG", "UPA", AncillaryType.BAGGAGE, 6));
        //6 elements in total.
    }

    @Test
    public void getAllAncillariesIds_fromToItineraryBookId_shouldReturnAncillariesIds(){
        when(mapperMock.transformToAncillariesRequest(any(String.class), any(String.class), any(Route.class), any(String.class)))
                .thenReturn(firstRequests)
                .thenReturn(secondRequests);
        when(ancillariesConnectorMock.getResponse(any(AncillariesRequest.class)))
                .thenReturn("ID01")
                .thenReturn("ID02")
                .thenReturn("ID03")
                .thenReturn("ID04")
                .thenReturn("ID05")
                .thenReturn("ID06");
        doNothing().when(repositoryMock).addAncillaries(anyList());
        ItineraryResponse itinerary = this.getItinerariesResponseSample();
        service.setAncillariesConnector(ancillariesConnectorMock);
        service.setMapper(mapperMock);
        service.setRepository(repositoryMock);

        List<String> ids = service.getAncillariesIds("RIO", "RUS", itinerary, "despegar.com");

        assertFalse(ids.isEmpty());
        assertEquals(6, ids.size());
        assertTrue(ids.contains("ID01"));
        assertTrue(ids.contains("ID02"));
        assertTrue(ids.contains("ID03"));
        assertTrue(ids.contains("ID04"));
        assertTrue(ids.contains("ID05"));
        assertTrue(ids.contains("ID06"));
    }

    @Test(expected = AncillariesBookingException.class)
    public void getAllAncillariesIds_fromToItineraryBookId_shouldThrowsException(){
        when(mapperMock.transformToAncillariesRequest(any(String.class), any(String.class), any(Route.class), any(String.class)))
                .thenReturn(firstRequests)
                .thenReturn(secondRequests);
        when(ancillariesConnectorMock.getResponse(any(AncillariesRequest.class)))
                .thenReturn("ID01")
                .thenReturn("ID02")
                .thenReturn("ID03")
                .thenThrow(HttpRequestException.class);
        doNothing().when(bookingServiceMock).cancel(anyString());
        doNothing().when(repositoryMock).addAncillaries(anyList());
        ItineraryResponse itinerary = this.getItinerariesResponseSample();
        service.setBookingService(bookingServiceMock);
        service.setAncillariesConnector(ancillariesConnectorMock);
        service.setMapper(mapperMock);
        service.setRepository(repositoryMock);

        service.getAncillariesIds("RIO", "RUS", itinerary, "despegar.com");
    }
}
