package com.despegar.javAncillaries;

import com.despegar.javAncillaries.model.ancillaries.SoldAncillaries;
import com.despegar.javAncillaries.repositories.SoldAncillariesRepository;
import com.despegar.javAncillaries.services.reports.ReportService;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class ReportServiceTest {

    private ReportService service = new ReportService();
    private SoldAncillariesRepository repositoryMock;

    @Before
    public void setUp(){
        repositoryMock = mock(SoldAncillariesRepository.class);
    }

    @Test
    public void getSoldAncillariesIn_monthFromTo_shouldReturnAList(){
        when(repositoryMock.getSoldAncillariesFrom(any(LocalDate.class), anyString(), anyString()))
                .thenReturn(new ArrayList<SoldAncillaries>());
        service.setRepository(repositoryMock);

        List<SoldAncillaries> response = service.getSoldAncillariesIn("2019-01", "BUE", "MAD");

        assertNotNull(response);
        verify(repositoryMock).getSoldAncillariesFrom(any(LocalDate.class), anyString(), anyString());
    }

}
