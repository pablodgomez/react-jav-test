package com.despegar.javAncillaries;

import com.despegar.javAncillaries.connectors.BookingConnector;
import com.despegar.javAncillaries.mappers.MapperToRequest;
import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.LocationPack;
import com.despegar.javAncillaries.model.Recommendation;
import com.despegar.javAncillaries.model.booking.ItineraryBooking;
import com.despegar.javAncillaries.services.ancillaries.ItineraryService;
import com.despegar.javAncillaries.services.antares.AntaresService;
import com.despegar.javAncillaries.services.bookings.ItineraryBookingResponse;
import com.despegar.javAncillaries.services.recommendations.RecommendationsService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ItineraryServiceTest extends TestUtils{

    private AntaresService antaresServiceMock;
    private BookingConnector bookingConnectorMock;
    private MapperToRequest mapperMock;
    private RecommendationsService recommendationsServiceMock;
    private MapperToResponse mapperToResponseMock;
    private ItineraryService service = new ItineraryService();

    @Before
    public void setUp(){
        antaresServiceMock = mock(AntaresService.class);
        bookingConnectorMock = mock(BookingConnector.class);
        mapperMock = mock(MapperToRequest.class);
        recommendationsServiceMock = mock(RecommendationsService.class);
        mapperToResponseMock = mock(MapperToResponse.class);
    }

    @Test
    public void getResponse_params_shouldReturnAnItinerary(){
        HashMap<LocationPack, List<Recommendation>> recommendations = new HashMap<>();
        recommendations.put(new LocationPack("EZE", "MIA"), this.getBaggageRecommendation(5));
        recommendations.put(new LocationPack("MIA", "EZE"), this.getBaggageRecommendation(5));
        recommendations.put(new LocationPack("CHI", "MIA"), this.getBaggageRecommendation(5));
        recommendations.put(new LocationPack("RIO", "EZE"), this.getBaggageRecommendation(5));
        List<ItineraryResponse> responses = this.getItineraryResponses();
        when(antaresServiceMock.getItineraries(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(responses);
        when(recommendationsServiceMock.getRecommendations(any()))
                .thenReturn(recommendations);
        when(mapperToResponseMock.mapToFinalItineraryResponse(any(), any())).thenReturn(this.getItinerariesResponseSample());
        service.setAntaresService(antaresServiceMock);
        service.setRecommendationsService(recommendationsServiceMock);
        service.setMapperToResponse(mapperToResponseMock);
        ItineraryResponse answer = service.getResponse("BUE", "MIA", "2018-03-15","2018-03-25", "Anxious");

        assertNotNull(answer);
        verify(antaresServiceMock).getItineraries(anyString(), anyString(), anyString(), anyString());
        verify(mapperToResponseMock, times(3)).mapToFinalItineraryResponse(any(), any());
    }

}
