package com.despegar.javAncillaries;

import com.despegar.javAncillaries.connectors.AntaresConnector;
import com.despegar.javAncillaries.connectors.RecommendationsConnector;
import com.despegar.javAncillaries.services.antares.AntaresResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { MainApp.class })
@WebAppConfiguration
public class ItinerariesTest extends TestUtils{

    @Autowired
    private WebApplicationContext webAppContext;
    private MockMvc mockMvc;
    @Autowired
    private AntaresConnector antaresConnectorMock;
    @Autowired
    private RecommendationsConnector recommendationsConnectorMock;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webAppContext).build();

    }

    @Test
    public void givenWac_whenServletContext_thenItProvidesGreetController() {
        ServletContext servletContext = webAppContext.getServletContext();

        assertNotNull(servletContext);
        assertTrue(servletContext instanceof MockServletContext);
        assertNotNull(webAppContext.getBean("itineraryService"));
    }

    @Test
    public void getItinerariesResponse_travelData_shouldReturnBadRequestMissingFrom() throws Exception{
        when(antaresConnectorMock.getResponse(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(this.getAntaresResponse());

        mockMvc.perform(get("/despegar-it-jav/itinerary").param("from", "BUE"))
                    .andExpect(status().is4xxClientError());
    }

    @Test
    public void getItinerariesResponse_travelData_shouldReturnBadRequestMissingTo() throws Exception{
        when(antaresConnectorMock.getResponse(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(this.getAntaresResponse());

        mockMvc.perform(get("/despegar-it-jav/itinerary").param("to", "MIA"))
                    .andExpect(status().is4xxClientError());
    }

    @Test
    public void getItinerariesResponse_travelData_shouldReturnBadRequestMissingFromDate() throws Exception{
        when(antaresConnectorMock.getResponse(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(this.getAntaresResponse());

        mockMvc.perform(get("/despegar-it-jav/itinerary").param("from", "BUE").param("to", "MIA"))
                    .andExpect(status().is4xxClientError());
    }

    @Test
    public void getItinerariesResponse_travelData_shouldReturnBadRequestMissingClientType() throws Exception{
        when(antaresConnectorMock.getResponse(anyString(), anyString(), anyString(), anyString()))
                .thenReturn(this.getAntaresResponse());

        mockMvc.perform(get("/despegar-it-jav/itinerary").param("from", "BUE").param("to", "MIA").param("from_date", "2018-03-18"))
                    .andExpect(status().is4xxClientError());
    }

    @Test
    public void getItinerariesResponse_travelData_shouldReturnAJson() throws Exception{
        AntaresResponse response = this.getAntaresResponse();
        when(antaresConnectorMock.getResponse(any(), any(), any(), any()))
                .thenReturn(response);
        when(recommendationsConnectorMock.getResponse(any(), any()))
                .thenReturn(this.getRecommendationResponse());
        String contentType = new String();

        contentType = mockMvc.perform(get("/despegar-it-jav/itinerary").param("from", "BUE").param("to", "MIA").param("from_date", "2020-03-18").param("client_type", "AUSTERE"))
                    .andExpect(status().is2xxSuccessful())
                    .andReturn().getResponse().getContentType();

        assertTrue(contentType.contains("application/json"));
        verify(antaresConnectorMock).getResponse(any(), any(), any(), any());
    }
}
