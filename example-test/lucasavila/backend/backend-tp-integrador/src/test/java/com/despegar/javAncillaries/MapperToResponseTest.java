package com.despegar.javAncillaries;

import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.ancillaries.*;
import com.despegar.javAncillaries.services.ancillaries.AncillariesRequest;
import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MapperToResponseTest extends TestUtils{

    private MapperToResponse mapper = new MapperToResponse();
    private List<AncillariesRequest> ancillaries;

    @Before
    public void setUp() {
        ancillaries = new ArrayList<>();
    }

    @Test
    public void mapToSoldAncillaries_fromToAncillaries_shouldReturnAllAncillariesCombinedInOne(){
        ancillaries.add(new AncillariesRequest("MAD", "USA", new LocalDate(), AncillaryType.BAGGAGE, 3));
        ancillaries.add(new AncillariesRequest("MAD", "USA", new LocalDate(), AncillaryType.MEAL, 1));
        ancillaries.add(new AncillariesRequest("GER", "UKI", new LocalDate(), AncillaryType.MEAL, 1));
        ancillaries.add(new AncillariesRequest("MAD", "JAP", new LocalDate(), AncillaryType.ENTERTAINMENT, 2));
        ancillaries.add(new AncillariesRequest("MAD", "JAP", new LocalDate(), AncillaryType.ENTERTAINMENT, 2));
        ancillaries.add(new AncillariesRequest("ARG", "UPA", new LocalDate(),AncillaryType.BAGGAGE, 6));

        List<SoldAncillaries> soldAncillaries = mapper.mapToSoldAncillaries("JAP", "BEL", ancillaries);

        assertFalse(soldAncillaries.isEmpty());
        /* Checking locations */
        assertEquals("JAP", soldAncillaries.get(0).getFrom());
        assertEquals("BEL", soldAncillaries.get(0).getTo());
        /* Checking quantities */
        assertEquals(1, soldAncillaries.size());
        assertEquals(9, soldAncillaries.get(0).getBaggages());
        assertEquals(2, soldAncillaries.get(0).getMeals());
        assertEquals(4, soldAncillaries.get(0).getEntertainments());
    }

    @Test
    public void mapToSoldAncillaries_fromToAncillaries_shouldReturnThreeSoldAncillaries(){
        ancillaries.add(new AncillariesRequest("MAD", "USA", new LocalDate("2020-03-03"), AncillaryType.BAGGAGE, 3));
        ancillaries.add(new AncillariesRequest("MAD", "USA", new LocalDate("2020-03-03"), AncillaryType.MEAL, 1));
        ancillaries.add(new AncillariesRequest("GER", "UKI", new LocalDate("2015-03-03"), AncillaryType.MEAL, 1));
        ancillaries.add(new AncillariesRequest("MAD", "JAP", new LocalDate("2015-03-03"), AncillaryType.ENTERTAINMENT, 2));
        ancillaries.add(new AncillariesRequest("MAD", "JAP", new LocalDate("2033-03-03"), AncillaryType.ENTERTAINMENT, 2));
        ancillaries.add(new AncillariesRequest("ARG", "UPA", new LocalDate("2033-03-03"),AncillaryType.BAGGAGE, 6));

        List<SoldAncillaries> soldAncillaries = mapper.mapToSoldAncillaries("JAP", "BEL", ancillaries);

        assertFalse(soldAncillaries.isEmpty());
        /* Checking locations */
        assertEquals("JAP", soldAncillaries.get(0).getFrom());
        assertEquals("BEL", soldAncillaries.get(0).getTo());
        assertEquals("JAP", soldAncillaries.get(1).getFrom());
        assertEquals("BEL", soldAncillaries.get(1).getTo());
        assertEquals("JAP", soldAncillaries.get(2).getFrom());
        assertEquals("BEL", soldAncillaries.get(2).getTo());
        /* Checking quantities */
        assertEquals(3, soldAncillaries.size());
        assertEquals(3, soldAncillaries.get(0).getBaggages());
        assertEquals(1, soldAncillaries.get(0).getMeals());
        assertEquals(1, soldAncillaries.get(1).getMeals());
        assertEquals(2, soldAncillaries.get(1).getEntertainments());
        assertEquals(2, soldAncillaries.get(2).getEntertainments());
        assertEquals(6, soldAncillaries.get(2).getBaggages());
    }

    @Test
    public void mapToFinalItineraryResponse_itineraryAncillariesPack_shouldReturnAnItinerary(){
        ItineraryResponse itinerary = this.getItinerariesResponseSample();
        Ancillaries ancillariesTo = new Ancillaries(new Baggage(1), new Meal(2), new Entertainment(3));
        Ancillaries ancillariesBack = new Ancillaries(new Baggage(1), new Meal(1), new Entertainment(0));
        AncillariesAndPrice ancillariesAndPriceTo = new AncillariesAndPrice(ancillariesTo, 500);
        AncillariesAndPrice ancillariesAndPriceBack = new AncillariesAndPrice(ancillariesBack, 500);
        AncillariesPack pack = new AncillariesPack(ancillariesAndPriceTo, ancillariesAndPriceBack);

        ItineraryResponse finalAnswer = mapper.mapToFinalItineraryResponse(itinerary, pack);

        Ancillaries additionalsTo = finalAnswer.getToRoute().getAdditionalAncillaries();
        Ancillaries additionalsBack = finalAnswer.getBackRoute().getAdditionalAncillaries();

        assertEquals(1, additionalsTo.getSuitcasesQuantity());
        assertEquals(2, additionalsTo.getMealsQuantity());
        assertEquals(3, additionalsTo.getEntertainmentQuantity());
        assertEquals(1, additionalsBack.getSuitcasesQuantity());
        assertEquals(1, additionalsBack.getMealsQuantity());
        assertEquals(0, additionalsBack.getEntertainmentQuantity());
        // Itinerary Price 1000.
        assertEquals(2000, finalAnswer.getPrice().getTotal(), 0);
    }

}
