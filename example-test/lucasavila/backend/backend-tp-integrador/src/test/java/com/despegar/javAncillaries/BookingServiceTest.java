package com.despegar.javAncillaries;

import com.despegar.javAncillaries.connectors.BookingConnector;
import com.despegar.javAncillaries.mappers.MapperToRequest;
import com.despegar.javAncillaries.mappers.MapperToResponse;
import com.despegar.javAncillaries.model.ItineraryResponse;
import com.despegar.javAncillaries.model.Price;
import com.despegar.javAncillaries.model.Route;
import com.despegar.javAncillaries.model.booking.BookingResponse;
import com.despegar.javAncillaries.model.booking.ItineraryBooking;
import com.despegar.javAncillaries.services.ancillaries.AncillariesService;
import com.despegar.javAncillaries.services.ancillaries.ItineraryService;
import com.despegar.javAncillaries.services.bookings.BookingService;
import com.despegar.javAncillaries.services.bookings.ItineraryBookingResponse;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class BookingServiceTest {

    private BookingService service = new BookingService();
    private ItineraryService itineraryServiceMock;
    private AncillariesService ancillariesServiceMock;
    private BookingConnector bookingConnectorMock;
    private MapperToRequest mapperToRequestMock;

    @Before
    public void setUp(){
        itineraryServiceMock = mock(ItineraryService.class);
        ancillariesServiceMock = mock(AncillariesService.class);
        bookingConnectorMock = mock(BookingConnector.class);
        mapperToRequestMock = mock(MapperToRequest.class);
    }

    @Test
    public void getResponse_params_shouldReturnABookingResponse(){
        List<String> ids = new ArrayList<>();
        ids.add("1");
        ids.add("2");
        ids.add("3");
        when(itineraryServiceMock.getResponse(anyString(), anyString(), anyString(), anyString(), anyString()))
                .thenReturn(new ItineraryResponse("003", "AA", new ArrayList<Route>(), new Price(1000.00)));
        when(bookingConnectorMock.getResponse(any()))
                .thenReturn(new ItineraryBookingResponse("1000"));
        when(ancillariesServiceMock.getAncillariesIds(anyString(), anyString(), any(ItineraryResponse.class), anyString()))
                .thenReturn(ids);
        service.setBookingConnector(bookingConnectorMock);
        service.setItineraryService(itineraryServiceMock);
        service.setAncillariesService(ancillariesServiceMock);
        service.setMapperToRequest(mapperToRequestMock);
        BookingResponse response = service.getResponse("MEH", "DES", "2015-05-23", "2017-03-06", "Cautious", "Lucas Avila");

        assertNotNull(response);
        assertEquals("1000", response.getBookId());
        assertEquals("003", response.getItinerary().getItineraryId());
        assertTrue(response.getAdditionalAncillariesIds().contains("1"));
        assertTrue(response.getAdditionalAncillariesIds().contains("2"));
        assertTrue(response.getAdditionalAncillariesIds().contains("3"));

        verify(itineraryServiceMock).getResponse(anyString(), anyString(), anyString(), anyString(), anyString());
        verify(bookingConnectorMock).getResponse(any());
        verify(ancillariesServiceMock).getAncillariesIds(anyString(), anyString(), any(ItineraryResponse.class), anyString());
    }

    @Test
    public void book_itineraryIdPassenger_shouldReturnAnItineraryBookingResponse(){
        when(bookingConnectorMock.getResponse(any(ItineraryBooking.class)))
                .thenReturn(new ItineraryBookingResponse("003"));
        when(mapperToRequestMock.transformToItineraryBookingRequest(anyString(), anyString()))
                .thenReturn(new ItineraryBooking("003", new ArrayList<>()));
        service.setBookingConnector(bookingConnectorMock);
        service.setMapperToRequest(mapperToRequestMock);

        ItineraryBookingResponse response = service.book("BUE", "MIA");

        assertNotNull(response);
        verify(bookingConnectorMock).getResponse(any(ItineraryBooking.class));
        verify(mapperToRequestMock).transformToItineraryBookingRequest(anyString(), anyString());
    }

    @Test
    public void cancel_bookId_shouldCancelBook(){
        doNothing().when(bookingConnectorMock).cancelItinerary(anyString());
        service.setBookingConnector(bookingConnectorMock);

        service.cancel("003");

        verify(bookingConnectorMock).cancelItinerary(anyString());
    }
}
