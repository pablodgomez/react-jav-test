public class Justo extends SistemaPyC {

    @Override
    public boolean juzgar(Muerto muerto) {
        return muerto.getAccionesBuenas() > muerto.getAccionesMalas();
    }
}
