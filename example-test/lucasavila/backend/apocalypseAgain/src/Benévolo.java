public class Benévolo extends SistemaPyC {

    @Override
    public boolean juzgar(Muerto muerto) {
        return muerto.getAccionesBuenas() > 0;
    }
}
