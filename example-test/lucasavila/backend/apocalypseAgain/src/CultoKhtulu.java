public class CultoKhtulu {

    private Khtulu dios;
    private static CultoKhtulu self;

    private CultoKhtulu(){
        this.dios = Khtulu.getInstance();
    }

    public static CultoKhtulu getInstance(){
        if(self == null)
            self = new CultoKhtulu();
        return self;
    }
}
