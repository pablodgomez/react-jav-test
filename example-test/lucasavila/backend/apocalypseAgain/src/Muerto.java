public class Muerto {

    private boolean procesado;
    private boolean salvado;
    private int accionesBuenas;
    private int accionesMalas;
    Religion religion;

    public Muerto(Religion religion){
        this.religion = religion;
        this.accionesBuenas = 0;
        this.accionesMalas = 0;
    }

    public void setSalvado(boolean salvado) {
        this.salvado = salvado;
    }

    public void setProcesado(boolean procesado) {
        this.procesado = procesado;
    }

    public int getAccionesBuenas() {
        return accionesBuenas;
    }

    public int getAccionesMalas() {
        return accionesMalas;
    }
}
