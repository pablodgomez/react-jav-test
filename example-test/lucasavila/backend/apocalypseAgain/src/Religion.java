public abstract class Religion {

    public abstract boolean juzgar(Muerto muerto);

    public void sentencia(Muerto muerto){
        if(this.juzgar(muerto))
            salvar(muerto);
        else
            this.condenar(muerto);

        muerto.setProcesado(true);
    }

    protected void condenar(Muerto muerto){
        muerto.setSalvado(false);
    }

    protected void salvar(Muerto muerto){
        muerto.setSalvado(true);
    }
}
