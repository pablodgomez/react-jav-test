import java.util.List;

public class Griega extends Religion{

    private List<Dios> panteon;
    private static Griega self;

    private Griega(){}

    public static Griega getInstance(){
        if(self == null)
            self = new Griega();
        return self;
    }

    @Override
    public boolean juzgar(Muerto muerto) {
        return panteon.stream().anyMatch(dios -> dios.juzgar(muerto));
    }
}
