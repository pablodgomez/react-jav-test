public class Severo extends SistemaPyC {

    @Override
    public boolean juzgar(Muerto muerto) {
        return muerto.getAccionesMalas() == 0;
    }
}
