import java.util.List;

public class Egipcia extends Religion {

    private List<Dios> panteon;

    private static Egipcia self;

    private Egipcia(){}

    public static Egipcia getInstance(){
        if(self == null)
            self = new Egipcia();
        return self;
    }

    @Override
    public boolean juzgar(Muerto muerto) {
        return panteon.stream().allMatch(dios -> dios.juzgar(muerto));
    }
}
