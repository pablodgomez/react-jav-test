package com.despegar.altovuelo.report;

import java.time.LocalDate;
import java.time.Period;

/**
 * 
 * @author draffaeli
 *
 */
public class AgeCalculator {

	public int calculate(LocalDate birthdate) {
		return Period.between(birthdate, LocalDate.now()).getYears();
	}
	
}
