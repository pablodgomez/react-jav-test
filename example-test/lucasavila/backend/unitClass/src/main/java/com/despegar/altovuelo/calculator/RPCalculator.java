package com.despegar.altovuelo.calculator;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Function;


public class RPCalculator {

    static Map<String, Function<String, Function<String, BigDecimal>>> fops = new HashMap<>();

    static {
        Function<String, Function<String, BigDecimal>> adds = a -> b -> new BigDecimal(a).add(new BigDecimal(b));
        Function<String, Function<String, BigDecimal>> subs = a -> b -> new BigDecimal(a).subtract(new BigDecimal(b));
        Function<String, Function<String, BigDecimal>> mult = a -> b -> new BigDecimal(a).multiply(new BigDecimal(b));
        Function<String, Function<String, BigDecimal>> div = a -> b -> {
            final BigDecimal den = new BigDecimal(a);
            return new BigDecimal(b).divide(den);
        };

        fops.put("+", adds);
        fops.put("-", subs);
        fops.put("*", mult);
        fops.put("/", div);
    }

    private static BigDecimal divide(BigDecimal bigDecimal, BigDecimal den) {
        return bigDecimal.divide(den);
    }


    private com.despegar.altovuelo.util.Stack<String> elements;

    public RPCalculator(){
        this.elements = new com.despegar.altovuelo.util.Stack<String>();


    }

    public RPCalculator parse(String expr) {
        List<String> tokens = Arrays.asList(expr.split(" "));

        for (String token: tokens) {
            this.elements.push(token);
        }
        return this;
    }

    public BigDecimal top() {
        return new BigDecimal(this.elements.peek());
    }
    public RPCalculator resolve() {

        if (this.elements.size() == 0) {
            throw new EmptyStackException();
        }

        String top = this.elements.pop();

        if (fops.containsKey(top)){

            String arg1 = this.elements.pop();
            if(fops.containsKey(this.elements.peek())){
                this.resolve();
            }
            String arg2 = this.elements.pop();
            Function<String, Function<String, BigDecimal>> f = fops.get(top);

            BigDecimal e = f.apply(arg1).apply(arg2);
            this.elements.push(e.toEngineeringString());
        }
        else {
            this.elements = new com.despegar.altovuelo.util.Stack<String>();
            throw new IllegalStateException("Can't handle token" + top );
        }

        return this;
    }

    private static boolean isNumeric(String str)
    {
        return str.matches("-?\\d+(\\.\\d+)?");
    }
}
