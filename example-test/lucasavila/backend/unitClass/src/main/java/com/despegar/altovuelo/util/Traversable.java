package com.despegar.altovuelo.util;

public interface Traversable<T> {

    Traversable<T> drop(int n);

    Traversable<T> take(int n);
}
