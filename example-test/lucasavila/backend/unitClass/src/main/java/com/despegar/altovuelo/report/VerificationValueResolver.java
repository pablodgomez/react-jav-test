package com.despegar.altovuelo.report;

import java.time.LocalDate;

/**
 * 
 * @author draffaeli
 *
 */
public class VerificationValueResolver {

	private BinaryGap binaryGap;

	public int resolve(Customer customer) {
		LocalDate birthDate = customer.getBirthDate();
		int value = birthDate.getDayOfMonth() + birthDate.getMonthValue() + birthDate.getYear();
		
		return binaryGap.perform(value)*value;
	}
}
