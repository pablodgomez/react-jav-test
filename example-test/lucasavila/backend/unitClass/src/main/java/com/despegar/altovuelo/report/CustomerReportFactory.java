package com.despegar.altovuelo.report;

import java.util.List;

/**
 * 
 * @author draffaeli
 *
 */
public class CustomerReportFactory {

	private AgeCalculator ageCalculator;
	private AgeRangeCalculator ageRangeCalculator;
	private CustomerTypeResolver customerTypeResolver;
	private ChineseZodiacResolver chineseZodiacResolver;
	private VerificationValueResolver verificationValueResolver;
	
	public CustomerReport create(List<Customer> customers) {
		
		CustomerReport report = new CustomerReport();
		for (Customer customer : customers) {
			CustomerReportLine line = new CustomerReportLine();
			
			line.setName(customer.getName().toUpperCase());
			line.setSurname(customer.getLastName().toUpperCase());
			int age = ageCalculator.calculate(customer.getBirthDate());
			line.setAge(age);
			line.setAgeRange(ageRangeCalculator.calculate(age));
			line.setCustomerType(customerTypeResolver.resolve(customer));
			line.setChineseZodiacSign(this.chineseZodiacResolver.resolve(customer.getBirthDate().getYear()));
			line.setVerificationValue(this.verificationValueResolver.resolve(customer));
			
			report.addLine(line);
		}
		
		return report;
	}
	
	public void setAgeCalculator(AgeCalculator ageCalculator) {
		this.ageCalculator = ageCalculator;
	}
	
	public void setAgeRangeCalculator(AgeRangeCalculator ageRangeCalculator) {
		this.ageRangeCalculator = ageRangeCalculator;
	}
	
	public void setCustomerTypeResolver(
			CustomerTypeResolver customerTypeResolver) {
		this.customerTypeResolver = customerTypeResolver;
	}

	public void setChineseZodiacResolver(ChineseZodiacResolver chineseZodiacResolver) {
		this.chineseZodiacResolver = chineseZodiacResolver;
	}
	
	public void setVerificationValueResolver(VerificationValueResolver verificationValueResolver) {
		this.verificationValueResolver = verificationValueResolver;
	}
	
	
}
