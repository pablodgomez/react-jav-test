package com.despegar.altovuelo.util;

import java.math.BigDecimal;
import java.util.*;

public class Stack<T> implements Traversable{

    private List<T> elements;

    public Stack() {
        elements = new LinkedList<>();
    }

    public Stack(List list) {
        elements = list;
    }

    public int size() {
        return elements.size();
    }

    public Stack<T> push(T e) {
        this.elements.add(0, e);
        return this;
    }

    public T pop() {
        if (this.elements.isEmpty()){
            throw new NoSuchElementException("Can't pop an empty stack");
        }

        return this.elements.remove(0);
    }

    public T peek() {
        if (this.elements.isEmpty()){
            throw new NoSuchElementException("Can't peek an empty stack");
        }

        return this.elements.get(0);
    }

    @Override
    public Traversable take(int n) {
        if(n < 1 || n > this.size()){
            throw new IllegalArgumentException("The input is wrong");
        }

        Stack<T> stack_aux = new Stack(this.elements.subList(this.size()-n, this.size()));
        this.elements.subList(this.size()-n, this.size()).clear();
        return stack_aux;
    }

    @Override
    public Traversable drop(int n) {
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stack<?> stack = (Stack<?>) o;
        return Objects.equals(elements, stack.elements);
    }

    @Override
    public int hashCode() {

        return Objects.hash(elements);
    }
}
