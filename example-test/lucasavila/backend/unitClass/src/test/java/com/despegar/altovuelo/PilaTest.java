package com.despegar.altovuelo;

import org.junit.Test;
import static org.junit.Assert.*;

public class PilaTest {
    @Test
    public void testPush() {
        Pila<String> pila = new Pila<String>();
        assertEquals(0, pila.size());
        pila.push("A");
        assertEquals(1, pila.size());
        pila.push("B");
        assertEquals(2, pila.size());
        pila.push("C");
        assertEquals(3, pila.size());
        pila.push("D");
        assertEquals(4, pila.size());
    }
    @Test
    public void testPop() {
        Pila<String> pila = new Pila<String>();
        pila.push("A");
        pila.push("B");
        pila.push("C");
        pila.push("D");
        assertEquals(4, pila.size());
        String element = pila.pop();
        assertEquals("D", element);
        assertEquals(3, pila.size());
        element = pila.pop();
        assertEquals("C", element);
        assertEquals(2, pila.size());
        element = pila.pop();
        assertEquals("B", element);
        assertEquals(1, pila.size());
        element = pila.pop();
        assertEquals("A", element);
        assertEquals(0, pila.size());
    }

    @Test(expected = IllegalStateException.class)
    public void testPopVacio(){
        Pila<String> pila = new Pila<String>();
        pila.pop();
    }
}