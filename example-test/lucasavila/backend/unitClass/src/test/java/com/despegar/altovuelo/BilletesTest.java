package com.despegar.altovuelo;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

/**
 * Created by srosenbolt on 28/02/16.
 */
public class BilletesTest {
    private Billetes billetes = new Billetes();

    @Test
    public void testCalcularVuelto90() throws Exception {
        Map<Integer, Integer> vuelto = billetes.calcularVuelto(90);
        assertEquals(1, vuelto.size());
        assertEquals(Integer.valueOf(1), vuelto.get(10));
    }

    @Test
    public void testCalcularVuelto75() throws Exception {
        Map<Integer, Integer> vuelto = billetes.calcularVuelto(75);
        assertEquals(2, vuelto.size());
        assertEquals(Integer.valueOf(1), vuelto.get(20));
        assertEquals(Integer.valueOf(1), vuelto.get(5));
    }

    @Test
    public void testCalcularVuelto100() throws Exception {
        Map<Integer, Integer> vuelto = billetes.calcularVuelto(100);
        assertEquals(0, vuelto.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalcularVuelto0() throws Exception {
        Map<Integer, Integer> vuelto = billetes.calcularVuelto(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCalcularVuelto101() throws Exception {
        Map<Integer, Integer> vuelto = billetes.calcularVuelto(101);
    }

}