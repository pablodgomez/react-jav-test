package com.despegar.altovuelo.report;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author draffaeli
 *
 */
public class AgeRangeCalculatorTest {

	private AgeRangeCalculator ageRangeCalculator = new AgeRangeCalculator();
	
	@Test
	public void calculate_age10_returnNinio() {
		assertEquals("Niño", this.ageRangeCalculator.calculate(10));
	}
	
	@Test
	public void calculate_age18_returnAdolescente() {
		assertEquals("Adolescente", this.ageRangeCalculator.calculate(18));
	}
	
	@Test
	public void calculate_age40_returnJoven() {
		assertEquals("Joven", this.ageRangeCalculator.calculate(40));
	}
	
	@Test
	public void calculate_age63_returnAdulto() {
		assertEquals("Adulto", this.ageRangeCalculator.calculate(63));
	}
	
	@Test
	public void calculate_age90_returnViejo() {
		assertEquals("Viejo", this.ageRangeCalculator.calculate(90));
	}

}
