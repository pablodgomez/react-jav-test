package com.despegar.altovuelo.report;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author draffaeli
 *
 */
public class BinaryGapTest {

	private BinaryGap binaryGap = new BinaryGap();
	
	@Test
	public void perform_529_return4() {
		assertEquals(4, this.binaryGap.perform(529));
		
	}
	
	@Test
	public void perform_1041_return5() {
		assertEquals(5, this.binaryGap.perform(1041));
	}

}
