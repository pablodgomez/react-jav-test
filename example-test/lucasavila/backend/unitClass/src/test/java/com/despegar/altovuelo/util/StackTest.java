package com.despegar.altovuelo.util;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class StackTest {

    Stack<Integer> stackUT;

    @Before
    public void setUp() {
        stackUT = new Stack<>();
        stackUT.push(5);
        stackUT.push(10);
        stackUT.push(30);
    }
    @Test
    public void stackShouldBeLIFO() {
        stackUT.push(33);
        assertThat(stackUT.pop(), is(33));
    }

    @Test
    public void stackShouldTakeNElements(){
        stackUT.take(1);
        assertThat(stackUT.size(), is(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void stackShouldBeIllegalArgumentException(){
        stackUT.take(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void stackShouldThrowException(){
        stackUT.take(10);
    }
}
