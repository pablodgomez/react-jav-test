package com.despegar.altovuelo;

import org.junit.Test;

import static org.junit.Assert.*;

public class StringBalancerTest {
    private StringBalancer balancer = new StringBalancer();

    @Test
    public void testBalanced(){
        assertTrue(balancer.isStringBalanced("[lalala]"));
        assertTrue(balancer.isStringBalanced("[l{al}ala]"));
        assertTrue(balancer.isStringBalanced("[{{l{al}al}a}]"));
        assertTrue(balancer.isStringBalanced("[{[{}]}]"));
        assertFalse(balancer.isStringBalanced("{"));
        assertFalse(balancer.isStringBalanced("{]"));
        assertFalse(balancer.isStringBalanced("]"));
        assertFalse(balancer.isStringBalanced("}"));
        assertFalse(balancer.isStringBalanced("{{]"));
        assertFalse(balancer.isStringBalanced("}{"));
    }

}