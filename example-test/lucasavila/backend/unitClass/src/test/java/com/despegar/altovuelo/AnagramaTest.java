package com.despegar.altovuelo;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;


public class AnagramaTest {
    private Anagrama anagrama = new Anagrama();

    @Test
    public void testIsAnagramaTrue() throws Exception {
        assertTrue(anagrama.isAnagrama("amor", "roma"));
        assertTrue(anagrama.isAnagrama("roma", "amor"));
    }

        @Test
    public void testIsAnagramaFalse() throws Exception {
        assertFalse(anagrama.isAnagrama("perro", "gato"));
        assertFalse(anagrama.isAnagrama("gato", "perro"));
    }

    @Test
    public void testIsAnagramaVacio() throws Exception {
        assertFalse(anagrama.isAnagrama("", "hola"));
        assertFalse(anagrama.isAnagrama("hola", ""));
    }

    @Test
    public void testMayusculas() throws Exception {
        assertTrue(anagrama.isAnagrama("AlOH", "hola"));
        assertTrue(anagrama.isAnagrama("Curso", "Surco"));
    }

    @Test
    public void testAnagramaPez() throws Exception {
        Set<String> resultado = anagrama.anagrama("pez");
        assertEquals(6, resultado.size());
        assertTrue(resultado.contains("pez"));
        assertTrue(resultado.contains("pze"));
        assertTrue(resultado.contains("zpe"));
        assertTrue(resultado.contains("zep"));
        assertTrue(resultado.contains("epz"));
        assertTrue(resultado.contains("ezp"));
    }

    @Test
    public void testAnagramaVacio() throws Exception {
        Set<String> resultado = anagrama.anagrama("");
        assertEquals(0, resultado.size());
    }
}