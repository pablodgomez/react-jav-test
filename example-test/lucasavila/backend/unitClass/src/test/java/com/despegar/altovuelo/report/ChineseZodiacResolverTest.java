package com.despegar.altovuelo.report;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author draffaeli
 *
 */
public class ChineseZodiacResolverTest {

	private ChineseZodiacResolver resolver = new ChineseZodiacResolver();
	
	@Test
	public void resolve_1982_retunrDog() {
		assertEquals("Dog", resolver.resolve(1982));
	}

}
