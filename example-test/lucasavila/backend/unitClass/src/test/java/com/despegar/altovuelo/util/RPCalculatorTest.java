package com.despegar.altovuelo.util;

import com.despegar.altovuelo.calculator.RPCalculator;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.EmptyStackException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class RPCalculatorTest {

    RPCalculator calculator;

    @Before
    public void setUp() {
        calculator = new RPCalculator();
    }

    @Test
    public void shouldParseBinaryExpressions() {
        String expr = "3 2 +";
        RPCalculator expression = calculator.parse(expr);
        assertThat(expression.resolve().top(), is(BigDecimal.valueOf(5)));
    }

    @Test
    public void shouldParseComplexExpressions(){
        String expr = "3 2 + 4 *";
        RPCalculator expression = calculator.parse(expr);
        assertThat(expression.resolve().top(), is(BigDecimal.valueOf(20)));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowWhenTokenIsIllegal(){
        String expr = "3 2";
        RPCalculator expression = calculator.parse(expr);
        expression.resolve().top();
    }

    @Test(expected = EmptyStackException.class)
    public void shouldThrowWhenEmpty() {
        calculator.resolve();
    }

    @Test(expected = ArithmeticException.class)
    public void div0(){
        calculator.parse("4 0 /").resolve();
    }
}
