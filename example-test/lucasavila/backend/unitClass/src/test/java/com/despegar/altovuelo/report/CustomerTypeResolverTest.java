package com.despegar.altovuelo.report;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author draffaeli
 *
 */
public class CustomerTypeResolverTest {

	private CustomerTypeResolver customerTypeResolver = new CustomerTypeResolver();
	
	@Test
	public void resolve_CosmeFulanito_returnCann() {
		// arrange
		Customer customer = new Customer();
		customer.setName("Cosme");
		customer.setLastName("Fulanito");
		
		// act
		String actualCustomerType = this.customerTypeResolver.resolve(customer);
		
		// assert
		assertEquals("Cann", actualCustomerType);
	}
	
	@Test
	public void resolve_MariaLeal_returnWind() {
		// arrange
		Customer customer = new Customer();
		customer.setName("María");
		customer.setLastName("Leal");
		
		// act
		String actualCustomerType = this.customerTypeResolver.resolve(customer);
		
		// assert
		assertEquals("Wind", actualCustomerType);
	}
	
	@Test
	public void resolve_ClaudioCaniggia_returnWindSon() {
		// arrange
		Customer customer = new Customer();
		customer.setName("Claudio");
		customer.setLastName("Caniggia");
		
		// act
		String actualCustomerType = this.customerTypeResolver.resolve(customer);
		
		// assert
		assertEquals("WindSon", actualCustomerType);
	}

}
