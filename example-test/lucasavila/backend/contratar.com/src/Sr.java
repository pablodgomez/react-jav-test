public class Sr implements Categoria {

    private static Sr self;
    private String puesto = "Sr";

    private Sr(){}

    public static Sr getInstance(){
        if(self == null)
            self = new Sr();
        return self;
    }

    public String getPuesto(){
        return puesto;
    }

    public float calcularSueldo(Empresa empresa, Usuario usuario) {
        return usuario.getSueldoActual() * 2;
    }
}
