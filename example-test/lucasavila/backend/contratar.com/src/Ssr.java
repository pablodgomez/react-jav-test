public class Ssr implements Categoria {

    private static Ssr self;
    private String puesto = "Ssr";

    private Ssr(){}

    public static Ssr getInstance(){
        if(self == null)
            self = new Ssr();
        return self;
    }

    public String getPuesto(){
        return puesto;
    }

    public float calcularSueldo(Empresa empresa, Usuario usuario) {
        return usuario.getExperiencia() * 5000;
    }
}
