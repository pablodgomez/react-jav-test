public class Gerente implements Categoria {

    private static Gerente self;
    private String puesto = "Gerente";

    private Gerente(){}

    public static Gerente getInstance(){
        if(self == null)
            self = new Gerente();
        return self;
    }

    public String getPuesto(){
        return puesto;
    }

    public float calcularSueldo(Empresa empresa, Usuario usuario) {
        return (empresa.getCantEmpleados() / 10) * usuario.getSueldoActual();
    }
}


//Cantidad de empleados de la empresa / 10 * sueldo actual