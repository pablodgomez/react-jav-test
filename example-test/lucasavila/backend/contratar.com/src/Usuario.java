import java.util.ArrayList;
import java.util.List;

public class Usuario {

    private float sueldoActual;
    private Integer experiencia;
    private Categoria categoria;
    private List<Registro> historial;

    public Usuario(Categoria categoria, float sueldoActual, Integer experiencia){
        this.categoria = categoria;
        this.sueldoActual = sueldoActual;
        this.experiencia = experiencia;
        this.historial = new ArrayList<Registro>();
    }

    public float calcularSueldo(Empresa empresa){
        return categoria.calcularSueldo(empresa, this);
    }

    public void agregarExperienciaLaborar(Empresa empresa, Integer ingreso, Integer egreso){
        historial.add(new Registro(empresa, ingreso, egreso, this.categoria));
    }

    public void mostrarExperiencias(){
        for(Registro trabajo : historial){
            trabajo.show();
        }
    }

    public float getSueldoActual() {
        return this.sueldoActual;
    }

    public Integer getExperiencia() {
        return this.experiencia;
    }

    public Categoria getCategoria() {
        return this.categoria;
    }
}
