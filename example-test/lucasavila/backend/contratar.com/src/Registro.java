import java.util.Date;

public class Registro {

    Empresa empresa;
    Integer ingreso;
    Integer egreso;
    Categoria puesto;

    public Registro(Empresa empresa, Integer ingreso, Integer egreso, Categoria puesto){
        this.empresa = empresa;
        this.ingreso = ingreso;
        this.egreso = egreso;
        this.puesto = puesto;
    }

    public void show(){
        System.out.println("Empresa: " + empresa.getNombre());
        System.out.println("Ingreso: " + ingreso);
        System.out.println("Egreso: " + egreso);
        System.out.println("Puesto: " + puesto.getPuesto());
        System.out.println("--------------------------------");
    }
}
