public interface Categoria {

    public float calcularSueldo(Empresa empresa, Usuario usuario);

    public String getPuesto();
}
