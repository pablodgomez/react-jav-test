public class Junior implements Categoria {

    private static Junior self;
    private String puesto = "Junior";

    private Junior(){}

    public static Junior getInstance(){
        if(self == null)
            self = new Junior();
        return self;
    }

    public String getPuesto(){
        return puesto;
    }

    public float calcularSueldo(Empresa empresa, Usuario usuario) {
        return Math.max(usuario.getSueldoActual(), empresa.getSueldoBase());
    }
}
