public class Empresa {

    private String nombre;
    private float sueldoBase;
    private Integer cantEmpleados;

    public Empresa(String nombre, float sueldo_base, Integer cantEmpleados){
        this.nombre = nombre;
        this.sueldoBase = sueldo_base;
        this.cantEmpleados = cantEmpleados;
    }

    public float getSueldoBase() {
        return this.sueldoBase;
    }

    public Integer getCantEmpleados() {
        return this.cantEmpleados;
    }

    public String getNombre() {
        return this.nombre;
    }
}
