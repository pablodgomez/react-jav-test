public class Trainee implements Categoria {

    private static Trainee self;
    private String puesto = "Trainee";

    private Trainee(){}

    public static Trainee getInstance(){
        if(self == null)
            self = new Trainee();
        return self;
    }

    public String getPuesto(){
        return puesto;
    }

    public float calcularSueldo(Empresa empresa, Usuario usuario) {
        return empresa.getSueldoBase();
    }
}
