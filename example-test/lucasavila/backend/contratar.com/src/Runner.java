import java.util.Date;

public class Runner {

    public static void main(String args[]){

        Empresa empresa = new Empresa("Despegar", 2000f, 10);
        Usuario usuario = new Usuario(Gerente.getInstance(), 3000, 3);

        System.out.println("Su posible sueldo es: "
                + usuario.calcularSueldo(empresa));

        /////////////////////////////////////////

        usuario.agregarExperienciaLaborar(empresa, 2017, 2018);
        usuario.mostrarExperiencias();
    }
}
