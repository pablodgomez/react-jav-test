package com.despegar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        int a = 4;
        int b = 3;
        int c = a + b;
        System.out.println(new Date().toString() + "- " + c);
        LOGGER.info("El valor de c es " + c );

        LOGGER.error("Soy un error");
        LOGGER.debug("Soy un debug");
        LOGGER.info("Soy un info");
        LOGGER.warn("Soy un warning");

        hola();
    }

    public static void hola(){
        try {
            throw new RuntimeException("hola");
        } catch (RuntimeException e){
            //LOGGER.info("atrap una excepcion", e);
        }
    }
}
