public class Jirafa implements AnimalCorredor {

    private Integer peso;

    public Jirafa(Integer peso) {
        this.peso = peso;
    }

    public void correr(Integer kilometros) {
        this.reducirPeso(kilometros);
        System.out.println("Estoy corriendo " + kilometros + "km y peso " + this.getPeso() + "kg :)");
    }

    private void reducirPeso(Integer kilometros){
        if(this.peso - kilometros/2 <= 0)
            this.setPeso(0);
        else
            this.setPeso(this.peso - kilometros/2);
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Integer getPeso() {
        return peso;
    }
}
