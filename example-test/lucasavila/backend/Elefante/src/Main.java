public class Main {

    public static void main(String arg[]){

        Elefante elefante = new Elefante(30);
        Jirafa jirafa = new Jirafa(50);
        Doctor doctor = new Doctor();

        doctor.hacerRutina(elefante, 10);
        doctor.hacerRutina(jirafa, 10);
    }
}
