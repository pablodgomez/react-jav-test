public class Elefante implements  AnimalCorredor {

    private Integer peso;

    public Elefante(Integer peso) {
        this.peso = peso;
    }

    public void barritar() {}

    public void correr(Integer kilometros) {
        this.reducirPeso(kilometros);
        System.out.println("Estoy corriendo " + kilometros + "km y peso " + this.getPeso() + "kg :)");
    }

    private void reducirPeso(Integer kilometros){
        if(this.peso - kilometros/2 <= 0)
            this.setPeso(0);
        else
            this.setPeso(this.peso - kilometros/2);
    }

    public Integer getPeso() {
        return this.peso;
    }

    public void setPeso(Integer kg) {
        this.peso = kg;
    }
}
