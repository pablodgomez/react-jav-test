package com.despegar.jav.examples.di.agenda;

import java.time.LocalDateTime;

public class Task {
    private String description;
    private LocalDateTime due;
    private TaskStatus status;

    public Task(String description, LocalDateTime due, TaskStatus status) {
        this.description = description;
        this.due = due;
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDue() {
        return due;
    }

    public TaskStatus getStatus() {
        return status;
    }
}
