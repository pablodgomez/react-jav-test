package com.despegar.jav.examples.di.agenda;

import com.despegar.jav.examples.di.agenda.repository.FileRepository;

import java.util.ArrayList;
import java.util.List;

class Agenda {

    private FileRepository fileRepository;

    public Agenda(FileRepository fileRepository) {
        this.fileRepository = fileRepository;
    }

    public List<Task> filter(TaskFilter filter) {
        List<Task> result = new ArrayList<>();
        for (Task task : fileRepository.list()) {
            if (filter.pass(task)) {
                result.add(task);
            }
        }
        return result;
    }

    public void save(Task task) {
        if (task.getStatus() == null)
            throw new IllegalArgumentException("task.status cant be null");

        if (task.getDue() == null)
            throw new IllegalArgumentException("task.due cant be null");

        if (task.getDescription() == null)
            throw new IllegalArgumentException("task.description cant be null");

        fileRepository.save(task);
    }
}
