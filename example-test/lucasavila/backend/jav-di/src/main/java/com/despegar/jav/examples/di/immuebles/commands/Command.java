package com.despegar.jav.examples.di.immuebles.commands;

import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;

public interface Command extends Function<Map<String, String>, String> {

}
