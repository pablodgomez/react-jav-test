package com.despegar.jav.examples.di.immuebles.domain.cobros;

import com.despegar.jav.examples.di.immuebles.domain.cobros.model.OrdenCobro;
import com.despegar.jav.examples.di.immuebles.domain.database.Dao;

import java.util.List;

public class Cobrador {
    private final Dao<OrdenCobro> ordenCobroDao;

    public Cobrador(Dao<OrdenCobro> ordenCobroDao) {
        this.ordenCobroDao = ordenCobroDao;
    }

    public void cobrar(OrdenCobro orden) {
        if (orden == null)
            throw new IllegalArgumentException("OrdenCobro cant be null");

        ordenCobroDao.save(orden);
    }

    public List<OrdenCobro> verCobros() {
        return ordenCobroDao.list();
    }
}
