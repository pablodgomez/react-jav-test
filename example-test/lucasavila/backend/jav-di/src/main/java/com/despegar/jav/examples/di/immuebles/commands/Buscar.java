package com.despegar.jav.examples.di.immuebles.commands;

import com.despegar.jav.examples.di.immuebles.domain.publicacion.Publicador;
import com.despegar.jav.examples.di.immuebles.domain.publicacion.model.Publicacion;
import com.despegar.jav.examples.di.immuebles.parser.PredicateBuilder;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Buscar implements Command {

    private final Publicador publicador;
    private final PredicateBuilder predicateBuilder;

    public Buscar(Publicador publicador, PredicateBuilder predicateBuilder) {
        this.publicador = publicador;
        this.predicateBuilder = predicateBuilder;
    }

    public String apply(Map<String, String> request) {
        List<Predicate<Publicacion>> filtros = request
                .entrySet()
                .stream()
                .map(entry -> predicateBuilder.makeFilter(entry.getKey(), entry.getValue()))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        List<Publicacion> publicaciones = publicador.listar(filtros);
        return publicaciones.toString();
    }

}
