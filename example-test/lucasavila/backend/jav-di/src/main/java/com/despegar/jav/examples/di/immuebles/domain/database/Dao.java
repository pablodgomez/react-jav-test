package com.despegar.jav.examples.di.immuebles.domain.database;

import java.util.*;

public class Dao<T> {
    private final Map<String, T> items = new HashMap<>();

    public void update(String id, T item) {
        synchronized (items) {
            validateNotBlank(id);
            validateEntityExists(id);

            if (item == null)
                throw new IllegalArgumentException("id " + id + " item cant be null.");

            items.put(id, item);
        }
    }

    public String save(T item) {
        synchronized (items) {
            if (item == null)
                throw new IllegalArgumentException("item cant be null.");

            String id = UUID.randomUUID().toString();
            items.put(id, item);
            return id;
        }
    }


    public T get(String id) {
        validateNotBlank(id);
        return items.get(id);
    }

    public void remove(String id) {
        synchronized (items) {
            validateNotBlank(id);
            validateEntityExists(id);
            items.remove(id);
        }
    }

    public List<T> list() {
        return new ArrayList<>(items.values());
    }

    private void validateEntityExists(String id) {
        if (items.get(id) == null)
            throw new IllegalArgumentException("id " + id + " doesn't exists.");
    }

    private void validateNotBlank(String id) {
        if (id == null || id.trim().isEmpty())
            throw new IllegalArgumentException("id can't be blank");
    }
}
