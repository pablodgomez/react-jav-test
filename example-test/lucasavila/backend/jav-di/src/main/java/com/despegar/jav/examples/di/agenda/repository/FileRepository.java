package com.despegar.jav.examples.di.agenda.repository;

import com.despegar.jav.examples.di.agenda.Task;
import com.despegar.jav.examples.di.agenda.TaskStatus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FileRepository {
    private Path dataFilePath;

    public FileRepository(Path dataFilePath) {
        this.dataFilePath = dataFilePath;
    }

    public List<Task> list() {
        try {
            List<String> lines = Files.readAllLines(dataFilePath);
            List<Task> result = new ArrayList<>();
            for (String line : lines) {
                String[] fields = line.split(",");
                if (fields.length != 3) {
                    throw new CorruptLineException(line);
                }
                Task task = new Task(
                        fields[0] // description
                        , LocalDateTime.parse(fields[1]) // due
                        , TaskStatus.valueOf(fields[2])  // status
                );
                result.add(task);
            }
            return result;
        } catch (IOException e) {
            throw new ReadException(dataFilePath, e);
        }
    }

    public void save(Task task) {
        String line = task.getDescription() +
                "," + task.getDue().toString() +
                "," + task.getStatus().toString() +
                "\n";
        try {
            Files.write(dataFilePath
                    , line.getBytes()
                    , StandardOpenOption.APPEND);
        } catch (Exception e) {
            throw new WriteException(dataFilePath, line, e);
        }
    }


    public static class WriteException extends RuntimeException {
        WriteException(Path path, String line, Exception cause) {
            super("Couldn't write: '" + line
                            + "' into: " + path.toFile().toString()
                    , cause);
        }
    }

    public static class ReadException extends RuntimeException {
        ReadException(Path path, Exception cause) {
            super("Couldn't read " + path.toFile().toString()
                    , cause);
        }
    }

    public static class CorruptLineException extends RuntimeException {
        CorruptLineException(String line) {
            super("Corrupt line" + line);
        }
    }

    public static class ResourceNotFound extends RuntimeException {
        ResourceNotFound(String path, Exception e) {
            super("Cant open path:" + path, e);
        }
    }
}
