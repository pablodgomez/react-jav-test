package com.despegar.jav.examples.di.agenda.filters;

import com.despegar.jav.examples.di.agenda.Task;
import com.despegar.jav.examples.di.agenda.TaskFilter;
import com.despegar.jav.examples.di.agenda.TaskStatus;

public class TaskStatusFilter implements TaskFilter {
    private TaskStatus status;

    public TaskStatusFilter(TaskStatus status) {
        this.status = status;
    }

    @Override
    public boolean pass(Task task) {
        return status == task.getStatus();
    }
}
