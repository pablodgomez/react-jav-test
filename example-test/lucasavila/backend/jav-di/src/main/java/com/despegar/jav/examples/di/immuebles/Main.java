package com.despegar.jav.examples.di.immuebles;

import com.despegar.jav.examples.di.immuebles.commands.*;
import com.despegar.jav.examples.di.immuebles.domain.cobros.Cobrador;
import com.despegar.jav.examples.di.immuebles.domain.cobros.model.OrdenCobro;
import com.despegar.jav.examples.di.immuebles.domain.database.Dao;
import com.despegar.jav.examples.di.immuebles.domain.publicacion.Publicador;
import com.despegar.jav.examples.di.immuebles.domain.publicacion.model.Publicacion;
import com.despegar.jav.examples.di.immuebles.parser.PredicateBuilder;
import com.despegar.jav.examples.di.immuebles.parser.RequestParser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        ImmueblesApp immueblesApp = buildApp();
        System.out.println(immueblesApp.process(args[1]));
    }

    public static ImmueblesApp buildApp() {
        ApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("application-context-inmuebles.xml");

        return applicationContext.getBean(ImmueblesApp.class);
    }


}
