package com.despegar.jav.examples.di.immuebles.domain.cobros.model;

import com.despegar.jav.examples.di.immuebles.domain.publicacion.model.Publicacion;

import java.math.BigDecimal;

public class OrdenCobro {
    private Publicacion publicacion;
    private BigDecimal monto;

    public OrdenCobro(Publicacion publicacion
            , BigDecimal monto) {
        this.publicacion = publicacion;
        this.monto = monto;
    }

    public Publicacion getPublicacion() {
        return publicacion;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    @Override
    public String toString() {
        return "OrdenCobro{" +
                "publicacion=" + publicacion +
                ", monto=" + monto +
                '}';
    }
}
