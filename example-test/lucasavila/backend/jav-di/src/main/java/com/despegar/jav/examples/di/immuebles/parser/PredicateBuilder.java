package com.despegar.jav.examples.di.immuebles.parser;

import com.despegar.jav.examples.di.immuebles.domain.publicacion.model.Publicacion;

import java.math.BigDecimal;
import java.util.function.Predicate;

public class PredicateBuilder {
    private final RequestParser requestParser;

    public PredicateBuilder(RequestParser requestParser) {
        this.requestParser = requestParser;
    }

    public Predicate<Publicacion> makeFilter(String key, String value) {
        switch (key) {
            case "amb":
                return p -> p.getAmbientes() == Integer.parseInt(value);
            case "m2":
                return p -> p.getMetrosCuadrados() == Integer.parseInt(value);
            case "direccion":
                return p -> p.getDireccion().contains(value);
            case "desde":
                BigDecimal desde = requestParser.requiredBigDecimal(key, value);
                return p -> p.getMonto().compareTo(desde) >= 0;
            case "hasta":
                BigDecimal hasta = requestParser.requiredBigDecimal(key, value);
                return p -> p.getMonto().compareTo(hasta) <= 0;
        }
        return null;
    }
}
