package com.despegar.jav.examples.di.immuebles.domain.publicacion.model;

import java.math.BigDecimal;

public class Publicacion {
    private int ambientes;
    private int metrosCuadrados;

    private String descripcion;

    private String direccion;

    private String vendedor;

    private BigDecimal monto;

    public Publicacion(int ambientes
            , BigDecimal monto, int metrosCuadrados
            , String descripcion
            , String direccion
            , String vendedor) {
        this.ambientes = ambientes;
        this.metrosCuadrados = metrosCuadrados;
        this.descripcion = descripcion;
        this.direccion = direccion;
        this.vendedor = vendedor;
        this.monto = monto;
    }

    public int getAmbientes() {
        return ambientes;
    }

    public int getMetrosCuadrados() {
        return metrosCuadrados;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getVendedor() {
        return vendedor;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    @Override
    public String toString() {
        return "Publicacion{" +
                "ambientes=" + ambientes +
                ", metrosCuadrados=" + metrosCuadrados +
                ", descripcion='" + descripcion + '\'' +
                ", direccion=" + direccion +
                ", vendedor=" + vendedor +
                ", monto=" + monto +
                '}';
    }
}
