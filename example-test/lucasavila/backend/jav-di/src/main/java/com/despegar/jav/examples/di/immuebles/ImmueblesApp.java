package com.despegar.jav.examples.di.immuebles;

import com.despegar.jav.examples.di.immuebles.commands.Command;
import com.despegar.jav.examples.di.immuebles.parser.RequestParser;

import java.util.Map;

public class ImmueblesApp {

    private final Map<String, Command> commands;
    private final RequestParser requestParser;
    private final Command defaultCommand;

    public ImmueblesApp(Map<String, Command> commands
            , RequestParser requestParser
            , Command defaultCommand) {
        this.commands = commands;
        this.requestParser = requestParser;
        this.defaultCommand = defaultCommand;
    }

    public String process(String line) {
        Map<String, String> request = requestParser.parse(line);
        return commands.getOrDefault(request.get("command"), defaultCommand)
                .apply(request);
    }

}
