package com.despegar.jav.examples.di.agenda.filters;

import com.despegar.jav.examples.di.agenda.Task;
import com.despegar.jav.examples.di.agenda.TaskFilter;

import java.time.LocalDateTime;

public class DueAfterFilter implements TaskFilter {
    private LocalDateTime from;

    public DueAfterFilter(LocalDateTime from) {
        this.from = from;
    }

    @Override
    public boolean pass(Task task) {
        return task.getDue().isAfter(from);
    }
}
