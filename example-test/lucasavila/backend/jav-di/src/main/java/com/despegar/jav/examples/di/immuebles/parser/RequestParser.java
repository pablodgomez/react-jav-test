package com.despegar.jav.examples.di.immuebles.parser;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class RequestParser {

    public static class RequiredArgumentException extends RuntimeException {
        public RequiredArgumentException(String name) {
            super(name + " required ");
        }
    }

    private final String keyValueSeparator = "=";
    private final String pairSeparator = "\\s+";

    public Map<String, String> parse(String stringRequest) {
        return Stream.of(stringRequest.split(pairSeparator))
                .map(pair -> pair.split(keyValueSeparator))
                .filter(pair -> pair.length == 2)
                .collect(toMap(pair -> pair[0]
                        , pair -> pair[1]));
    }


    public Integer requiredInt(Map<String, String> req, String key) {
        String val = req.get(key);
        if (val == null) {
            throw new RequiredArgumentException(key);
        }
        return parseInteger(key, val);
    }

    public String requiredString(Map<String, String> req, String key) {
        String val = req.get(key);
        if (val == null) {
            throw new RequiredArgumentException(key);
        }
        return val;
    }

    public BigDecimal requiredBigDecimal(Map<String, String> req, String key) {
        String val = req.get(key);
        if (val == null) {
            throw new RequiredArgumentException(key);
        }
        return requiredBigDecimal(key, val);
    }

    public Integer parseInteger(String key, String val) {
        try {
            return Integer.parseInt(val);
        } catch (Exception e) {
            throw new IllegalArgumentException(key + " must be an integer", e);
        }
    }

    public BigDecimal requiredBigDecimal(String key, String val) {
        try {
            return new BigDecimal(val);
        } catch (Exception e) {
            throw new IllegalArgumentException(key + " must be an bigdecimal", e);
        }
    }


}
