package com.despegar.jav.examples.di.immuebles.domain.publicacion;

import com.despegar.jav.examples.di.immuebles.domain.cobros.Cobrador;
import com.despegar.jav.examples.di.immuebles.domain.cobros.model.OrdenCobro;
import com.despegar.jav.examples.di.immuebles.domain.database.Dao;
import com.despegar.jav.examples.di.immuebles.domain.publicacion.model.Publicacion;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Publicador {

    private final Dao<Publicacion> dao;
    private final Cobrador cobrador;

    public Publicador(Dao<Publicacion> dao
            , Cobrador cobrador) {
        this.dao = dao;
        this.cobrador = cobrador;
    }

    public String publicar(Publicacion publicacion) {
        String id = dao.save(publicacion);
        try {
            OrdenCobro ordenCobro = new OrdenCobro(publicacion
                    , BigDecimal.valueOf(10));
            cobrador.cobrar(ordenCobro);
        } catch (Exception e) {
            dao.remove(id);
            throw e;
        }
        return id;
    }

    public List<Publicacion> listar(List<Predicate<Publicacion>> filtros) {
        return dao.list()
                .stream()
                .filter(publicacion ->
                        filtros.isEmpty()
                                || filtros.stream()
                                .anyMatch(filtro ->
                                        filtro.test(publicacion)))
                .collect(Collectors.toList());
    }
}
