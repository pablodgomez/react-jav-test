package com.despegar.jav.examples.di.agenda;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static org.junit.jupiter.api.Assertions.assertEquals;

class IntegrationTest {

    @BeforeEach
    void AgendaFile() throws Exception {
        Files.write(resourcePath()
                , "".getBytes()
                , StandardOpenOption.TRUNCATE_EXISTING);
    }

    @Test
    void add_givenFullTaskInfo_isAbleToRetrieve() throws Exception {
        // Esto es poco feo, por que?
        Main.main(new String[]{"", "add", "Usar spring", "2018-01-01T10:10", "TODO"});

        // Que tiene de bueno y que tiene de malo?
        assertEquals(new String(Files.readAllBytes(resourcePath()))
                , "Usar spring,2018-01-01T10:10,TODO\n");
    }

    private Path resourcePath() throws Exception {
        URI uri = Thread.currentThread()
                .getContextClassLoader()
                .getResource("agenda.db")
                .toURI();
        return Paths.get(uri);
    }
}