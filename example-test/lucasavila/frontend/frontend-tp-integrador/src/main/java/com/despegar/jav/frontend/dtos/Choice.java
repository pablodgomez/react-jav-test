package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

public class Choice {

    @Expose
    private List<BusquetsRoute> routes;

    public List<BusquetsRoute> getRoutes() {
        return routes;
    }

    public Date getDepartureDate() {
        return routes.get(0).getDepartureDate();
    }

    public Date getArrivalDate() {
        return routes.get(0).getArrivalDate();
    }

    public String getDepartureAirportCode() {
        return routes.get(0).getDepartureAirportCode();
    }

    public String getArrivalAirportCode() {
        return routes.get(0).getArrivalAirportCode();
    }
}
