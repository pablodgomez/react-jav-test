package com.despegar.jav.frontend.dtos;

public enum BaggageType {

    INCLUDED, WITHOUT_INFO, NOT_INCLUDED;
}
