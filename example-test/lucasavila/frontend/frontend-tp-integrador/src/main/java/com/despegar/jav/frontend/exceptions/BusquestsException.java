package com.despegar.jav.frontend.exceptions;

public class BusquestsException extends RuntimeException {
    public BusquestsException(String s) {
        super(s);
    }
}
