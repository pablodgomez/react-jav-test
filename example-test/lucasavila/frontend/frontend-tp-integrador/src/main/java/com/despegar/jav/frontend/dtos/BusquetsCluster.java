package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.EnumUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class BusquetsCluster {

    @Expose
    @SerializedName("routeChoices")
    private List<Choice> choices;
    @Expose
    private BusquetsPriceDetail priceDetail;
    @Expose
    private PaymentsInfo paymentsInfo;

    public Choice getOutboundChoice() {
        return choices.get(0);
    }

    public Choice getInboundChoice(){
         return choices.get(1);
    }

    public boolean hasAReturnChoice() {
        return choices.size() > 1;
    }

    public BusquetsPriceDetail getPriceDetail() {
        return priceDetail;
    }

    public String getPaymentsMessage() {
        return paymentsInfo.getPaymentsMessage();
    }

    public Date getDepartureDate(){
        return choices.get(0).getDepartureDate();
    }

    public Date getArrivalDate(){
        return choices.get(0).getArrivalDate();
    }

    public String getDepartureAirportCodeOutbound(){
        return choices.get(0).getDepartureAirportCode();
    }

    public String getArrivalAirportCodeOutbound(){
        return choices.get(0).getArrivalAirportCode();
    }

    public String getDepartureAirportCodeInbound(){
        return choices.get(1).getDepartureAirportCode();
    }

    public String getArrivalAirportCodeInbound(){
        return choices.get(1).getArrivalAirportCode();
    }
}
