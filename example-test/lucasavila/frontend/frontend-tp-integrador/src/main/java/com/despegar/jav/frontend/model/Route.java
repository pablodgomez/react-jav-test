package com.despegar.jav.frontend.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class Route {

    private int baggageQuantity;
    private Map<String, String> airlines;
    private String departureAirportCode;
    private String arrivalAirportCode;
    private String departureHour;
    private String arrivalHour;
    private StopsInformation stopsInfo;
    private String totalDuration;
    private long dayGap;
    private BaggageInfo baggageInfo;

    public Route(int baggageQuantity, Map<String, String> airlines, String departureAirportCode, String arrivalAirportCode, String departureHour, String arrivalHour, long dayGap, StopsInformation stopsInfo, String totalDuration, BaggageInfo baggageInfo) {
        this.baggageQuantity = baggageQuantity;
        this.airlines = airlines;
        this.departureAirportCode = departureAirportCode;
        this.arrivalAirportCode = arrivalAirportCode;
        this.departureHour = departureHour;
        this.arrivalHour = arrivalHour;
        this.stopsInfo = stopsInfo;
        this.totalDuration = totalDuration;
        this.dayGap = dayGap;
        this.baggageInfo = baggageInfo;
    }
}
