package com.despegar.jav.frontend.controllers;

import com.despegar.jav.frontend.model.SearchResult;
import com.despegar.jav.frontend.services.FlightService;
import com.despegar.jav.frontend.utils.Validator;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {

    @Autowired
    private FlightService flightService;
    @Autowired
    private Validator validator;

    @RequestMapping(value = "/flights", method = RequestMethod.GET, produces = "application/json")
    public String searchFlights(@RequestParam String adults,
                                       @RequestParam String from,
                                       @RequestParam String to,
                                       @RequestParam String departureDate,
                                       @RequestParam String returnDate){
        validator.validateInputs(Integer.parseInt(adults), from, to, departureDate, returnDate);
        SearchResult result = flightService.getSearchResult(Integer.parseInt(adults), from, to, departureDate, returnDate);
        return new Gson().toJson(result);
    }
}
