package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

public class PaymentsInfo {

    @Expose
    private String paymentsMessage;

    public String getPaymentsMessage() {
        return paymentsMessage;
    }
}
