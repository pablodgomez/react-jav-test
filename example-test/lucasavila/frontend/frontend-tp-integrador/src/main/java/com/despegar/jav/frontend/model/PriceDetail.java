package com.despegar.jav.frontend.model;

public class PriceDetail {

    private String amountWithDiscount;
    private String amountWithoutDiscount;
    private String primaryMessage;
    private String secondaryMessage;
    private String paymentsMessage;

    public PriceDetail(String amountWithDiscount, String amountWithoutDiscount, String primaryMessage, String secondaryMessage, String paymentsMessage) {
        this.amountWithDiscount = amountWithDiscount;
        this.amountWithoutDiscount = amountWithoutDiscount;
        this.primaryMessage = primaryMessage;
        this.secondaryMessage = secondaryMessage;
        this.paymentsMessage = paymentsMessage;
    }
}
