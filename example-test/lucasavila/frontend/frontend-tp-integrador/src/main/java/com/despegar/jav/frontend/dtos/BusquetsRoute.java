package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

public class BusquetsRoute {

    @Expose
    private List<Segment> segments;
    @Expose
    private BusquestsBaggageInfo baggageInfo;
    @Expose
    private List<String> airlines;
    @Expose
    private FlightInformation departure;
    @Expose
    private FlightInformation arrival;
    @Expose
    private int stopsCount;
    @Expose
    private String totalDuration;

    public int getBaggageQuantity(){
        return baggageInfo.getQuantity();
    }

    public List<String> getAirlines(){
        return airlines;
    }

    public String getDepartureAirportCode() {
        return departure.getAirportCode();
    }

    public String getArrivalAirportCode() {
        return arrival.getAirportCode();
    }

    public Date getDepartureDate() {
        return departure.getDate();
    }

    public Date getArrivalDate() {
        return arrival.getDate();
    }

    public int getStopsQuantity() {
        return stopsCount;
    }

    public String getTotalDuration() {
        return totalDuration;
    }

    public BusquestsBaggageInfo getBaggageInfo() {
        return baggageInfo;
    }
}
