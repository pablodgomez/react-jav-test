package com.despegar.jav.frontend.model;

import java.util.Date;
import java.util.List;

public class RoutePack {

    private List<Route> outboundRoutes;
    private List<Route> inboundRoutes;
    private String departureDate;
    private String arrivalDate;

    public RoutePack(String departureDate, String arrivalDate) {
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
    }

    public void setOutboundRoutes(List<Route> outboundRoutes) {
        this.outboundRoutes = outboundRoutes;
    }

    public void setInboundRoutes(List<Route> inboundRoutes) {
        this.inboundRoutes = inboundRoutes;
    }
}
