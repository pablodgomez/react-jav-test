package com.despegar.jav.frontend.utils;

import com.despegar.jav.frontend.exceptions.ParameterException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashMap;

@Component
public class Validator {

    private final static Logger LOGGER = LoggerFactory.getLogger(Validator.class);

    public void validateInputs(int adults, String from, String to, String departureDate, String returnDate) {
        checkAdults(adults);
        checkPlace(from);
        checkPlace(to);
        checkDate(departureDate);
        if(!StringUtils.isEmpty(returnDate)) {
            isWithinTheFollowingYear(returnDate);
            checkDatesCombination(departureDate, returnDate);
        }
    }

    private void checkAdults(int adults) {
        if(adults > 4){
            LOGGER.error("The passengers are more than expected.");
            throw new ParameterException("La cantidad de pasajeros excede el límite posible.");
        }
    }


    private void checkDatesCombination(String fromDate, String toDate) {
        Date firstDate = this.getDate(fromDate);
        Date secondDate = this.getDate(toDate);

        if(firstDate.compareTo(secondDate) > 0) {
            LOGGER.error("The to_date is in the past. Impossible to make the journey, sorry.");
            throw new ParameterException("No puedes volar al pasado.");
        }
    }

    private void checkDate(String date) {
        if(StringUtils.isEmpty(date)) {
            LOGGER.error("You need to specify the departure date. The arrival date is optional.");
            throw new ParameterException("Necesitas especificar la fecha de partida. La fecha de llegada es opcional.");
        }
        this.isAFutureDate(date);
        this.isWithinTheFollowingYear(date);
    }

    private void isWithinTheFollowingYear(String date) {
        LocalDate localDateFlight = this.getDate(date).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate localDateNow = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        if((localDateFlight.getYear() != (localDateNow.getYear()+1)) && (localDateFlight.getYear() != localDateNow.getYear())){
            LOGGER.error("There was a problem with the DATE (year) input");
            throw new ParameterException("El AÑO es incorrecto (debe ser entre hoy y un año en el futuro).");
        }

        if( localDateFlight.getYear() == (localDateNow.getYear()+1) ){
            if( localDateFlight.getMonthValue() > localDateNow.getMonthValue()){
                LOGGER.error("There was a problem with the DATE (month) input");
                throw new ParameterException("El MES es incorrecto (debe ser entre hoy y un año en el futuro).");
            }
            if( localDateFlight.getDayOfMonth() > localDateNow.getDayOfMonth()){
                LOGGER.error("There was a problem with the DATE (day) input");
                throw new ParameterException("El DÍA es incorrecto (debe ser entre hoy y un año en el futuro).");
            }
        }

    }

    private void isAFutureDate(String date) {
        Date now = new Date();
        Date flightDate = this.getDate(date);

        if(now.compareTo(flightDate) > 0) {
            LOGGER.error("The from_date is too old to perform the operation.");
            throw new ParameterException("No se puede realizar una búsqueda en el pasado (la fecha de partida es muy antigua).");
        }
    }

    private Date getDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        dateFormat.setLenient(false);
        try {
            return dateFormat.parse(date);
        } catch (ParseException pe) {
            LOGGER.error("The date " + date + " does not match the format yyyy-MM-dd or is an invalid date.", pe);
            throw new ParameterException("La fecha " + date + " no coincide con el patrón yyyy-MM-dd o es una fecha inválida.", pe);
        }
    }

    private void checkPlace(String location) {
        if(StringUtils.isEmpty(location)){
            LOGGER.error("You need to specify the departure and destiny location.");
            throw new ParameterException("Debe específicar el lugar de origen y su destino.");
        }
        if(location.length() != 3) {
            LOGGER.error("The locations must be 3 characters long.");
            throw new ParameterException("Los lugares deben ser 3 caracteres de longitud máximo.");
        }
        if(!location.matches("[a-zA-Z]+")) {
            LOGGER.error("The locations must follow the regex '[a-zA-Z]+'");
            throw new ParameterException("El lugar '" + location + "' no es un código IATA válido.");
        }
        if(!isAValidLocation(location)) {
            LOGGER.error("The location '" + location + "' is not a valid one.");
            throw new ParameterException("El lugar '" + location + "' no es válido.");
        }
    }

    //TODO - When locations repository is available.
    private boolean isAValidLocation(String location) {
        HashMap<String, String> codes = new HashMap<>();
        //return codes.containsValue(location);
        return true;
    }

}
