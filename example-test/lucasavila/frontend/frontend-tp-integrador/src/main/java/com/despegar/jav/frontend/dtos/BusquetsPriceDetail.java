package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

public class BusquetsPriceDetail {

    @Expose
    private MainFare mainFare;

    public double getAmountWithDiscount() {
        return mainFare.getAmountWithDiscount();
    }

    public double getAmountWithoutDiscount() {
        return mainFare.getAmountWithoutDiscount();
    }

    public String getPrimaryMessage() {
        return mainFare.getPrimaryMessage();
    }

    public String getSecondaryMessage() {
        return mainFare.getSecondaryMessage();
    }

}
