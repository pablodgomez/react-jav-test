package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

public class Segment {

    @Expose
    private FlightInformation arrival;
    @Expose
    private FlightInformation departure;
    @Expose
    private String airlineCode;
    @Expose
    private CabinType cabinType;
    @Expose
    private String duration;
}
