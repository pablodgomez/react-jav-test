package com.despegar.jav.frontend.exceptions;

import com.google.gson.annotations.Expose;

public class BusquestsError {

    @Expose
    private String message;

    public String getMessage() {
        return message;
    }
}
