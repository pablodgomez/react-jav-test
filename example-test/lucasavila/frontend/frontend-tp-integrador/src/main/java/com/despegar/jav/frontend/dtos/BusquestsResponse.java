package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

import java.util.HashMap;
import java.util.List;

public class BusquestsResponse {

    @Expose
    private List<BusquetsCluster> clusters;
    @Expose
    private ReferenceData referenceData;

    public List<BusquetsCluster> getClusters() {
        return clusters;
    }

    public HashMap<String,String> getAirlinesNames() {
        return referenceData.getAirlinesNames();
    }

    public HashMap<String,String> getCities() {
        return referenceData.getCities();
    }

    public HashMap<String,String> getCitiesCodeByAirport() {
        return referenceData.getCitiesCodeByAirport();
    }

}
