package com.despegar.jav.frontend.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class Cluster {

    private RoutePack routePack;
    private PriceDetail priceDetail;
    private String departureAirportCodeOutbound;
    private String arrivalAirportCodeOutbound;
    private String departureAirportCodeInbound = "";
    private String arrivalAirportCodeInbound = "";

    public Cluster(RoutePack routePack, PriceDetail priceDetail, String departureAirportCodeOutbound, String arrivalAirportCodeOutbound, String departureAirportCodeInbound, String arrivalAirportCodeInbound) {
        this.routePack = routePack;
        this.priceDetail = priceDetail;
        this.departureAirportCodeOutbound = departureAirportCodeOutbound;
        this.arrivalAirportCodeOutbound = arrivalAirportCodeOutbound;
        this.departureAirportCodeInbound = departureAirportCodeInbound;
        this.arrivalAirportCodeInbound = arrivalAirportCodeInbound;
    }

    public Cluster(RoutePack routePack, PriceDetail priceDetail, String departureAirportCodeOutbound, String arrivalAirportCodeOutbound) {
        this.routePack = routePack;
        this.priceDetail = priceDetail;
        this.departureAirportCodeOutbound = departureAirportCodeOutbound;
        this.arrivalAirportCodeOutbound = arrivalAirportCodeOutbound;
    }
}
