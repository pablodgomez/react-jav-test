package com.despegar.jav.frontend.model;

public class StopsInformation {

    private int count;
    private String message;
    private boolean hasStops;

    public StopsInformation(int count, String message, boolean hasStops) {
        this.count = count;
        this.message = message;
        this.hasStops = hasStops;
    }
}
