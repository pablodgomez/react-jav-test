package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

public class MainFare {

    @Expose
    private double amount;
    @Expose
    private double amountWithoutDiscount;
    @Expose
    private String primaryMessage;
    @Expose
    private String secondaryMessage;

    public double getAmountWithDiscount() {
        return amount;
    }


    public double getAmountWithoutDiscount() {
        return amountWithoutDiscount;
    }

    public String getPrimaryMessage() {
        return primaryMessage;
    }

    public String getSecondaryMessage() {
        return secondaryMessage;
    }
}
