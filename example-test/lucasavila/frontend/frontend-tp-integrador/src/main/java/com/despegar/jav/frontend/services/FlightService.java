package com.despegar.jav.frontend.services;

import com.despegar.jav.frontend.connectors.BusquestsConnector;
import com.despegar.jav.frontend.dtos.BusquestsResponse;
import com.despegar.jav.frontend.mappers.MapperToModel;
import com.despegar.jav.frontend.model.SearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class FlightService {

    @Autowired
    private BusquestsConnector busquestsConnector;
    @Autowired
    private MapperToModel mapper;
    private static final Logger LOGGER = LoggerFactory.getLogger(FlightService.class);

    public SearchResult getSearchResult(int adults, String from, String to, String departureDate, String returnDate) {
        BusquestsResponse response = busquestsConnector.getResponse(adults, from, to, mapper
                .transformDateFormat(departureDate), mapper.transformDateFormat(returnDate));
        LOGGER.info("Proceding to map response to model");
        Boolean hasAReturnTravel = true;
        if(StringUtils.isEmpty(returnDate))
            hasAReturnTravel = false;
        return mapper.mapToSearchResult(response, hasAReturnTravel);
    }
}
