package com.despegar.jav.frontend.model;

import java.util.HashMap;
import java.util.List;

public class SearchResult {

    private List<Cluster> clusters;
    private HashMap<String, String> airlines;
    private HashMap<String, String> citiesCodeByAirport;
    private HashMap<String, String> cities;
    private boolean hasAReturnTravel;

    public SearchResult(List<Cluster> clusters, HashMap<String, String> airlines, HashMap<String, String> cities, HashMap<String, String> citiesCodeByAirport, boolean hasAReturnTravel) {
        this.clusters = clusters;
        this.airlines = airlines;
        this.cities = cities;
        this.citiesCodeByAirport = citiesCodeByAirport;
        this.hasAReturnTravel = hasAReturnTravel;
    }
}
