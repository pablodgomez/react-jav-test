package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

import java.util.HashMap;

public class ReferenceData {

    @Expose
    private HashMap<String, String> airlines;
    @Expose
    private HashMap<String, String> cities;
    @Expose
    private HashMap<String, String> citiesCodeByAirport;

    public HashMap<String,String> getAirlinesNames() {
        return airlines;
    }

    public HashMap<String, String> getCities() {
        return cities;
    }

    public HashMap<String,String> getCitiesCodeByAirport() {
        return citiesCodeByAirport;
    }
}
