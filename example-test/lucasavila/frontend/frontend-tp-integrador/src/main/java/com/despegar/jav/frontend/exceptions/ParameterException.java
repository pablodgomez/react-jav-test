package com.despegar.jav.frontend.exceptions;

public class ParameterException extends RuntimeException {

    public ParameterException(String s) {
        super(s);
    }

    public ParameterException(String s, Exception e) {
        super(s, e);
    }
}
