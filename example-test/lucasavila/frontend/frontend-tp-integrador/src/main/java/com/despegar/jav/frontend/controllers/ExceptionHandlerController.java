package com.despegar.jav.frontend.controllers;

import com.despegar.jav.frontend.exceptions.BusquestsException;
import com.despegar.jav.frontend.exceptions.HttpRequestException;
import com.despegar.jav.frontend.exceptions.ParameterException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler({ ParameterException.class })
    public void handleParameterException(ParameterException ex, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        response.getWriter().write(ex.getMessage());
    }

    @ExceptionHandler({ HttpRequestException.class })
    public void handleConnectorException(HttpRequestException ex, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        response.getWriter().write(ex.getMessage());
    }

    @ExceptionHandler({ BusquestsException.class })
    public void handleConnectorException(BusquestsException ex, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpStatus.NOT_FOUND.value());
        response.getWriter().write(ex.getMessage());
    }
}


