package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

import java.util.Date;

public class FlightInformation {

    @Expose
    private String airportCode;
    @Expose
    private Date date;

    public String getAirportCode() {
        return airportCode;
    }

    public Date getDate() {
        return date;
    }
}
