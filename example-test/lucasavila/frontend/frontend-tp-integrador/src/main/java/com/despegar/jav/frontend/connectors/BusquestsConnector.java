package com.despegar.jav.frontend.connectors;

import com.despegar.jav.frontend.dtos.BusquestsResponse;
import com.despegar.jav.frontend.exceptions.BusquestsError;
import com.despegar.jav.frontend.exceptions.BusquestsException;
import com.despegar.jav.frontend.exceptions.BusquestsMessage;
import com.despegar.jav.frontend.exceptions.HttpRequestException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Request;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.List;

@Component
public class BusquestsConnector {

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
    @Value("${busquests.url}")
    private String url;
    private static final Logger LOGGER = LoggerFactory.getLogger(BusquestsConnector.class);

    public BusquestsResponse getResponse(int adults, String from, String to, String departureDate, String returnDate) {

        LOGGER.info(String.format("Appending info to URL. Adults: %d, FROM: %s, TO: %s, DepartureDate: %s, ReturnDate: %s", adults, from, to, departureDate, returnDate));
        String finalUrl = this.appendInfo(adults, from, to, departureDate, returnDate);

        try {
            String jsonResponse = this.sendRequest(Request.Get(finalUrl).addHeader("XDESP-SF-DEBUG", "true"));
            LOGGER.info("Got Busquests response -> JSON: " + jsonResponse);
            return gson.fromJson(jsonResponse, BusquestsResponse.class);
        } catch (HttpRequestException hre) {
            LOGGER.info("The communication with Busquest failed", hre);
            throw new HttpRequestException("Hubo un problema con la petición. Intente más tarde.", hre);
        }

    }

    private String appendInfo(int adults, String from, String to, String departureDate, String returnDate) {

        String finalUrl = String.format(url, adults, from, to, departureDate, "%s");

        if (!StringUtils.isEmpty(returnDate))
            finalUrl = String.format(finalUrl, "returnDate=" + returnDate);
        else
            finalUrl = String.format(finalUrl, "");

        LOGGER.info(String.format("Final URL: %s", finalUrl));
        return finalUrl;
    }


    private String sendRequest(Request request) {
        try {
            HttpResponse response = request.execute().returnResponse();
            this.checkStatusCode(response);
            return EntityUtils.toString(response.getEntity());
        } catch (IOException ioe) {
            LOGGER.error("There was an error with the request.", ioe);
            throw new HttpRequestException("Hubo un error en la conexión. Por favor intente más tarde.", ioe);
        }
    }

    private void checkStatusCode(HttpResponse response) throws IOException {
        int statusCode = response.getStatusLine().getStatusCode();
        LOGGER.info("Checking status code: " + statusCode);
        if(statusCode == 430){
            LOGGER.error("There was a problem with the request. Status code: " + statusCode);
            handleBusquestsError(response);
        }
        if (statusCode < 200 || statusCode > 299) {
            LOGGER.error("There was a problem with the request. Status code: " + statusCode);
            throw new HttpRequestException("Hubo un error en la conexión. Por favor intente más tarde.");
        }
    }

    private void handleBusquestsError(HttpResponse response) {
        try{
            String jsonResponse = EntityUtils.toString(response.getEntity());
            LOGGER.info("Got Busquests Error response -> JSON: " + jsonResponse);
            String message = gson.fromJson(jsonResponse, BusquestsError.class).getMessage();
            String place = gson.fromJson(message, BusquestsMessage.class).getCauses().get(0).substring(0, 3);
            throw new BusquestsException("Hubo un error en la conexión. No existen aeropuertos para " + place + " especificado.");
        }catch (IOException ioe){
            LOGGER.error("There was an error with the request.", ioe);
            throw new HttpRequestException("Hubo un error en la conexión. Por favor intente más tarde.", ioe);
        }
    }

}