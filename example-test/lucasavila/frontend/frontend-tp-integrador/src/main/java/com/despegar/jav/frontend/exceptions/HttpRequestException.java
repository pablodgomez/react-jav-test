package com.despegar.jav.frontend.exceptions;


import java.io.IOException;

public class HttpRequestException extends RuntimeException {

    public HttpRequestException(String message){
        super(message);
    }

    public HttpRequestException(String message, Exception ex) {
        super(message, ex);
    }
}
