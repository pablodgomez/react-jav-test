package com.despegar.jav.frontend.dtos;

import com.google.gson.annotations.Expose;

public class BusquestsBaggageInfo {

    @Expose
    private int quantity;
    @Expose
    private BaggageType type;
    @Expose
    private BaggageType handbagType;

    public int getQuantity() {
        return quantity;
    }

    public BaggageType getType() {
        return type;
    }

    public BaggageType getHandbagType() {
        return handbagType;
    }
}
