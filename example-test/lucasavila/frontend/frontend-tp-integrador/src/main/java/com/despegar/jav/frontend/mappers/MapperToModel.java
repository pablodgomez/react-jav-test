package com.despegar.jav.frontend.mappers;

import com.despegar.jav.frontend.dtos.*;
import com.despegar.jav.frontend.model.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class MapperToModel {

    public SearchResult mapToSearchResult(BusquestsResponse response, boolean hasAReturnTravel){
        return new SearchResult(this.mapToClusters(response.getClusters(), hasAReturnTravel, response.getAirlinesNames()),
                response.getAirlinesNames(),
                response.getCities(),
                response.getCitiesCodeByAirport(),
                hasAReturnTravel);
    }

    private List<Cluster> mapToClusters(List<BusquetsCluster> busquetsClusters, boolean hasAReturnTravel, HashMap<String, String> airlines) {
        return busquetsClusters.stream().map(cluster -> this.createNewCluster(cluster, hasAReturnTravel, airlines)).collect(Collectors.toList());
    }

    private Cluster createNewCluster(BusquetsCluster oldCluster, boolean hasAReturnTravel, HashMap<String, String> airlines) {
        RoutePack routePack = getRoutePack(oldCluster, airlines);
        PriceDetail priceDetail = getPriceDetail(oldCluster);
        if(hasAReturnTravel)
            return new Cluster(routePack,
                    priceDetail,
                    oldCluster.getDepartureAirportCodeOutbound(),
                    oldCluster.getArrivalAirportCodeOutbound(),
                    oldCluster.getDepartureAirportCodeInbound(),
                    oldCluster.getArrivalAirportCodeInbound());

        return new Cluster(routePack, priceDetail, oldCluster.getDepartureAirportCodeOutbound(), oldCluster.getArrivalAirportCodeOutbound());
    }

    private PriceDetail getPriceDetail(BusquetsCluster oldCluster) {
        BusquetsPriceDetail oldPriceDetail = oldCluster.getPriceDetail();
        return new PriceDetail(formatPrice(oldPriceDetail.getAmountWithDiscount()),
                formatPrice(oldPriceDetail.getAmountWithoutDiscount()),
                oldPriceDetail.getPrimaryMessage(),
                oldPriceDetail.getSecondaryMessage(),
                oldCluster.getPaymentsMessage());
    }

    private String formatPrice(double number) {
        NumberFormat anotherFormat = NumberFormat.getNumberInstance(Locale.GERMAN);
        DecimalFormat finalFormat = (DecimalFormat) anotherFormat;
        finalFormat.setGroupingUsed(true);
        finalFormat.setGroupingSize(3);
        return finalFormat.format(number);
    }

    private RoutePack getRoutePack(BusquetsCluster oldCluster, HashMap<String, String> airlines){
        Choice choice = oldCluster.getOutboundChoice();
        List<BusquetsRoute> routes = choice.getRoutes();
        List<Route> finalRoutes = routes.stream().map(route -> this.createNewRoute(route, airlines)).collect(Collectors.toList());

        RoutePack routePack = new RoutePack(createDate(oldCluster.getDepartureDate()), createDate(oldCluster.getArrivalDate()));
        routePack.setOutboundRoutes(finalRoutes);
        if(oldCluster.hasAReturnChoice()){
            choice = oldCluster.getInboundChoice();
            routes = choice.getRoutes();
            finalRoutes = routes.stream().map(route -> this.createNewRoute(route, airlines)).collect(Collectors.toList());
            routePack.setInboundRoutes(finalRoutes);
        }
        return routePack;
    }

    private String createDate(Date departureDate) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE. d MMM. yyyy", new Locale("es", "ES"));
        return dateFormat.format(departureDate);
    }

    private Route createNewRoute(BusquetsRoute route, HashMap<String, String> airlines) {
        return new Route(route.getBaggageQuantity(), createAirlinesHash(airlines, route.getAirlines()),
                route.getDepartureAirportCode(),
                route.getArrivalAirportCode(),
                getTime(route.getDepartureDate()),
                getTime(route.getArrivalDate()),
                getDayGap(route.getDepartureDate(), route.getArrivalDate()),
                createStopsInformation(route.getStopsQuantity()),
                formatDuration(route.getTotalDuration()),
                createBaggageInfo(route.getBaggageInfo()));
    }

    private BaggageInfo createBaggageInfo(BusquestsBaggageInfo baggageInfo) {
        return new BaggageInfo(baggageInfo.getType().equals(BaggageType.INCLUDED),
                baggageInfo.getHandbagType().equals(BaggageType.INCLUDED));
    }

    private StopsInformation createStopsInformation(int stopsQuantity) {
        return new StopsInformation(stopsQuantity, getStopMessage(stopsQuantity), stopsQuantity != 0);
    }

    private String getStopMessage(int stopsQuantity) {
        switch (stopsQuantity){
            case 1:
                return "1 escala";
            case 2:
                return "2 escalas";
            default:
                return "Directo";
        }
    }

    private long getDayGap(Date departureDate, Date arrivalDate) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String date = dateFormat.format(departureDate);
        LocalDate departure = LocalDate.parse(date);
        date = dateFormat.format(arrivalDate);
        LocalDate arrival = LocalDate.parse(date);

        long diff = departure.getDayOfWeek().getValue() - arrival.getDayOfWeek().getValue();
        return Math.abs(diff);
    }

    private String formatDuration(String totalDuration) {
        String[] duration = totalDuration.split(":");
        return String.format("%sh %sm", duration[0], duration[1]);
    }

    private String getTime(Date date) {
        LocalDateTime time = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        time.format(DateTimeFormatter.ofPattern("HH:mm"));
        time.getHour();
        return String.format("%02d:%02d", time.getHour(), time.getMinute());
    }

    private Map<String, String> createAirlinesHash(HashMap<String, String> airlinesMap, List<String> airlinesCodes) {
        return airlinesCodes.stream().collect(Collectors.toMap(airlinesCode -> airlinesCode, airlinesCode -> airlinesMap.get(airlinesCode)));
    }

    public String transformDateFormat(String sampleDate) {
        if(StringUtils.isEmpty(sampleDate))
            return sampleDate;
        String[] date = sampleDate.split("/");
        return date[2]+ "-" + date[1] + "-" +date[0];
    }
}
