package com.despegar.jav.frontend.model;

public class BaggageInfo {

    private boolean hasBigBaggage;
    private boolean hasHandbag;

    public BaggageInfo(boolean hasBigBaggage, boolean hasHandbag) {
        this.hasBigBaggage = hasBigBaggage;
        this.hasHandbag = hasHandbag;
    }
}
