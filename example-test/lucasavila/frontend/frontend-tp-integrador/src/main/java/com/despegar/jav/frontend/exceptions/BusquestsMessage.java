package com.despegar.jav.frontend.exceptions;

import com.google.gson.annotations.Expose;

import java.util.List;

public class BusquestsMessage {

    @Expose
    private List<String> causes;

    public List<String> getCauses() {
        return causes;
    }
}
