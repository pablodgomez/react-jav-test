<!DOCTYPE html>
<html>
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
  <link rel="stylesheet" type="text/css" href="eva/eva-core.min.css">
  <link rel="stylesheet" type="text/css" href="eva/eva.min.css">
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <link rel="stylesheet" type="text/css" href="styles/main.css">
  <link rel="stylesheet" type="text/css" href="styles/loader.css">
  <script type="text/javascript" src="js/libs/jquery-3.3.1.js"></script>
  <script type="text/javascript" src="js/libs/handlebars-v4.0.11.js"></script>
  <script type="text/javascript" src="js/libs/jquery-autocomplete.js"></script>
</head>
<body>

  <div class="eva-3-container">
    <!-- Search Box -->
    <div class="eva-3-row" id="search-box">
      <!-- Grill starts -->
      <div class="col -sm-12 -md-8 -lg-5">
        <div class="-eva-3-bc-green-1 -eva-3-bc-yellow-3 search-box-padding">
          <div class="eva-3-row">
            <!-- Row title -->
            <div class="col -sm-12">
              <span class="-eva-3-tc-blue-2 eva-3-h3">Encontrá tu Vuelo</span>
            </div>
            <!-- Row ends -->
          </div>
          <div class="eva-3-row">
            <!-- Row radio buttons -->
            <div class="col -sm-12">
              <!-- Radio Buttons Container -->
              <div class="flexible-container -eva-3-mt-md">
                <div class="radio-margin-right">
                  <span class="eva-3-radio" id="modalities"> 
                    <label class="radio-label-container"> 
                      <input type="radio" name="modality-radio" class="radio-tag flight-type" value="Ida y vuelta" checked/><i class="radio-circle"></i><span class="-eva-3-tc-gray-1 -sm eva-3-h6">Ida y vuelta</span>
                    </label>
                  </span>
                </div>
                <div class="radio-margin-right">
                  <span class="eva-3-radio"> 
                    <label class="radio-label-container"> 
                      <input type="radio" name="modality-radio" class="radio-tag flight-type" value="Solo ida"/><i class="radio-circle"></i><span class="-eva-3-tc-gray-1 eva-3-h6">Solo ida</span>
                    </label>
                  </span>
                </div>
                <div class="radio-margin-right">
                  <span class="eva-3-radio"> 
                    <label class="radio-label-container"> 
                      <input type="radio" name="modality-radio" class="radio-tag flight-type" value="Multidestino" disabled/><i class="radio-circle"></i><span class="-eva-3-tc-gray-1 eva-3-h6">Multidestino</span>
                    </label>
                  </span>
                </div>
              </div>
              <!-- Ends Radio Buttons Container -->
            </div>
            <!-- Row ends -->
          </div>
          <div class="eva-3-row -no-gutter">
            <!-- Row Places -->
            <div class="col -sm-12 -md-6">
              <!-- Origin -->
              <div class="eva-3-input -lg -eva-3-mt-lg -eva-3-mr-xsm places" id="origin-box">
                <label for="origen-input" class="eva-3-label-form -lg -eva-3-pb-xsm -eva-3-tc-gray-2">Origen</label>
                <div class="input-container" id="md-lg-borders-right">
                  <input type="text" placeholder="Ingresa desde dónde viajas" class="input-tag place-input" id="origin-input"/>
                  <span class="validation-msg -eva-3-hide"></span>
                </div>
              </div>
            </div>
            <div class="col -sm-12 -md-6">
              <!-- Destiny -->
              <div class="eva-3-input -lg -eva-3-mt-lg places" id="destiny-box">
                <label for="destiny-input" class="eva-3-label-form -lg -eva-3-pb-xsm -eva-3-tc-gray-2">Destino</label>
                <div class="input-container" id="md-lg-borders-left">
                  <input type="text" placeholder="Ingresa hacia dónde viajas" id="destiny-input" class="input-tag place-input"/>
                  <span class="validation-msg -eva-3-hide"></span>
                </div>
              </div>
            </div>
            <!-- Row ends -->
          </div>
          <div class="eva-3-row -no-gutter">
            <!-- Row Dates and Passengers -->
            <div class="col -sm-12 -md-8">
              <!-- Column dates -->
              <div class="eva-3-row">
                <div class="col -sm-12">
                  <div class="eva-3-row">
                    <!-- Title row -->
                    <div class="col -sm-12">
                      <div class="eva-3-input -lg -eva-3-mt-lg">
                        <label for="departure-input" class="eva-3-label-form -lg -eva-3-pb-xsm -eva-3-tc-gray-2">Fechas</label>
                      </div>
                    </div>
                    <!-- Title row ends-->
                  </div>
                  <div class="eva-3-row -no-gutter">
                    <!-- Input Dates Row -->
                    <div class="col -sm-6">
                      <!-- Column Departure -->
                      <div class="-eva-3-mr-xsm">
                        <div class="eva-3-input -lg -icon-left" id="departure-date">
                          <div class="input-container" id="borders-right">
                            <input type="text" placeholder="Partida" id="departure-input" class="input-tag date-input"/><i class="input-icon eva-3-icon-calendar"></i>
                            <span class="validation-msg"></span>
                          </div>
                        </div>
                      </div>
                      <!-- Column Departure ends -->
                    </div>
                    <div class="col -sm-6">
                      <!-- Column Arrival -->
                      <div class="eva-3-input -lg -icon-left dates" id="arrival-date">
                        <div class="input-container" id="borders-left">
                          <input type="text" placeholder="Regreso" id="arrival-input" class="input-tag date-input"/><i class="input-icon eva-3-icon-calendar"></i>
                          <span class="validation-msg"></span>
                        </div>
                      </div>
                      <!-- Column Arrival ends -->
                    </div>
                    <!-- Input Dates end -->
                  </div>
                  <div class="eva-3-row">
                    <!-- Checkbox row -->
                    <div class="col -sm-12">
                      <div class="-eva-3-mt-sm">
                        <span class="eva-3-checkbox"> 
                          <label class="checkbox-label"> 
                            <input type="checkbox" class="checkbox-tag"/><i class="checkbox-check eva-3-icon-checkmark"></i><label class="eva-3-label-form -md -eva-3-pb-xsm -eva-3-tc-gray-2 -eva-3-ml-xsm">Todavía no decidí mi fecha</label>
                          </label></span>
                        </div>
                      </div>
                      <!-- Checkbox row ends-->
                    </div>
                  </div>
                </div>
                <!-- Column dates ends -->
              </div>
              <div class="col -sm-12 -md-4">
                <!-- Column passengers -->
                <div class="eva-3-input -lg -eva-3-mt-lg passengers-margin-left">
                  <label for="label-input" class="eva-3-label-form -lg -eva-3-pb-xsm -eva-3-tc-gray-2">Pasajeros</label>
                  <div class="eva-3-select -lg -no-border passengers-select passengers-icon">
                    <div class="select-container" id="select-container-personalize">
                      <select id="passengers-select" class="select-tag">
                        <option value="1" selected="selected" class="select-option">1 persona</option>
                        <option value="2" class="select-option">2 personas</option>
                        <option value="3" class="select-option">3 personas</option>
                        <option value="4" class="select-option">4 personas</option>
                      </select>
                      <span class="validation-msg"></span>
                    </div>
                  </div>
                </div>
                <!-- Column passengers ends -->
              </div>
              <!-- Row ends -->
            </div>
            <div class="eva-3-row">
              <!-- Row Search Button -->
              <div class="col -sm-12">
                <div class="-eva-3-mt-md -eva-3-mb-md">
                  <a class="eva-3-btn -lg  -secondary search-width" id="submit"><em class="btn-text">Buscar</em><span class="btn-status">Cargando ...</span></a>
                </div>
              </div>
              <!-- Row Search Button ends -->
            </div>
        </div>
      </div>
      <div class="col -sm-12 -md-4 -lg-7">
        <!-- Ads -->
        <div class="eva-3-row">
          <!-- Row starts -->
          <div class="col -eva-3-hide-small -md-12 -lg-6">
            <div class="-eva-3-bc-red-1 box"></div>
          </div>
          <div class="col -eva-3-hide-medium-down -lg-6">
            <div class="-eva-3-bc-orange-3 box"></div>
            <!-- Row ends -->
          </div>
        </div>
        <!-- Ads ends -->
      </div>
      <!-- Grill ends -->
    </div>

    <!-- BusquestsCluster Flights -->
    <div class ="-eva-3-hide" id="filter-options">
      <a class="eva-3-button-circle filter-button" id="direct-button">
        <div class="button-circle-icon"><i class="eva-3-icon-subarrow-bold-right"></i></div><span class="button-circle-label">Directos</span>
      </a>
      <a class="eva-3-button-circle -active filter-button" id="all-button">
        <div class="button-circle-icon"><i class="eva-3-icon-flights"></i></div><span class="button-circle-label">Todos</span>
      </a>
      <a class="eva-3-button-circle filter-button" id="stops-button">
        <div class="button-circle-icon"><i class="eva-3-icon-more-outline"></i></div><span class="button-circle-label">Con escalas</span>
      </a>
    </div>

    <div class="loader"></div>

    <div id="search-results">
    </div>

    <div id="warning">
    </div>

    <div id="error">
    </div>

  </div>

  <!-- JS -->
  <script type="text/javascript" src="js/connector.js"></script>
  <script type="text/javascript" src="js/errorCentral.js"></script>
  <script type="text/javascript" src="js/validationsModule.js"></script>
  <script type="text/javascript" src="js/searchBoxModule.js"></script>
  <script type="text/javascript" src="js/helpers.js"></script>
  <script type="text/javascript" src="js/clustersBoxModule.js"></script>
  <script id="results-template" type="text/x-handlebars-template">
    {{#each clusters}}
    <div class="cluster-flight">
      <div class="eva-3-row">
        <div class="eva-3-cluster-basic">
          <div class="cluster-container">
            <div class="cluster-content box">
              <div class="-eva-3-bc-white -eva-3-pt-xsm">
                <!-- Header Departure row -->
                <div class="header-height">
                  <div class="eva-3-row -no-gutter -eva-3-bc-gray-8">
                    <div class="col -sm-12 -md-3">
                      <!-- Flight Modality and Date -->
                      <div class="-eva-3-bc-gray-7 sm-border-radius fmodality-bg-color header-padding">
                        <div class="eva-3-row -no-gutter">
                          <!-- Flight Modality -->
                          <div class="col -sm-6 -md-12">
                            <i class="input-icon eva-3-icon-airplane-going -eva-3-tc-gray-1 -eva-3-icon-xsm"></i><span class="-eva-3-tc-gray-1 eva-3-h6 -eva-3-bold">IDA</span>
                          </div>
                          <!-- Date -->
                          <div class="col -sm-6 -md-12">
                            <div class="date-text-align-right">
                              <span class="-eva-3-tc-gray-1 eva-3-h6 -eva-3-bold -eva-3-capitalize">{{routePack.departureDate}}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Flight Modality and Date ends -->
                    </div>
                    <div class="col -eva-3-hide-small -md-3">
                      <!-- Origin location Info -->
                      <div class="-eva-3-pl-sm">
                        <div class="eva-3-row -no-gutter">
                          <div class="col -eva-3-hide-small -md-12">
                            <div class="combo-location-box">
                              <div>
                                <span class="eva-3-h5 bordered-location-box -eva-3-tc-gray-2">{{departureAirportCodeOutbound}}</span>
                              </div>
                              <span class="font-size-11 -eva-3-tc-gray-2">{{findCity departureAirportCodeOutbound ../cities ../citiesCodeByAirport}}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Origin location Info ends -->
                    </div>
                    <div class="col -eva-3-hide-small -md-6">
                      <!-- Destination location Info -->
                      <div class="eva-3-row">
                        <div class="col -eva-3-hide-small -md-12">
                          <div class="combo-location-box">
                            <div>
                              <span class="eva-3-h5 -eva-3-tc-gray-2">{{arrivalAirportCodeOutbound}}</span>
                            </div>
                            <span class="font-size-11 -eva-3-tc-gray-2">{{findCity arrivalAirportCodeOutbound ../cities ../citiesCodeByAirport}}</span>
                          </div>
                        </div>
                      </div>
                      <!-- Destination location Info ends -->
                    </div>
                  </div>
                </div>

                <!-- Option Going row -->
                {{#each routePack.outboundRoutes}}
                <div class="option-height relative-container">
                    <span class="eva-3-radio">
                        <label class="radio-label-container">
                            <!-- Radio button -->
                            <div class="radio-position">
                                <input type="radio" name="flight-option-radio-cluster{{@../index}}-routeIn" class="radio-tag"/><i class="radio-circle"></i>
                            </div>
                            <!-- Information -->
                            <div class="margin-left-option">
                                <div class="eva-3-row -no-gutter -eva-3-bc-white">
                                    <!-- Flight Details column -->
                                    <div class="col -sm-12">
                                        <div class="-eva-3-pt-sm -eva-3-pl-xsm">
                                            <div class="eva-3-row -no-gutter">
                                                <!-- Airline column -->
                                                <div class="col -sm-12 -md-2">
                                                    <div class="flexible-container">
                                                      {{#each airlines}}
                                                        <span><img src="http://ar.staticontent.com/flights-images/9.111.4/common/airlines/25x25/{{@key}}.png" title="{{this}}"></span>

                                                        {{#hasOnlyOneAirline ../airlines}}
                                                        <div class="eva-3-input -lg">
                                                            <div class="input-container">
                                                                <span class="eva-3-label-form -lg -eva-3-mt-xsm airline-tag">{{this}}</span>
                                                            </div>
                                                        </div>
                                                        {{/hasOnlyOneAirline}}
                                                        {{/each}}
                                                        <span class="info-location"><i class="input-icon eva-3-icon-info-circle-outline -eva-3-tc-light-blue-2 -eva-3-icon-xsm -eva-3-hide-medium-up"></i></span>
                                                    </div>
                                                </div>
                                                <!-- Hours & Stops & Duration-->
                                                <div class="col -sm-10 -md-8">
                                                    <div class="departure-hour-margin">
                                                        <div class="eva-3-row -no-gutter">
                                                            <!-- Departure Hour -->
                                                            <div class="col -sm-4 -md-2">
                                                                <div class="combo-location-box">
                                                                    <div>
                                                                            <span class="eva-3-h5 bordered-location-box -eva-3-tc-gray-2 -eva-3-hide-medium-up -eva-3-mb-xsm">{{departureAirportCode}}</span>
                                                                        </div>
                                                                    <div>
                                                                        <span class="hour-info">{{departureHour}}</span>
                                                                </div>
                                                                </div>
                                                            </div>

                                                            <!-- Stops -->
                                                            <div class="col -sm-4 -md-3">
                                                                <div class="margin-top-option">
                                                                    <div class="relative-container">
                                                                        <span class="stop-border {{#if stopsInfo.hasStops}}no-{{/if}}direct eva-3-h6">{{stopsInfo.message}}</span>
                                                                        {{#if stopsInfo.hasStops}}
                                                                            {{#switch stopsInfo.count}}
                                                                                {{#case 1}}
                                                                        <div class="stop one-circle"></div>
                                                                                {{/case}}
                                                                                {{#case 2}}
                                                                        <div class="stop two-circle"></div>
                                                                        <div class="stop two-circle-sibiling"></div>
                                                                                {{/case}}
                                                                            {{/switch}}
                                                                        {{/if}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Arrival Hour -->
                                                            <div class="col -sm-4 -md-3">
                                                                <div class="combo-location-box">
                                                                    <div>
                                                                        <span class="eva-3-h5 bordered-location-box -eva-3-tc-gray-2 -eva-3-hide-medium-up -eva-3-mb-xsm">{{arrivalAirportCode}}</span>
                                                                    </div>
                                                                    <span class="hour-info">{{arrivalHour}}</span>
                                                                    {{#hasDayGap dayGap}}
                                                                    <span class="font-size-11 -eva-3-tc-red-3 -eva-3-ml-xsm">+{{dayGap}}</span>
                                                                    {{/hasDayGap}}
                                                                </div>
                                                            </div>
                                                            <!-- Duration -->
                                                            <div class="col -sm-12 -md-2">
                                                                <div class="-eva-3-mt-xsm">
                                                                    <span class="-eva-3-tc-gray-2 -sm eva-3-h6">{{totalDuration}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--Baggages -->
                                                <div class="col -sm-2 -md-2">
                                                    <div class="flexible-container space-around margin-top-option">
                                                        <div class="flexible-container">
                                                        <i class="input-icon eva-3-icon-handbag -eva-3-tc-{{#if baggageInfo.hasBigBaggage}}green{{else}}gray{{/if}}-3 -eva-3-icon-sm"></i>
                                                        <i class="input-icon eva-3-icon-single -eva-3-tc-{{#if baggageInfo.hasHandbag}}green{{else}}gray{{/if}}-3 -eva-3-icon-sm"></i>
                                                    </div>
                                                    <i class="input-icon eva-3-icon-arrow-down -eva-3-tc-gray-1 -eva-3-icon-sm -eva-3-hide-small"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </label>
                    </span>
                </div>
                {{/each}}
                {{#if ../hasAReturnTravel}}
                <!-- Header Return Row -->
                <div class="header-height">
                  <div class="eva-3-row -no-gutter -eva-3-bc-gray-8">
                    <div class="col -sm-12 -md-3">
                      <!-- Flight Modality and Date -->
                      <div class="-eva-3-bc-gray-7 sm-border-radius fmodality-bg-color header-padding">
                        <div class="eva-3-row -no-gutter">
                          <!-- Flight Modality -->
                          <div class="col -sm-6 -md-12">
                            <i class="input-icon eva-3-icon-airplane-return -eva-3-tc-gray-1 -eva-3-icon-xsm"></i><span class="-eva-3-tc-gray-1 eva-3-h6 -eva-3-bold">VUELTA</span>
                          </div>
                          <!-- Date -->
                          <div class="col -sm-6 -md-12">
                            <div class="date-text-align-right">
                              <span class="-eva-3-tc-gray-1 eva-3-h6 -eva-3-bold -eva-3-capitalize">{{routePack.arrivalDate}}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Flight Modality and Date ends -->
                    </div>
                    <div class="col -eva-3-hide-small -md-3">
                      <!-- Origin location Info -->
                      <div class="-eva-3-pl-sm">
                        <div class="eva-3-row -no-gutter">
                          <div class="col -eva-3-hide-small -md-12">
                            <div class="combo-location-box">
                              <div>
                                <span class="eva-3-h5 -eva-3-tc-gray-2">{{departureAirportCodeInbound}}</span>
                              </div>
                              <span class="font-size-11 -eva-3-tc-gray-2">{{findCity departureAirportCodeInbound ../cities ../citiesCodeByAirport}}</span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Origin location Info ends -->
                    </div>
                    <div class="col -eva-3-hide-small -md-6">
                      <!-- Destination location Info -->
                      <div class="eva-3-row">
                        <div class="col -eva-3-hide-small -md-12">
                          <div class="combo-location-box">
                            <div>
                              <span class="eva-3-h5 bordered-location-box -eva-3-tc-gray-2">{{arrivalAirportCodeInbound}}</span>
                            </div>
                            <span class="font-size-11 -eva-3-tc-gray-2">{{findCity arrivalAirportCodeInbound ../cities ../citiesCodeByAirport}}</span>
                          </div>
                        </div>
                      </div>
                      <!-- Destination location Info ends -->
                    </div>
                  </div>
                </div>
                <!-- Option Return row -->
                {{#each routePack.inboundRoutes}}
                <div class="option-height relative-container">
                    <span class="eva-3-radio">
                        <label class="radio-label-container">
                            <!-- Radio button -->
                            <div class="radio-position">
                                <input type="radio" name="flight-option-radio-return-cluster{{@../index}}-routeOut" class="radio-tag"/><i class="radio-circle"></i>
                            </div>
                          <!-- Information -->
                            <div class="margin-left-option">
                                <div class="eva-3-row -no-gutter -eva-3-bc-white">
                                    <!-- Flight Details column -->
                                    <div class="col -sm-12">
                                        <div class="-eva-3-pt-sm -eva-3-pl-xsm">
                                            <div class="eva-3-row -no-gutter">
                                                <!-- Airline column -->
                                                <div class="col -sm-12 -md-2">
                                                    <div class="flexible-container">
                                                      {{#each airlines}}
                                                        <span><img src="http://ar.staticontent.com/flights-images/9.111.4/common/airlines/25x25/{{@key}}.png" title="{{this}}"></span>

                                                        {{#hasOnlyOneAirline ../airlines}}
                                                        <div class="eva-3-input -lg">
                                                            <div class="input-container">
                                                                <span class="eva-3-label-form -lg -eva-3-mt-xsm airline-tag">{{this}}</span>
                                                            </div>
                                                        </div>
                                                        {{/hasOnlyOneAirline}}
                                                        {{/each}}
                                                        <span class="info-location"><i class="input-icon eva-3-icon-info-circle-outline -eva-3-tc-light-blue-2 -eva-3-icon-xsm -eva-3-hide-medium-up"></i></span>
                                                    </div>
                                                </div>
                                              <!-- Hours & Stops & Duration-->
                                                <div class="col -sm-10 -md-8">
                                                    <div class="departure-hour-margin">
                                                        <div class="eva-3-row -no-gutter">
                                                            <!-- Departure Hour -->
                                                            <div class="col -sm-4 -md-2">
                                                                <div class="combo-location-box">
                                                                    <div>
                                                                            <span class="eva-3-h5 bordered-location-box -eva-3-tc-gray-2 -eva-3-hide-medium-up -eva-3-mb-xsm">{{departureAirportCode}}</span>
                                                                        </div>
                                                                    <div>
                                                                        <span class="hour-info">{{departureHour}}</span>
                                                                </div>
                                                                </div>
                                                            </div>

                                                          <!-- Stops -->
                                                            <div class="col -sm-4 -md-3">
                                                                <div class="margin-top-option">
                                                                    <div class="relative-container">
                                                                        <span class="stop-border {{#if stopsInfo.hasStops}}no-{{/if}}direct eva-3-h6">{{stopsInfo.message}}</span>
                                                                        {{#if stopsInfo.hasStops}}
                                                                            {{#switch stopsInfo.count}}
                                                                                {{#case 1}}
                                                                        <div class="stop one-circle"></div>
                                                                                {{/case}}
                                                                                {{#case 2}}
                                                                        <div class="stop two-circle"></div>
                                                                        <div class="stop two-circle-sibiling"></div>
                                                                                {{/case}}
                                                                            {{/switch}}
                                                                        {{/if}}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                          <!-- Arrival Hour -->
                                                            <div class="col -sm-4 -md-3">
                                                                <div class="combo-location-box">
                                                                    <div>
                                                                        <span class="eva-3-h5 bordered-location-box -eva-3-tc-gray-2 -eva-3-hide-medium-up -eva-3-mb-xsm">{{arrivalAirportCode}}</span>
                                                                    </div>
                                                                    <span class="hour-info">{{arrivalHour}}</span>
                                                                    {{#hasDayGap dayGap}}
                                                                    <span class="font-size-11 -eva-3-tc-red-3 -eva-3-ml-xsm">+{{dayGap}}</span>
                                                                    {{/hasDayGap}}
                                                                </div>
                                                            </div>
                                                          <!-- Duration -->
                                                            <div class="col -sm-12 -md-2">
                                                                <div class="-eva-3-mt-xsm">
                                                                    <span class="-eva-3-tc-gray-2 -sm eva-3-h6">{{totalDuration}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                              <!--Baggages -->
                                                <div class="col -sm-2 -md-2">
                                                    <div class="flexible-container space-around margin-top-option">
                                                        <div class="flexible-container">
                                                        <i class="input-icon eva-3-icon-handbag -eva-3-tc-green-3 -eva-3-icon-sm"></i>
                                                        <i class="input-icon eva-3-icon-single -eva-3-tc-green-3 -eva-3-icon-sm"></i>
                                                    </div>
                                                    <i class="input-icon eva-3-icon-arrow-down -eva-3-tc-gray-1 -eva-3-icon-sm -eva-3-hide-small"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </label>
                    </span>
                </div>
                {{/each}}
                {{/if}}
              </div>
            </div>
            <!-- PriceBox -->
            <div class="cluster-pricebox-container -eva-3-bc-yellow-4 price-box-dimensions">
              <div class="eva-3-row -no-gutter -sm-box-price-rounded-borders">
                <div class="col -sm-12">
                  <div class="eva-3-row -no-gutter price-box-padding-top">
                    <!-- Price Column -->
                    <div class="col -sm-6 -md-12">
                      <div class="eva-3-row">
                        <div class="col -sm-12">
                          <div class="price-text-align">
                            <span class="font-size-11 -eva-3-tc-gray-1">{{priceDetail.primaryMessage}}</span><i class="-eva-3-icon-xsm eva-3-icon-favorite -eva-3-tc-gray-3 -eva-3-hide-small" id="fav-icon"></i>
                          </div>
                        </div>
                        <div class="col -sm-12">
                          <div class="price-text-align">
                            <span class="font-size-11 -eva-3-tc-gray-3">{{priceDetail.secondaryMessage}}</span>
                          </div>
                        </div>
                        <div class="col -sm-12">
                          <div class="price-text-align -md-margin-price">
                            <span class="font-size-11 -eva-3-tc-gray-1">$</span><span class="font-size-price -eva-3-tc-gray-1">{{priceDetail.amountWithDiscount}}</span>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- Button Column -->
                    <div class="col -sm-6 -md-12">
                      <div class="-eva-3-center -md-margin-button">
                        <a class="eva-3-btn -lg -primary -eva-3-mt-sm">
                          <em class="btn-text">Comprar</em>
                        </a>
                      </div>
                    </div>
                  </div>

                  <div class="eva-3-row -no-gutter">
                    <!-- Credit Card Column -->
                    <div class="col -sm-12">
                      <div class="-eva-3-mt-md -eva-3-bc-yellow-2 -sm-box-price-rounded-borders -md-lg-card-margins">
                        <div class="-eva-3-center">
                          <div class="eva-3-row -no-gutter">
                            <div class="col -sm-6 -md-12">
                              <div class="-eva-3-center">
                                <i class="input-icon eva-3-icon-card -eva-3-tc-gray-1 -eva-3-icon-xsm"></i><span class="font-size-11 -eva-3-tc-gray-1 -eva-3-mt-xsm">{{priceDetail.paymentsMessage}}</span>
                              </div>
                            </div>
                            <div class="col -sm-6 -md-12">
                              <div class="-eva-3-center">
                                <a class="eva-3-btn -sm -link" id="card-link"><em class="btn-text">Ver bancos y tarjetas</em></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Credit Card Column ends -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- PriceBox ends -->
          </div>
        </div>
      </div>
    </div>
    {{/each}}
  </script>
  <script id="warning-template" type="text/x-handlebars-template">
    <div class="eva-3-message -warning">
      <div class="message-icon-container"><i class="message-icon eva-3-icon-warning"></i></div>
      <div class="message-header">
        <h5 class="eva-3-h5 message-title">¡Cuidado con los parámetros!</h5>
      </div>
      <div class="message-body">
        <p class="eva-3-p message-text">{{responseText}}</p>
      </div>
    </div>
  </script>
  <script id="error-template" type="text/x-handlebars-template">
    <!-- Message-->
    <div class="eva-3-message -error">
      <div class="message-icon-container"><i class="message-icon eva-3-icon-error-circle"></i></div>
      <div class="message-header">
        <h5 class="eva-3-h5 message-title">Oops! Hubo un error inesperado</h5>
      </div>
      <div class="message-body">
        <p class="eva-3-p message-text">{{responseText}}</p>
      </div>
    </div>
  </script>
  <script type="text/javascript">
    var validator = new Validator();
    var errorCentral = new ErrorCentral(validator);
    var connector = new Connector();
    var clustersBox = new ClustersBox();
    var searchBox = new SearchBox(errorCentral, connector, clustersBox);
    var helpers = new Helpers();
  </script>

</body>
</html>