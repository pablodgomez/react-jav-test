var ErrorCentral = (function(validator) {
	
	function showPlaceError(place, otherPlace, placeBox, otherPlaceBox){
		if(validator.matchWithOtherPlace(place, otherPlace)){
			showError(placeBox, "Los lugares son iguales");
			showError(otherPlaceBox, "Los lugares son iguales");
			return false;
		}

		hideError(placeBox);
		hideSharedError(otherPlaceBox);

		if(validator.isEmpty(place)){
			showError(placeBox, "Ingrese un origen");
			return false;
		} 

		if(validator.isAnInvalidIATA(place)){
			showError(placeBox, "El lugar es un código IATA inválido");
			return false;
		}

		hideError(placeBox);
		return true;
	}

	function showDateError(date, dateBox){
		if(validator.isEmpty(date)){
			showError(dateBox, "Ingrese una fecha");
			return false;
		}

		if(validator.isAnInvalidDateFormat(date)){
			showError(dateBox, "La fecha debe respetar el formato 'dd/mm/yyyy'");
			return false;
		}

		var stringDate = date.split("/");
		var parsedDate = new Date(stringDate[2], stringDate[1] - 1, stringDate[0]);

		if(!validator.dateIsOk(parsedDate)){
			showError(dateBox, "La fecha es inválida");
			return false;
		}

		if(!validator.isAFutureDate(parsedDate)){
			showError(dateBox, "Debe ser una fecha en el futuro mínimamente");
			return false;
		}

		if(!validator.isANotSoFutureDate(parsedDate)){
			showError(dateBox, "La fecha no puede ser más de un año en el futuro");
			return false;
		} 

		hideError(dateBox);

		return true;
	}

	function showDatesError(departureDate, arrivalDate, arrivalDateBox){
		var stringDate = arrivalDate.split("/");
		var parsedArrival = new Date(stringDate[2], stringDate[1] - 1, stringDate[0]);
		stringDate = departureDate.split("/");
		var parsedDeparture = new Date(stringDate[2], stringDate[1] - 1, stringDate[0]);

		if(!validator.isEmpty(arrivalDate)) {
			if(validator.compareDates(parsedArrival,parsedDeparture)){
				showError(arrivalDateBox, "La fecha debe ser posterior a la partida");
				return false;
			} else {
				hideError(arrivalDateBox);
			}
		}

		return true;
	}

	function showPassengersError(quantity, passengersBox){
		if(validator.isAnInvalidPassengerQuantity(quantity)){
			showError(passengersBox, "La cantidad seleccionada es inválida");
			return false;
		}

		hideError(passengersBox);

		return true;
	}

	function showAllErrors(e){
		var isAllOk = true;
		showPlaceError(e.originInput.val(), e.destinyInput.val(), e.originBox, e.destinyBox) ? isAllOk : (isAllOk = false);
		showPlaceError(e.destinyInput.val(), e.originInput.val(), e.destinyBox, e.originBox) ? isAllOk : (isAllOk = false);
		showDateError(e.departureDateInput.val(), e.departureDateBox) ? isAllOk : (isAllOk = false);
		showDatesError(e.departureDateInput.val(), e.arrivalDateInput.val(), e.arrivalDateBox) ? isAllOk : (isAllOk = false);
		checkModality(e) ? isAllOk : (isAllOk = false);
		showPassengersError($("#passengers-select :selected").val(), e.passengersBox) ? isAllOk : (isAllOk = false);

		return isAllOk;
	}

	function checkModality(e){
		var isOk = true;
		if($('input[name=modality-radio]:checked').val() == "Ida y vuelta"){
			showDateError(e.arrivalDateInput.val(), e.arrivalDateBox) ? isOk : (isOk = false);
			showDatesError(e.departureDateInput.val(), e.arrivalDateInput.val(), e.arrivalDateBox) ? isOk : (isOk = false);
		}

		return isOk;
	}

	function showError(box, message) {
		$(box).addClass("eva-3-validation -invalid -top-right");
		$(box).children(".input-container").children(".validation-msg").removeClass("-eva-3-hide").text(message);
	}

	function hideError(box){
		$(box).removeClass("eva-3-validation -invalid -top-right");
		$(box).children(".input-container").children(".validation-msg").addClass("-eva-3-hide");
	}

	function hideSharedError(box){
		if($(box).children(".input-container").children(".validation-msg").text() == "Los lugares son iguales")
			hideError(box);
	}

	return {
		showPlaceError: showPlaceError,
		showDateError: showDateError,
		showDatesError: showDatesError,
		showPassengersError: showPassengersError,
		showAllErrors: showAllErrors,
		hideError: hideError
	}
});