var Connector = (function(){

	function requestFlights(adults, from, to, departureDate, returnDate, showFlights, showErrorOrWarning){
        $.ajax({
			dataType: "json",
			contentType: "application/json",
            url: "/flights",
			data: { adults: adults, from: from, to: to, departureDate: departureDate, returnDate: returnDate},
            success: showFlights,
            error: showErrorOrWarning
        });
	}

    function requestSuggestions(place, processSeggestions, done){
        $.ajax({
            dataType: "json",
            url: "https://www.despegar.com.ar/suggestions?grouped=true&locale=es_AR&profile=sbox-flights",
            data: { hint: place},
            success: function (response) {
				processSeggestions(response, done);
            }
        });
    }

	return {
		requestFlights: requestFlights,
        requestSuggestions: requestSuggestions
	}
});