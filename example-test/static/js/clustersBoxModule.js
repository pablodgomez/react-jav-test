var ClustersBox = (function () {

    var allFlightsButton = $("#all-button", "#filter-options"),
    directButton = $("#direct-button", "#filter-options"),
    stopsButton = $("#stops-button",  "#filter-options"),
    filterButtons = $(".filter-button", "#filter-options"),
    resultsSource = $("#results-template").html(),
    warningSource = $("#warning-template").html(),
    errorSource = $("#error-template").html(),
    resultTemplate = Handlebars.compile(resultsSource),
    warningTemplate = Handlebars.compile(warningSource),
    errorTemplate = Handlebars.compile(errorSource),
    optionsBox = $("#filter-options"),
    warning = $("#warning"),
    error = $("#error"),
    searchResults = $("#search-results"),
    loader = $(".loader"),
    responseSearch;

    function init() {
        allFlightsButton.on("click", function () {
            changeSelectedButton(allFlightsButton);
            showFlights(responseSearch);
        });

        directButton.on("click", function () {
            changeSelectedButton(directButton);
            showFlights(filterFlights(responseSearch, isDirect));
        });

        stopsButton.on("click", function () {
            changeSelectedButton(stopsButton);
            showFlights(filterFlights(responseSearch, hasStops));
        });
    }

    function changeSelectedButton(button) {
        unMarkedButtons();
        button.addClass("-active");
    }

    function unMarkedButtons() {
        allFlightsButton.removeClass("-active");
        directButton.removeClass("-active");
        stopsButton.removeClass("-active");
    }

    function processResult(searchResult){
        responseSearch = searchResult;
        showFlights(searchResult);
    }

    function showFlights(searchResult){
        loader.css("display", "none");

        if(searchResult.clusters.length == 0){
            showErrorOrWarning({"status": 500, "responseText": "No hay vuelos para dichos parámetros. Intente luego."});
            return;
        }

        optionsBox.removeClass("-eva-3-hide");
        warning.html("");
        error.html("");
        searchResults.html(resultTemplate(searchResult));
    }

    function showErrorOrWarning(response) {
        loader.css("display", "none");
        searchResults.html("");
        if(response.status == 404){
            error.html(errorTemplate(response))
            return;
        }

        if(response.status == 400){
            warning.html(warningTemplate(response));
            return;
        }

        if(response.status == 500) {
            error.html(errorTemplate(response));
            return;
        }
    }

    function filterFlights(response, condition) {
        var clonedResponse = {};
        var clusters = response.clusters;
        Object.keys(response).forEach(key => {
            if(response[key] != response["clusters"])
        clonedResponse[key] = response[key];
    });
        clonedResponse.clusters = new Array();
        clusters.forEach(cluster => {
            if(isSuitableToBeFiltered(cluster.routePack, condition, clonedResponse.hasAReturnTravel)){
            clonedResponse.clusters.push(cloneCluster(cluster, condition, clonedResponse.hasAReturnTravel));
        }
    });
        return clonedResponse;
    }

    function isSuitableToBeFiltered(routePack, condition, hasAReturnTravel) {
        if(!routePack.outboundRoutes.some(condition)){
            return false;
        }
        if(!hasAReturnTravel)
            return true;
        if(routePack.inboundRoutes.some(condition))
            return true;

        return false;
    }

    function cloneCluster(cluster, condition, hasAReturnTravel){
        var newCluster = {};
        Object.keys(cluster).forEach(key => {
            if(key != "routePack")
        newCluster[key] = cluster[key];
    });
        newCluster.routePack = filterRoutePackByCondition(cluster.routePack, condition, hasAReturnTravel);
        return newCluster;
    }

    function filterRoutePackByCondition(routePack, condition, hasAReturnTravel){
        var newRoutePack = { departureDate: routePack.departureDate, arrivalDate: routePack.arrivalDate,
            outboundRoutes: new Array(),
            inboundRoutes: new Array()};
        var goRoutes = routePack.outboundRoutes;
        var returnRoutes = routePack.inboundRoutes;

        newRoutePack.outboundRoutes = goRoutes.filter(route => condition(route));
        if(hasAReturnTravel)
            newRoutePack.inboundRoutes = returnRoutes.filter(route => condition(route));

        return newRoutePack;
    }

    function clean(){
        optionsBox.addClass("-eva-3-hide");
        warning.html("");
        error.html("");
        searchResults.html("");
    }

    function isDirect(route) {
        return route.stopsInfo.count == 0;
    }

    function hasStops(route) {
        return route.stopsInfo.count > 0;
    }

    init()

    return {
        processResult: processResult,
        showErrorOrWarning: showErrorOrWarning,
        clean: clean
    }

});