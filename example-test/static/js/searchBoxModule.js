var SearchBox = (function (errorCentral, connector, clusterBox) {

	var submit = $('#submit', "#search-box"),
	departureDateBox = $("#departure-date", "#search-box"),
	arrivalDateBox = $("#arrival-date", "#search-box"),
	passengersBox = $(".passengers-select", "#search-box"),
	passengersSelect = $("#passengers-select", "#search-box"),
	originBox = $("#origin-box", "#search-box"),
	destinyBox = $("#destiny-box", "#search-box"),
	originInput = $("#origin-input", "#search-box"),
	destinyInput = $("#destiny-input", "#search-box"),
	arrivalDateInput = $("#arrival-input", "#search-box"),
	departureDateInput = $("#departure-input", "#search-box"),
	flightTypes = $(".flight-type"),
	loader = $(".loader");
	var suggestions = new Array();

	function init(){
		originBox.on('focusout', function(){
			errorCentral.showPlaceError(originInput.val(), destinyInput.val(), this, destinyBox);
		});

        destinyBox.on('focusout', function(){
            errorCentral.showPlaceError(destinyInput.val(), originInput.val(), this, originBox);
        });

        originInput.autocomplete({
            lookup: function (query, done) {
            	if(query.length >= 3)
                	connector.requestSuggestions(query, processSuggestions, done);
            },
            onSelect: function (suggestion) {
				originInput.val(suggestion.data);
                errorCentral.showPlaceError(originInput.val(), destinyInput.val(), this, destinyBox);
            }
        });

        destinyInput.autocomplete({
            lookup: function (query, done) {
                if(query.length >= 3)
                    connector.requestSuggestions(query, processSuggestions, done);
            },
            onSelect: function (suggestion) {
                destinyInput.val(suggestion.data);
            }
        });

		departureDateBox.on('focusout', function(){
			if(errorCentral.showDateError(departureDateInput.val(), departureDateBox))
				errorCentral.showDatesError(departureDateInput.val(), arrivalDateInput.val(), arrivalDateBox);
		});

		arrivalDateBox.on('focusout', function(){
			if(flightTypes.filter(':checked').val() == "Ida y vuelta"){
				if(errorCentral.showDateError(arrivalDateInput.val(), arrivalDateBox))
					errorCentral.showDatesError(departureDateInput.val(), arrivalDateInput.val(), arrivalDateBox);
			}
		});

		passengersSelect.on('change', function(e) {
			errorCentral.showPassengersError(e.currentTarget.value, passengersBox);
		});

		submit.on('click', function() {
			if(errorCentral.showAllErrors(getAllSearchElements())) {
				clusterBox.clean();
				loader.css("display", "block");
                connector.requestFlights(passengersSelect.val(), originInput.val(),
                    destinyInput.val(), departureDateInput.val(), arrivalDateInput.val(), clusterBox.processResult, clusterBox.showErrorOrWarning);
            }
		});

        flightTypes.on('change', checkRadioOptionChecked);
	}

	function getAllSearchElements(){
		return {
			departureDateBox: departureDateBox,
			arrivalDateBox: arrivalDateBox,
			passengersBox: passengersBox,
			originBox: originBox,	
			destinyBox: destinyBox,
			originInput: originInput,
			destinyInput: destinyInput,
			arrivalDateInput: arrivalDateInput,
			departureDateInput: departureDateInput
		}
	}

	function checkRadioOptionChecked(e){
		if(e.currentTarget.value == "Solo ida"){
			arrivalDateInput.prop("disabled", true);
			arrivalDateInput.val("");
		}
		else {
			arrivalDateInput.prop("disabled", false);
		}

		errorCentral.hideError(arrivalDateBox);
	}

	/* AutoComplete */

	function processSuggestions(response, done) {
		suggestions = new Array();
		var airports = response.items.find(function (item) {
			return item.group == "AIRPORT";
        });
        var cities = response.items.find(function (item) {
            return item.group == "CITY";
        });

        if(airports != undefined)
            processPlaces(airports.items);
        if(cities != undefined)
            processPlaces(cities.items);

        var result = {
            suggestions: suggestions
        };

        done(result);
    }

	function processPlaces(places) {
        places.forEach(place => {
        	if(place.refinements != undefined){
            	processPlaces(place.refinements, suggestions);
        	}
        	suggestions.push({ "value": place.display, "data": (place.target.code || place.target.iata)});
    });
    }

	init();

	return {}
});