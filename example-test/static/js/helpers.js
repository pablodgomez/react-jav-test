var Helpers = (function () {

    Handlebars.registerHelper('findCity', function(airportCode, cities, citiesCodeByAirport) {
        return new Handlebars.SafeString(cities[citiesCodeByAirport[airportCode]]);;
    });

    Handlebars.registerHelper('hasOnlyOneAirline', function(airlines, options) {
        if(Object.keys(airlines).length == 1) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('hasDayGap', function(gap, options) {
        if(gap > 0) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    });

    Handlebars.registerHelper('switch', function(value, options) {
        this._switch_value_ = value;
        var html = options.fn(this);
        delete this._switch_value_;
        return html;
    });

    Handlebars.registerHelper('case', function(value, options) {
        if (value == this._switch_value_) {
            return options.fn(this);
        }
    });
});