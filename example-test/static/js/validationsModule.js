var Validator = (function(){

	IATA_REGEX = new RegExp(/^.[a-zA-Z0-9_]{2}$/),
	DATE_REGEX = new RegExp(/[0-9]{2}[-|\/]{1}[0-9]{2}[-|\/]{1}[0-9]{4}/);

	function validateAll(){
		places.each(function() {
			validatePlace(this);
		});
		validateDate(departureDateBox);
		if($('input[name=modality-radio]:checked').val() == "Ida y vuelta"){
			validateDate(arrivalDateBox);
			compareDates();
		}
		validatePassengers(passengersBox);
	}

	function isAnInvalidPassengerQuantity(quantity) {
		return quantity > 4;
	}

	function isAnInvalidDateFormat(date) {
		return !DATE_REGEX.test(date);
	}

	function compareDates(arrivalDate, departureDate){
		return arrivalDate < departureDate;
	}

	function isAnInvalidIATA(place) {
		return !IATA_REGEX.test(place);
	}

	function matchWithOtherPlace(place, otherPlace) {
		if(!isEmpty(place) && !isEmpty(otherPlace)){
			if(place == otherPlace)
				return true;
		}

		return false;
	}

	function dateIsOk(date){
		return date instanceof Date && !isNaN(date.valueOf());
	}

	function isAFutureDate(date) {
		return date > new Date();
	}

	function isANotSoFutureDate(date) {
		var now = new Date();

		if((date.getYear() != (now.getYear()+1)) && (date.getYear() != now.getYear())){
			return false;
		}

		if( date.getYear() == (now.getYear()+1) ){
			if( date.getMonth() > now.getMonth())
				return false;
			if( date.getDate() > now.getDate())
				return false;
		} 

		return true;
	}

	function isEmpty(string) {
		return !string.trim();
	}

	return {
		isAnInvalidPassengerQuantity: isAnInvalidPassengerQuantity,
		isAnInvalidDateFormat: isAnInvalidDateFormat,
		compareDates: compareDates,
		isAnInvalidIATA: isAnInvalidIATA,
		matchWithOtherPlace: matchWithOtherPlace,
		dateIsOk: dateIsOk,
		isAFutureDate: isAFutureDate,
		isANotSoFutureDate: isANotSoFutureDate,
		isEmpty: isEmpty
	};
});