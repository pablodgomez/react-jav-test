import React, { Component } from 'react';
import responseExample from './response-example.json';
import Filters from './components/Filters/Filters';
import Cluster from './components/Cluster/Cluster';
import Sorting from './components/Sorting/Sorting';
import Searchbox from './components/Searcbox/Searchbox';
import Loading from './components/loading/loading.js';
import './App.css';


class App extends Component { // App component (Alias Container)
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      results: responseExample,
      filteredResults: null
    };
  }

  showResults = (evt) => {
    this.setState({ loading: true });
    fetch('/response-example.json')
      .then((response) => response.json())
      .then((results) => {
        this.setState(
          {
            filteredResults: results,
            loading: false,
          }
        );
      });
  };

  sortItems = (applyOrder) => { // Recieve order method from child components
    this.setState({
      filteredResults: {
        items: applyOrder(this.state.filteredResults.items)
      },
      results: {
        items: applyOrder(this.state.results.items)
      }
    });
  }

  filterItems = (applyFilters) => { // Recieve filter method from child components
    this.setState({
      filteredResults:{ // We can use concat([newelement])
        ...this.state.filteredResults,
        items:applyFilters(this.state.results.items)
      }
    });
  }

  render() {
    const isLoading = this.state.loading;
    return (
    <div>
      <div className="container">
        <nav>
          <Searchbox showResults={this.showResults} />
        </nav>
          <Loading enabled={isLoading}/>
        {
          this.state.filteredResults &&
          (
            <div>
              <aside>
                <Filters
                  items={this.state.results.items}
                  qResults={this.state.filteredResults.items.length}
                  updateFilterItems={this.filterItems}
                />
              </aside>
              <article>
                <div className="sort">
                  <Sorting sortItems={this.sortItems} />
                  <Cluster items={this.state.filteredResults.items} />
                </div>
              </article>
            </div>
          )
        }
      </div>
    </div>
    );
  }
}

export default App;
