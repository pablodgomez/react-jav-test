
import React from 'react';

const Cluster = ({items}) => { // Stateless component
  return (
    <div className="contentContainer">
      {items.map((_item, i) => (
        <div className="box" key={i}>
          <b>FROM:</b> BUE / <b>TO:</b>  MIA <span>{_item.price_detail.total} {_item.price_detail.currency}</span>
          <h3>Outbound</h3>
          {_item.outbound_choices.map((choices,i) => (
            <ul key={i}>
              <li>STOPS: {choices.segments.length < 2 ? 'Direct' : choices.segments.length}</li>
              <li>TOTAL DURATION: {choices.duration}</li>
              <li>AIRLINE: {_item.validating_carrier}</li>
              <hr/>
            </ul>
          ))}
          <h3>Inbound</h3>
          {_item.inbound_choices.map((choices,i) => (
            <ul key={i}>
              <li>STOPS: {choices.segments.length < 2 ? 'Direct' : choices.segments.length}</li>
              <li>TOTAL DURATION: {choices.duration}</li>
              <li>AIRLINE: {_item.validating_carrier}</li>
              <hr/>
            </ul>
          ))}
        </div>
      ))}
    </div>
  )
}

export default Cluster;
