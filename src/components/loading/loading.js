import React from 'react';

const Loading = ({enabled}) => {
  return (
    <div>
      {
        enabled ?
        <div className="loadingContainer">
          <div className="loadingBox">
            <h3>Loading...</h3>
          </div>
        </div> :
        null
      }
    </div>
  )
}

export default Loading;
