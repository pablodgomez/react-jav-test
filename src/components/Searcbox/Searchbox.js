
import React, { Component } from 'react';
import searchIcon from './search-icon.png';

class Searchbox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toField: '',
      toFieldValidation: false
    };
  }

  _handleChange = (e) => {
    this.setState({ toField: e.target.value });
    if (e.target.value.length >= 3) {
      this.setState({ toFieldValidation: false});
    }
  }
  _handleEnterPress = (e) => { if (e.key === 'Enter') { this._handleSearch(e);} }

  _handleSearch = (e) => {
    const { showResults } = this.props
    e.preventDefault();
    if (!this.state.toField.match(/^.{3,}$/)) {
      this.setState({ toFieldValidation: true});
      return;
    }
    this.setState({ toFieldValidation: false });
    showResults(this.state.toField);
  }

  render() {
    return (
      <form className="search-container">
        <input
            className={this.state.toFieldValidation ? 'text-validation' : ''}
            type="text"
            id="search-bar"
            placeholder="Enter any City"
            onChange={this._handleChange}
            onKeyPress={this._handleEnterPress}
            value={this.state.toField}
          />
          <a href="#result"
          style={{display: this.state.toFieldValidation ? 'none' : '' }}
          onClick={this._handleSearch}>
            <img className="search-icon" src={searchIcon} alt="test" />
          </a><br/>
          <span>{this.state.toFieldValidation ? 'Enter valid CC (3 Digits)' : ''}</span>
      </form>
    );
  }
}

export default Searchbox;
