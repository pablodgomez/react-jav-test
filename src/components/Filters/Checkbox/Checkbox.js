
import React, { Component } from 'react';

class Checkbox extends Component { // Example component - Properties {filterType, handleFilters, label}
constructor(props) {
  super(props);
  this.state = { isChecked: false };
}

toggleCheckboxChange = () => {
  const { filterType, doFilters, label } = this.props;
  this.setState({ isChecked: !this.state.isChecked }); // or this.setState(({ isChecked }) => ({ isChecked: !isChecked }));
  doFilters(label, filterType);
}

render() {
  const { label, textLabel = null } = this.props;
  const { isChecked } = this.state;
  return (
    <div className="checkbox">
      <label>
        <input
          type="checkbox"
          value={label}
          checked={isChecked}
          onChange={this.toggleCheckboxChange}
        />
        {textLabel === null ? label : textLabel}
      </label>
    </div>
  );
}
}

export default Checkbox;
