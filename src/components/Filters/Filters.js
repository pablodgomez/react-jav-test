
import React, { Component } from 'react';
import Checkbox from './Checkbox/Checkbox'

class Filters extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      filters: { stops: [], airlines: [] },
      options: {
        stops: [
          { label: '1', textLabel: 'Direct' },
          { label: '2' }, { label: '3' }
        ],
        airlines: [ 
          { label: 'JJ' }, { label: 'LA' } 
        ],
      }
    };
    this.filterMethods = { // Defining filter methods
      airlines: (items, filter) => items.filter(item => filter.airlines.includes(item.validating_carrier)),
      stops: (items, filter) => items.filter(item => // Simple is better? 
        item.outbound_choices[0].segments.length === parseInt(filter.stops) 
        || item.inbound_choices[0].segments.length === parseInt(filter.stops)
      ),
    }
  }

  doFilters = (label, filterType) => {
    const { updateFilterItems } = this.props;
    const handler = (val, label) => val.includes(label) ? val.filter(i => i !== label) : [...val, label];
    const filterItems = filters => i => {
      let resultItems = i;
      Object.keys(filters).forEach(element=>{
        if(filters[element].length){
          resultItems = this.filterMethods[element](resultItems, filters);
        }  
      });
      return resultItems;
    }
    const filterValues = handler(this.state.filters[filterType], label);
    const filter = {[filterType]: filterValues};
    updateFilterItems(filterItems({ ...this.state.filters, ...filter}));
    this.setState({ filters: { ...this.state.filters, ...filter} });
  }

  render() {
    const { qResults } = this.props;
    return (
      <div>
        <h2>{qResults ? qResults : 'No'} Results</h2>
        <h3>Stops<hr /></h3>
        {
          this.state.options.stops.map( (_option,i) => (
            <Checkbox
              filterType='stops' 
              doFilters={this.doFilters}
              key={i}
              {..._option}
            />
          ))
        }
        <h3>Airlines<hr /></h3>
        {
          this.state.options.airlines.map( (_option,i) => (
            <Checkbox
              filterType='airlines' 
              doFilters={this.doFilters}
              key={i}
              {..._option}
            />
          ))
        }
      </div>
    );
  }
}

export default Filters;
