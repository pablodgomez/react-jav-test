
import React, { Component } from 'react';

class Cluster extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      options: [
        {id: 'priceLow', label: 'Price (Low to High)'},
        {id: 'priceHigh', label: 'Price (High to Low)'},
        {id: 'duration', label: 'Duration'},
      ]
    };

    this.orderMethods = { // Defining Order Methos methods
      priceLow: (toSort) => toSort.sort((a,b) => 
        (a.price_detail.total > b.price_detail.total) ? 1 : ((b.price_detail.total > a.price_detail.total) ? -1 : 0)),
      priceHigh: (toSort) => toSort.sort((a,b) => 
        (a.price_detail.total < b.price_detail.total) ? 1 : ((b.price_detail.total < a.price_detail.total) ? -1 : 0)),
      duration: (toSort) => toSort.sort((a,b) => { // OMG? you can do it!
        let aD = a.outbound_choices[0].duration.split(":").reduce((a, b) => a + b);
        let bD = b.outbound_choices[0].duration.split(":").reduce((a, b) => a + b);
        return ((aD > bD) ? 1 : (bD > aD) ? -1 : 0);
      })
    }
  }

  _handleSelectedSort = (e) => {
    const { sortItems } = this.props;
    sortItems(this.orderMethods[e.target.value]);
  }

  render() {
    return (
      <p>
        <label>
          Sort by: 
          <select name="value" onChange={this._handleSelectedSort}>
            {this.state.options.map(_option => <option key={_option.id} value={_option.id}> {_option.label} </option>)}
          </select>  
        </label>
      </p>
    );
  }
}

export default Cluster;
