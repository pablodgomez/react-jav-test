import React from 'react';
import hocExample from '../hocExample/hocExample';

class ComponentNeedingHoc extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currency: '',
      flyId: '',
    }
  }

  componentDidMount() {
    const currency = this.props.load('currency');
    const flyId = this.props.load('flyId');

    if (!currency || !flyId) {
      // This will come from the parent component
      // and would be passed when we spread props {...this.props}
      fetch('/response-example.json')
        .then((response) => response.json())
        .then((results) => {
          this.props.save('currency', results.items[0].price_detail.currency);
          this.props.save('flyId', results.items[0].id);
          this.setState({
            currency: results.items[0].price_detail.currency,
            flyId: results.items[0].id,
          });
        });
    } else {
      this.setState({ currency, flyId })
    }
  }

  render() {
    const { currency, flyId } = this.state;
    if (!currency || !flyId) {
      return <div>Loading...</div>;
    }

    return (
      <div>
        <p>Currency: {currency}</p>
        <p>Fly id: {flyId}</p>
      </div>
    )
  }

}

const WrappedComponentHoc = hocExample(ComponentNeedingHoc);

export default WrappedComponentHoc;
